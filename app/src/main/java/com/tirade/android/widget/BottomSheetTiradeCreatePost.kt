package com.tirade.android.widget

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tirade.android.R
import kotlinx.android.synthetic.main.bottom_sheet_tirade_post_create.view.*


class BottomSheetTiradeCreatePost: BottomSheetDialogFragment() {
    private var mBottomSheetListener: BottomSheetListenerTirade? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.bottom_sheet_tirade_post_create, container, false)

        //handle clicks
        v.upload_video.setOnClickListener {
            mBottomSheetListener!!.onPickVideo()
           // dismiss() //dismiss bottom sheet when item click
        }

        v.upload_image.setOnClickListener {
            mBottomSheetListener!!.onPickImage()
           // dismiss()
        }

        v.cross.setOnClickListener {
            dismiss()
        }

        v.post.setOnClickListener {
            //et_post
            mBottomSheetListener!!.onPostClick(v.et_post.text!!.toString())
            //dismiss()
        }

        return v
    }

    interface BottomSheetListenerTirade {
        fun onPickVideo()
        fun onPickImage()
        fun onPostClick(text: String)
    }

    override fun onStart() {
        super.onStart()
        val sheetContainer = requireView().parent as? ViewGroup ?: return
        sheetContainer.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mBottomSheetListener = context as BottomSheetListenerTirade?
        }
        catch (e: ClassCastException){
            throw ClassCastException(context.toString())
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), theme).apply {
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.peekHeight = (BottomSheetBehavior.PEEK_HEIGHT_AUTO)
        }
    }
}