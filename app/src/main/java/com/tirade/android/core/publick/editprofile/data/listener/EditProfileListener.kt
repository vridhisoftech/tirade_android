package com.tirade.android.core.publick.editprofile.data.listener

interface EditProfileListener {
    fun onFailure(statusMessage: String)
    fun onCategoryClick(adapterPosition: Int)
    fun toastShow(message: String)
    fun onSuccess(message: String)
    fun onEditProfile()
    fun onCameraClick()
    fun onEditHobbies()
    fun onEditFavImages()
    fun onEditBiography()
    fun onAddPeople()
}