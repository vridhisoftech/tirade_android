package com.tirade.android.core.publick.editprofile.data.viewmodel


import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.publick.editprofile.data.listener.EditProfileListener
import com.tirade.android.core.publick.editprofile.data.model.EditProfileResponse
import com.tirade.android.core.publick.editprofile.data.repo.EditProfileRepo
import com.tirade.android.network.Connection
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class EditProfileViewModel(private val repository: EditProfileRepo) : ViewModel() {

    var listner: EditProfileListener? = null

    private var dataList: ArrayList<EditProfileResponse> = ArrayList()
    private val mutablePostList: MutableLiveData<ArrayList<EditProfileResponse>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)
    var name: ObservableField<String> = ObservableField("")
    var gender: ObservableField<String> = ObservableField("")
    var dob: ObservableField<String> = ObservableField("")
    var profileImg: ObservableField<String> = ObservableField("")
    var education: ObservableField<String> = ObservableField("")
    var where_born: ObservableField<String> = ObservableField("")
    var where_live: ObservableField<String> = ObservableField("")
    var where_work: ObservableField<String> = ObservableField("")
    var city_live: ObservableField<String> = ObservableField("")
    var sport: ObservableField<String> = ObservableField("")
    var music: ObservableField<String> = ObservableField("")
    var singer: ObservableField<String> = ObservableField("")
    var hobby: ObservableField<String> = ObservableField("")
    var hero: ObservableField<String> = ObservableField("")
    var job: ObservableField<String> = ObservableField("")
    var interest: ObservableField<String> = ObservableField("")

    var animalLover: ObservableField<String> = ObservableField("")
    var band: ObservableField<String> = ObservableField("")
    var beliveInlife: ObservableField<String> = ObservableField("")
    var dreamCar: ObservableField<String> = ObservableField("")
    var dreamVacation: ObservableField<String> = ObservableField("")
    var earning: ObservableField<String> = ObservableField("")
    var favouriteColor: ObservableField<String> = ObservableField("")
    var food: ObservableField<String> = ObservableField("")
    var makesYouAngry: ObservableField<String> = ObservableField("")
    var makesYouHappy: ObservableField<String> = ObservableField("")
    var movies: ObservableField<String> = ObservableField("")
    var sadOrHappy: ObservableField<String> = ObservableField("")
    var team: ObservableField<String> = ObservableField("")
    var file: ObservableField<File> = ObservableField()

    fun getProjectList(): LiveData<ArrayList<EditProfileResponse>>? {
        val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_id

        if (!Connection.isConnected()) {
            listner?.toastShow("Check Internet Connection!")
            return null
        }

        progress.set(View.VISIBLE)

        Coroutines.main {
            try {
                // create a map of data to pass along
                val userId: RequestBody = createRequestBody(/*userId.toString()*/"132")

                var params: HashMap<String, RequestBody> = HashMap()
                params["user_id"] = userId

                val response = repository.fetchData(params)
                if (response.getProfileData.isNotEmpty()) {
                    listner?.onSuccess("Done")
                }
                response.getProfileData.let {
                    dataList = it as ArrayList<EditProfileResponse>
                    mutablePostList.postValue(dataList)//notify observable
                    progress.set(View.GONE)
                    return@main
                }
                progress.set(View.GONE)

                return@main
            } catch (e: ApiException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            }
        }

        return mutablePostList
    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }

    fun save() {
        //TODO api impl for post data

        return

        val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user?.user_id

        file.set(File(profileImg.get()!!))

        if (!Connection.isConnected()) {
            listner?.toastShow("Check Internet Connection!")
            return
        }

        progress.set(View.VISIBLE)

        Coroutines.main {
            try {

                val file = file.get()!!
                // Parsing any Media type file
                val requestFile: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
                val fileBody: MultipartBody.Part = MultipartBody.Part.createFormData("post_image", file.name, requestFile)

                // create a map of data to pass along
                val userId: RequestBody = createRequestBody(userId.toString())

                var name: RequestBody = createRequestBody(name.get()!!)
                var gender: RequestBody = createRequestBody(gender.get()!!)
                var dob: RequestBody = createRequestBody(dob.get()!!)
                var education: RequestBody = createRequestBody(education.get()!!)
                var where_born: RequestBody = createRequestBody(where_born.get()!!)
                var where_live: RequestBody = createRequestBody(where_live.get()!!)
                var where_work: RequestBody = createRequestBody(where_work.get()!!)
                var city_live: RequestBody = createRequestBody(city_live.get()!!)
                var sport: RequestBody = createRequestBody(sport.get()!!)
                var music: RequestBody = createRequestBody(music.get()!!)
                var singer: RequestBody = createRequestBody(singer.get()!!)
                var hobby: RequestBody = createRequestBody(hobby.get()!!)
                var hero: RequestBody = createRequestBody(hero.get()!!)
                var job: RequestBody = createRequestBody(job.get()!!)
                var interest: RequestBody = createRequestBody(interest.get()!!)
                var status: RequestBody = createRequestBody("true")

                var animalLover: RequestBody = createRequestBody(animalLover.get()!!)
                var band: RequestBody = createRequestBody(band.get()!!)
                var beliveInlife: RequestBody = createRequestBody(beliveInlife.get()!!)
                var dreamCar: RequestBody = createRequestBody(dreamCar.get()!!)
                var dreamVacation: RequestBody = createRequestBody(dreamVacation.get()!!)
                var earning: RequestBody = createRequestBody(earning.get()!!)
                var favouriteColor: RequestBody = createRequestBody(favouriteColor.get()!!)
                var food: RequestBody = createRequestBody(food.get()!!)
                var makesYouAngry: RequestBody = createRequestBody(makesYouAngry.get()!!)
                var makesYouHappy: RequestBody = createRequestBody(makesYouHappy.get()!!)
                var movies: RequestBody = createRequestBody(movies.get()!!)
                var sadOrHappy: RequestBody = createRequestBody(sadOrHappy.get()!!)
                var team: RequestBody = createRequestBody(team.get()!!)

                var params: HashMap<String, RequestBody> = HashMap()
                params["user_id"] = userId
                params["s1_userName"] = name
                params["s1_userGender"] = gender
                params["s1_userBirthdate"] = dob
                params["s2_userName"] = name
                params["s3_education"] = education
                params["s3_born"] = where_born
                params["s3_live"] = where_live
                params["s3_work"] = where_work
                params["s3_liveIn"] = city_live
                params["s3_status"] = status
                params["s5_team"] = team
                params["s5_music"] = music
                params["s5_band"] = band
                params["s5_hobbies"] = hobby
                params["s5_heros"] = hero
                params["s5_dreamjob"] = job
                params["s5_interested"] = interest
                params["s5_animalLover"] = animalLover
                params["s5_beliveInlife"] = beliveInlife
                params["s5_favouriteColor"] = favouriteColor
                params["s5_movies"] = movies
                params["s5_dreamCar"]  = dreamCar
                params["s5_dreamVacation"] = dreamVacation
                params["s5_earning"] = earning
                params["s5_food"] = food
                params["s5_sadOrHappy"] = sadOrHappy
                params["s5_makesYouHappy"] = makesYouHappy
                params["s5_makesYouAngry"] = makesYouAngry

                //val response = repository.postProfileDataToServer(params, fileBody)
                /*if (response.status == 1) {
                    listner?.onSuccess(response.message)
                }*/

                progress.set(View.GONE)

            } catch (e: Exception) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            }
        }

    }

    fun onEditProfile() {
        listner?.onEditProfile()
    }

    fun onCameraClick() {
        listner?.onCameraClick()
    }

    fun onEditHobbies() {
        listner?.onEditHobbies()
    }

    fun onEditFavImages() {
        listner?.onEditFavImages()
    }

    fun onEditBiography() {
        listner?.onEditBiography()
    }

    fun onAddPeople() {
        listner?.onAddPeople()
    }
}