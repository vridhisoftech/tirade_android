package com.tirade.android.core.publick.editprofile.data.model

data class GetProfileData(
    val Biography: ArrayList<Biography>,
    val Hobbies: ArrayList<Hobby>,
    val addPeople: ArrayList<AddPeople>,
    val editProfile: ArrayList<EditProfile>,
    val favouriteImg: ArrayList<FavouriteImg>,
    val message: String,
    val profile_id: String,
    val status: String
)