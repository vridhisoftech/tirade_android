package com.tirade.android.core.trashtalk.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tirade.android.core.publick.madskills.data.repo.MadSkillRepo
import com.tirade.android.core.publick.madskills.data.viewmodels.MadSkillFormViewModel
import com.tirade.android.core.publick.visionboard.data.repo.VisionboardRepo
import com.tirade.android.core.publick.visionboard.data.viewmodel.VisionBoardViewModel
import com.tirade.android.core.trashtalk.debate.data.repo.DebateRepo
import com.tirade.android.core.trashtalk.debate.data.viewmodel.DebateViewModel
import com.tirade.android.core.trashtalk.forums.data.repo.ForumsListRepo
import com.tirade.android.core.trashtalk.forums.data.repo.ForumsRepo
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumCommentActivityViewModel
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumsDetailsViewModel
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumsListViewModel
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumsViewModel
import com.tirade.android.core.trashtalk.home.data.repo.CommentRepo
import com.tirade.android.core.trashtalk.home.data.repo.TrashTalkHomeRepository
import com.tirade.android.core.trashtalk.home.data.viewmodel.CommentActivityViewModel
import com.tirade.android.core.trashtalk.home.data.viewmodel.TrashTalkHomeViewModel
import com.tirade.android.core.trashtalk.joke.data.repo.JokesRepo
import com.tirade.android.core.trashtalk.joke.data.viewmodel.JokesViewModel
import com.tirade.android.core.trashtalk.notification.data.repo.TrashNotificationRepo
import com.tirade.android.core.trashtalk.notification.data.viewmodel.TrashNotificationViewModel
import com.tirade.android.core.trashtalk.shame.data.repo.ShameRepo
import com.tirade.android.core.trashtalk.shame.data.viewmodel.ShameViewModel
import com.tirade.android.core.trashtalk.shout.data.viewmodel.ShoutViewModel
import com.tirade.android.core.trashtalk.trash.data.repo.TrashRepo
import com.tirade.android.core.trashtalk.trash.data.viewmodel.TrashViewModel
import com.tirade.android.core.trashtalk.wblower.data.repo.WblowerRepo
import com.tirade.android.core.trashtalk.wblower.data.viewmodel.WblowerViewModel

@Suppress("UNCHECKED_CAST")
class TrashTalkModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(TrashTalkHomeViewModel::class.java)) {
            TrashTalkHomeViewModel(TrashTalkHomeRepository.getInstance()) as T
        } else return if (modelClass.isAssignableFrom(CommentActivityViewModel::class.java)) {
            CommentActivityViewModel(CommentRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(ShoutViewModel::class.java)) {
            ShoutViewModel(
                TrashRepo.getInstance()
            ) as T
        }else return if (modelClass.isAssignableFrom(TrashViewModel::class.java)) {
            TrashViewModel(TrashRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(MadSkillFormViewModel::class.java)) {
            MadSkillFormViewModel(MadSkillRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(DebateViewModel::class.java)) {
            DebateViewModel(DebateRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(WblowerViewModel::class.java)) {
            WblowerViewModel(WblowerRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(JokesViewModel::class.java)) {
            JokesViewModel(JokesRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(ForumsViewModel::class.java)) {
            ForumsViewModel(ForumsRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(ForumsListViewModel::class.java)) {
            ForumsListViewModel(ForumsListRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(ForumCommentActivityViewModel::class.java)) {
            ForumCommentActivityViewModel(ForumsRepo.getInstance()) as T
        }
        else return if (modelClass.isAssignableFrom(ForumsDetailsViewModel::class.java)) {
            ForumsDetailsViewModel(ForumsListRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(ShameViewModel::class.java)) {
            ShameViewModel(ShameRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(VisionBoardViewModel::class.java)) {
            VisionBoardViewModel(VisionboardRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(TrashNotificationViewModel::class.java)) {
            TrashNotificationViewModel(TrashNotificationRepo.getInstance()) as T
        } else {
            //TODO for others view model repository
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

}