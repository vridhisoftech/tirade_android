package com.tirade.android.core.trashtalk.shame.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.tirade.android.R
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.shame.data.viewmodel.ShameViewModel
import com.tirade.android.databinding.ActivityShameBinding
import kotlinx.android.synthetic.main.activity_shame.*

class ShameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShameBinding
    private lateinit var viewModel: ShameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shame)
        viewModel = ViewModelProvider(this, TrashTalkModelFactory()).get(ShameViewModel::class.java)
        binding.viewModel = viewModel
        logo.setOnClickListener{
            finish()
        }
    }
}
