package com.tirade.android.core.trashtalk.home.data.interfaces

interface CommentListener {

    fun getComment(comment:String)

    fun toastShow(message: String)

}