package com.tirade.android.core.trashtalk.forums.data.model

data class ForumData(
    val name:String,
    val user_image: String,
    val status: String,
    val message: String,
    val images: ArrayList<ImagesModel>,
    val videos: ArrayList<VideosModel>,
   val notes:ArrayList<NotesModel>
)