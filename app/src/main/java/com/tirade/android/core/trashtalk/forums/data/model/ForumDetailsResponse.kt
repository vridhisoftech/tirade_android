package com.tirade.android.core.trashtalk.forums.data.model

data class ForumDetailsResponse (
    val data : List<ForumData>
)