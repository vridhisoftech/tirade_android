package com.tirade.android.core.trashtalk.forums.data.model

data class ForumActionPayload(
    var user_id: String? = null,
    var forum_id: String? = null,
    var value: String? = null
)