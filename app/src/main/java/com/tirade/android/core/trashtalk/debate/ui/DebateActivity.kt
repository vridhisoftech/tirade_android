package com.tirade.android.core.trashtalk.debate.ui

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.SparseBooleanArray
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.iceteck.silicompressorr.SiliCompressor
import com.nbsp.materialfilepicker.MaterialFilePicker
import com.nbsp.materialfilepicker.ui.FilePickerActivity
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.trashtalk.debate.data.model.Invite
import com.tirade.android.core.trashtalk.debate.data.model.InviteResponse
import com.tirade.android.core.trashtalk.debate.data.model.UserData
import com.tirade.android.core.trashtalk.debate.data.viewmodel.DebateViewModel
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.trash.data.listener.TrashListener
import com.tirade.android.databinding.ActivityDebateBinding
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import com.tirade.android.utils.toast
import kotlinx.android.synthetic.main.activity_debate.*
import java.io.File
import java.util.regex.Pattern

class DebateActivity : AppCompatActivity(), TrashListener {

    private var dataList: ArrayList<UserData>? = null
    private lateinit var binding: ActivityDebateBinding
    private lateinit var viewModel: DebateViewModel
    private var chooseUserPopUp: PopupWindow? = null

    companion object {
        private val PERMISSION_CODE = 1000
        private val IMAGE_PICK_CODE = 1001
        private val REQUEST_PICK_VIDEO = 1002
        private val REQUEST_PICK_AUDIO = 1003
        private val REQUEST_PICK_TXT = 1004
        private val REQUEST_PICK_PDF = 1005
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_debate)
        viewModel = ViewModelProvider(this, TrashTalkModelFactory()).get(DebateViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.listner = this

        dataList = intent.extras!!.getParcelableArrayList<UserData>("bundle")
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    this.toast("Permission denied")
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (data != null){
                when(requestCode){
                    IMAGE_PICK_CODE -> {
                        val image = data.data
                        val projection = arrayOf(MediaStore.Images.Media.DATA)
                        val cursor = contentResolver.query(image!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()

                        val fileDirSave = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() +  "/TIRADE_IMAGES")
                        val filePath = SiliCompressor.with(this).compress(mediaPath, fileDirSave)
                        viewModel.post_image.set(filePath)

                    }

                    REQUEST_PICK_VIDEO -> {
                        val video = data.data
                        val projection = arrayOf(MediaStore.Video.Media.DATA)
                        val cursor = contentResolver.query(video!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()
                        viewModel.post_video.set(mediaPath)
                    }

                    REQUEST_PICK_AUDIO -> {
                        val audio = data.data
                        val projection = arrayOf(MediaStore.Audio.Media.DATA)
                        val cursor = contentResolver.query(audio!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()
                        viewModel.post_audio.set(mediaPath)
                    }

                    REQUEST_PICK_TXT -> {
                        val textFileUri = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                        viewModel.post_file.set(textFileUri)
                    }

                    REQUEST_PICK_PDF -> {
                        val textFileUri = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                        viewModel.post_file.set(textFileUri)
                    }
                }
            }
        }
    }


    private fun checkPermissionAndPickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery()
            }
        } else{
            //system OS is < Marshmallow
            pickImageFromGallery()
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select a file"),
            IMAGE_PICK_CODE
        )
    }

    private fun pickVideoFromGallery(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "video/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select a file"),
            REQUEST_PICK_VIDEO
        )
    }

    private fun pickAudioFromFileManager(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
        //intent.action = Intent.ACTION_GET_CONTENT
        //intent.type = "audio/mpeg"
        startActivityForResult(
            Intent.createChooser(intent, "Select a file"),
            REQUEST_PICK_AUDIO
        )
    }

    private fun pickPdfFromFileManager(){
        MaterialFilePicker()
            .withActivity(this)
            .withRequestCode(REQUEST_PICK_PDF)
            .withHiddenFiles(true)
            .withFilter(Pattern.compile(".*\\.pdf$"))
            .withTitle("Select PDF file")
            .start()
        //val intent = Intent()
        //intent.action = Intent.ACTION_OPEN_DOCUMENT
        //intent.addCategory(Intent.CATEGORY_OPENABLE)
        //intent.type = "*/*"
        //val extraMimeTypes = arrayOf("application/pdf", "application/doc")
        //intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes)
        //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        //startActivityForResult(intent, REQUEST_PICK_PDF)
    }

    private fun pickTextFromFileManager(){
        MaterialFilePicker()
            .withActivity(this)
            .withRequestCode(REQUEST_PICK_TXT)
            .withHiddenFiles(true)
            .withFilter(Pattern.compile(".*\\.txt$"))
            .withTitle("Select TEXT file")
            .start()
    }

   private fun showDialogAskReasonDebate(selected: List<String>) {
        val context = this
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_ask_reason_debate)

        val editText = dialog.findViewById(R.id.et_reason) as EditText
        val cancel = dialog.findViewById(R.id.cancel) as Button
        val ok = dialog.findViewById(R.id.ok) as Button
        val progress = dialog.findViewById(R.id.progress) as ContentLoadingProgressBar

        cancel.setOnClickListener {
            dialog.dismiss()
        }

        ok.setOnClickListener {
            val reason  = editText.text
            if(reason.isEmpty()){
                this.toastShow("Please select a reason to debate.")
            }else{
                progress.visibility = View.VISIBLE
                Coroutines.main {
                    try {
                        val senderId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user?.user_id
                        val invite = Invite(sender_id = senderId, debate_reason = reason.toString(), invited_id = selected.toString())
                        val response: InviteResponse = postDataToServer(invite)
                        toastShow(response.message)
                        if(response.status == 1)
                        dialog.dismiss()
                    } catch (e: ApiException) {
                        progress.visibility = View.GONE
                        this.toastShow(e.message!!)
                    } catch (e: NoInternetException) {
                        progress.visibility = View.GONE
                        this.toastShow(e.message!!)
                    }
                }
            }
        }

        dialog.show()
    }

    suspend fun postDataToServer(params: Invite): InviteResponse {
        val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }
        return apiServices.inviteUsers(params).await()
    }

    private fun dismissPopup() {
        chooseUserPopUp?.let {
            if (it.isShowing) {
                it.dismiss()
            }
            chooseUserPopUp = null
        }
    }

    override fun onChooseToDebate() {
        dismissPopup()
        chooseUserPopUp = selectFromList()
        chooseUserPopUp?.isOutsideTouchable = true
        chooseUserPopUp?.isFocusable = true
        chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        chooseUserPopUp?.showAsDropDown(text, 0, 0, Gravity.START)
    }

    private fun selectFromList(): PopupWindow {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.debate_multi_user_selection, null)

        val adapter = CustomAdapter(this, dataList)
        val listView = view.findViewById<ListView>(R.id.listView)
        listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE
        listView.adapter = adapter

        listView.setOnItemClickListener { parent, view, position, id ->
          val userData: UserData = dataList!![position]
            userData.checked = !userData.checked!!
            adapter.notifyDataSetChanged()
        }

        val invite = view.findViewById<Button>(R.id.invite)
        invite.setOnClickListener {
            dismissPopup()
            val selected = getSelectedItems(listView)
            showDialogAskReasonDebate(selected)
        }

        return PopupWindow(
            view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private fun getSelectedItems(listView: ListView): List<String> {
        val result = ArrayList<String>()
       val checkedItems: SparseBooleanArray =  listView.checkedItemPositions
        for(i in 0 until checkedItems.size()){
            if (checkedItems.valueAt(i)) {
                val data = listView.getItemAtPosition(checkedItems.keyAt(i)) as UserData
                result.add(data.user_id!!)
            }
        }
       return result
    }

    override fun onVideo() {
        pickVideoFromGallery()
    }

    override fun onClickPdf() {
        pickPdfFromFileManager()
    }

    override fun onVoice() {
        pickAudioFromFileManager()
    }

    override fun onPhoto() {
        checkPermissionAndPickImage()
    }

    override fun onText() {
        pickTextFromFileManager()
    }

    override fun onContinue() {

    }

    override fun onFacebook() {
        shareOnSocial("https://www.facebook.com/")
    }

    override fun onGoogle() {
        shareOnSocial("https://www.google.com/")
    }

    override fun onTwitter() {
        shareOnSocial("https://twitter.com/login?lang=en")
    }

    override fun onWhatsUp() {
        shareOnSocial("https://web.whatsapp.com/")
    }

    override fun toastShow(message: String) {
        toast(message)
    }

    override fun finishActivity() {
        finish()
    }

    override fun onSuccess(message: String) {
        showConfirmationMessage(message)
    }


    private fun showConfirmationMessage(text: String) {
        val dialog = Dialog(this@DebateActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.confirm_message)

        (dialog.findViewById(R.id.message) as TextView).also {
            it.text = text
        }
        val ok = dialog.findViewById(R.id.ok) as Button
        ok.setOnClickListener {
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }

    private fun shareOnSocial(message: String){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type="text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(shareIntent, "Share Via"))
    }

}
