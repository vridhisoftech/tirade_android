package com.tirade.android.core.publick.madskills.data.model.skills

data class Data(
    val id: String,
    val image: String,
    val name: String
)