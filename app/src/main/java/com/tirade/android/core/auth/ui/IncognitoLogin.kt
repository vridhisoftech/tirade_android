package com.tirade.android.core.auth.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.iceteck.silicompressorr.SiliCompressor
import com.tirade.android.BuildConfig
import com.tirade.android.R
import com.tirade.android.core.auth.data.factory.AuthModelFactory
import com.tirade.android.core.auth.data.interfaces.AuthListener
import com.tirade.android.core.auth.data.viewmodel.IncognitoViewModel
import com.tirade.android.core.intro.LoginIntro
import com.tirade.android.core.intro.TrashTalkIntro
import com.tirade.android.databinding.ActivityIncognitoLoginBinding
import com.tirade.android.utils.Helper
import com.tirade.android.utils.goActivity
import com.tirade.android.utils.toast
import com.tirade.android.widget.BottomSheetCameraGallery
import kotlinx.android.synthetic.main.activity_incognito_login.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger


class IncognitoLogin : AppCompatActivity(), AuthListener,
    BottomSheetCameraGallery.BottomSheetListener {

    private val TAG = "IncognitoLogin"
    private var chooseImagePopUp: PopupWindow? = null
    private var chooseImageListPopUp: PopupWindow? = null
    private val CAMERA_PIC_REQUEST = 111
    private val IMAGE_PICK_CODE = 222
    private val REQUEST_PERMISSION = 100
    private val MEDIA_TYPE_IMAGE = 333
    private lateinit var mImageFileLocation: String
    private lateinit var fileUri: Uri

    private lateinit var binding: ActivityIncognitoLoginBinding
    private lateinit var viewModel: IncognitoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Helper.setFullScreen(this, window)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_incognito_login)
        viewModel =  ViewModelProvider(this@IncognitoLogin, AuthModelFactory()).get(IncognitoViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.listener = this

        setUpImojiIcon(R.drawable.imoji)

        setupPermissions()
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(this@IncognitoLogin, Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED) {
            imImoji.isEnabled = false
            ActivityCompat.requestPermissions(this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_PERMISSION
            )
        }
    }

    /**
     * Launching camera app to capture image
     */
    private fun captureImage() {
        dismissPopup()
        dismissListPopUp()
        chooseFromCameraOrGallery()
    }

    @Throws(IOException::class)
    fun createImageFile(): File? {
        Logger.getAnonymousLogger().info("Generating the image - method started")

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmSS").format(Date())
        val imageFileName = "IMAGE_$timeStamp"
        // Here we specify the environment location and the exact path where we want to save the so-created file
        val storageDirectory = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY)
        Logger.getAnonymousLogger().info("Storage directory set")

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir()

        // Here we create the file using a prefix, a suffix and a directory
        val image = File(storageDirectory, "$imageFileName.jpg")
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set")
        mImageFileLocation = image.absolutePath
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    /**
     * ------------ Helper Methods ----------------------
     */
    /**
     * Creating file uri to store image/video
     */
    private fun getOutputMediaFileUri(type: Int): Uri {
        return Uri.fromFile(
            getOutputMediaFile(
                type
            )
        )
    }

    /**
     * returning image / video
     */
    private fun getOutputMediaFile(type: Int): File? {
        // External sdcard location
        val mediaStorageDir = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY)
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create $IMAGE_DIRECTORY directory")
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val mediaFile: File
        mediaFile = if (type == MEDIA_TYPE_IMAGE) {
            File(mediaStorageDir.path + File.separator + "IMG_" + ".jpg")
        } else {
            return null
        }
        return mediaFile
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                    imImoji.isEnabled = false
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                    imImoji.isEnabled = true
                }
            }
        }
    }

    private fun setUpImojiIcon(imoji: Any) {
        Glide.with(this)
            .asBitmap().load(imoji)
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Bitmap>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    bitmap: Bitmap?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    updateBitmapImage(bitmap)
                    return false
                }
            }
            ).submit()
    }

    fun updateBitmapImage(bitmap: Bitmap?) {
        this@IncognitoLogin.runOnUiThread(java.lang.Runnable {
            imImoji.setImageBitmap(bitmap)
        })
    }

    override fun onStop() {
        super.onStop()
        dismissPopup()
        dismissListPopUp()
    }

    private fun dismissPopup() {
        chooseImagePopUp?.let {
            if (it.isShowing) {
                it.dismiss()
            }
            chooseImagePopUp = null
        }
    }

    private fun dismissListPopUp() {
        chooseImageListPopUp?.let {
            if (it.isShowing) {
                it.dismiss()
            }
            chooseImageListPopUp = null
        }
    }

    private fun chooseFromCameraOrGallery() {
        val bottomSheet = BottomSheetCameraGallery()
        bottomSheet.show(supportFragmentManager, "BottomSheetCameraGallery")
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), IMAGE_PICK_CODE)
    }

    override fun onOptionClick(text: String) {
       // toast(text)
        when (text) {
            "CAMERA" -> {
                if (Build.VERSION.SDK_INT > 21) {
                    //use this if Lollipop_Mr1 (API 22) or above
                    val intent = Intent()
                    intent.action = MediaStore.ACTION_IMAGE_CAPTURE

                    // We give some instruction to the intent to save the image
                    var photoFile: File? = null
                    try {
                        // If the createImageFile will be successful, the photo file will have the address of the file
                        photoFile = createImageFile()
                        // Here we call the function that will try to catch the exception made by the throw function
                    } catch (e: IOException) {
                        Logger.getAnonymousLogger()
                            .info("Exception error in generating the file")
                        e.printStackTrace()
                    }
                    // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
                    val outputUri: Uri = FileProvider.getUriForFile(
                        this, BuildConfig.APPLICATION_ID.toString() + ".provider",
                        photoFile!!
                    )
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri)

                    // The following is a new line with a trying attempt
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    Logger.getAnonymousLogger().info("Calling the camera App by intent")

                    // The following strings calls the camera app and wait for his file in return.
                    startActivityForResult(intent, CAMERA_PIC_REQUEST)
                } else {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)

                    // start the image capture Intent
                    startActivityForResult(intent, CAMERA_PIC_REQUEST)
                }
            }
            "GALLERY" -> {
                pickImageFromGallery()
            }
        }

    }

    private fun chooseImageCameraOrFace(): PopupWindow {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.image_selection_dialog, null)
        val viewFace = view.findViewById<ImageView>(R.id.face)
        viewFace.setOnClickListener {
            dismissPopup()
            dismissListPopUp()
            chooseImageListPopUp = chooseFaceImage()
            chooseImageListPopUp?.isOutsideTouchable = true
            chooseImageListPopUp?.isFocusable = true
            chooseImageListPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseImageListPopUp?.showAsDropDown(imImoji, 0, 0, Gravity.START)
        }

        val viewCamera = view.findViewById<ImageView>(R.id.camera)
        viewCamera.setOnClickListener {
            Log.i(TAG, "Camera Clicked")
            val permissionCam = ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            )
            val permissionStorage = ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )

            if (permissionCam == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission to storage accepted")
                captureImage()
            } else if (permissionCam == PackageManager.PERMISSION_DENIED || permissionStorage == PackageManager.PERMISSION_DENIED) {
                Log.i(TAG, "Permission to storage always denied")
                val message: String = "We noticed you have disabled permission.\n " +
                        "Please enable  CAMERA and STORAGE permission from,\n" +
                        "Tirade Application settings\n"
                this.toast(message, true)
                setupPermissions()
            } else {
                setupPermissions()
            }

        }
        return PopupWindow(
            view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private fun chooseFaceImage(): PopupWindow {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.image_selection_faces, null)
        val im1 = view.findViewById<ImageView>(R.id.im1)
        im1.setOnClickListener {
            setFaceImage(R.drawable.img1, "img1")
            dismissListPopUp()
        }
        val im2 = view.findViewById<ImageView>(R.id.im2)
        im2.setOnClickListener {
            setFaceImage(R.drawable.img2, "img2")
            dismissListPopUp()
        }
        val im3 = view.findViewById<ImageView>(R.id.im3)
        im3.setOnClickListener {
            setFaceImage(R.drawable.img3, "img3")
            dismissListPopUp()
        }
        val im4 = view.findViewById<ImageView>(R.id.im4)
        im4.setOnClickListener {
            setFaceImage(R.drawable.img4, "img4")
            dismissListPopUp()
        }
        val im5 = view.findViewById<ImageView>(R.id.im5)
        im5.setOnClickListener {
            setFaceImage(R.drawable.img5, "img5")
            dismissListPopUp()
        }
        val im6 = view.findViewById<ImageView>(R.id.im6)
        im6.setOnClickListener {
            setFaceImage(R.drawable.img6, "img6")
            dismissListPopUp()
        }
        val im7 = view.findViewById<ImageView>(R.id.im7)
        im7.setOnClickListener {
            setFaceImage(R.drawable.img7, "img7")
            dismissListPopUp()
        }
        val im8 = view.findViewById<ImageView>(R.id.im8)
        im8.setOnClickListener {
            setFaceImage(R.drawable.img8, "img8")
            dismissListPopUp()
        }
        val im9 = view.findViewById<ImageView>(R.id.im9)
        im9.setOnClickListener {
            setFaceImage(R.drawable.img9, "img9")
            dismissListPopUp()
        }
        val im10 = view.findViewById<ImageView>(R.id.im10)
        im10.setOnClickListener {
            setFaceImage(R.drawable.img10, "img10")
            dismissListPopUp()
        }
        val im11 = view.findViewById<ImageView>(R.id.im11)
        im11.setOnClickListener {
            setFaceImage(R.drawable.img11, "img11")
            dismissListPopUp()
        }
        val im12 = view.findViewById<ImageView>(R.id.im12)
        im12.setOnClickListener {
            setFaceImage(R.drawable.img12, "img12")
            dismissListPopUp()
        }
        val im13 = view.findViewById<ImageView>(R.id.im13)
        im13.setOnClickListener {
            setFaceImage(R.drawable.img13, "img13")
            dismissListPopUp()
        }
        val im14 = view.findViewById<ImageView>(R.id.im14)
        im14.setOnClickListener {
            setFaceImage(R.drawable.img14, "img14")
            dismissListPopUp()
        }
        val im15 = view.findViewById<ImageView>(R.id.im15)
        im15.setOnClickListener {
            setFaceImage(R.drawable.img15, "img15")
            dismissListPopUp()
        }
        val im16 = view.findViewById<ImageView>(R.id.im16)
        im16.setOnClickListener {
            setFaceImage(R.drawable.img16, "img16")
            dismissListPopUp()
        }
        val im17 = view.findViewById<ImageView>(R.id.im17)
        im17.setOnClickListener {
            setFaceImage(R.drawable.img17, "img17")
            dismissListPopUp()
        }
        val im18 = view.findViewById<ImageView>(R.id.im18)
        im18.setOnClickListener {
            setFaceImage(R.drawable.img18, "img18")
            dismissListPopUp()
        }
        val im19 = view.findViewById<ImageView>(R.id.im19)
        im19.setOnClickListener {
            setFaceImage(R.drawable.img19, "img19")
            dismissListPopUp()
        }
        val im20 = view.findViewById<ImageView>(R.id.im20)
        im20.setOnClickListener {
            setFaceImage(R.drawable.img20, "img20")
            dismissListPopUp()
        }

        return PopupWindow(
            view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private fun setFaceImage(drawable: Int, name: String) {
        Log.i(TAG, "Face Clicked")

        setUpImojiIcon(drawable)

        var bitmap = BitmapFactory.decodeResource(resources, drawable)
        val dirPath = File(
            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/TIRADE_IMAGES"
        )

        val imageFile = File(dirPath, "${name}.png")

        try {
            if (!dirPath.isDirectory) {
                dirPath.mkdirs()
            }
            imageFile.createNewFile()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            val outStream = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
            outStream.flush()
            outStream.close()
        } catch (e: FileNotFoundException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        val sdPath = dirPath.absolutePath.toString() + "/" + "${name}.png"
        Log.i("hiya", "Your IMAGE ABSOLUTE PATH:-$sdPath")

        viewModel.profileImg.set(sdPath)

        val temp = File(sdPath)
        if (!temp.exists()) {
            Log.e("file", "no image file at location :$sdPath")
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == IMAGE_PICK_CODE) {
                    if (data != null) {
                        val image = data.data
                        val projection = arrayOf(MediaStore.Images.Media.DATA)
                        val cursor = contentResolver.query(image!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()

                        val fileDirSave = File(
                            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                                .toString() + "/TIRADE_IMAGES"
                        )
                        val filePath = SiliCompressor.with(this).compress(mediaPath, fileDirSave)
                        setUpImojiIcon(filePath)
                        viewModel.profileImg.set(filePath)
                    }
            } else if (requestCode == CAMERA_PIC_REQUEST) {
                    if (Build.VERSION.SDK_INT > 21) {
                        setUpImojiIcon(mImageFileLocation)
                        val fileDirSave = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() +  "/TIRADE_IMAGES")
                        val filePath = SiliCompressor.with(this).compress(mImageFileLocation, fileDirSave)
                        viewModel.profileImg.set(filePath)
//                        viewModel.profileImg.set(mImageFileLocation)
                    } else {
                        setUpImojiIcon(fileUri)
//                        viewModel.profileImg.set(fileUri.path)
                        val fileDirSave = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() +  "/TIRADE_IMAGES")
                        val filePath = SiliCompressor.with(this).compress(fileUri.path, fileDirSave)
                        viewModel.profileImg.set(filePath)
                    }
            }
        }
    }

    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File((this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)).toString() + IMAGE_DIRECTORY)
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        try {
            val f = File(
                wallpaperDirectory,
                ((Calendar.getInstance().timeInMillis).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    companion object {
        private val IMAGE_DIRECTORY = "/TIRADE_IMAGES"
    }

    override fun onSuccess(message: String) {
        this.toast(message)
        this.goActivity(TrashTalkIntro::class.java)
        finish()
    }

    override fun onFailure(message: String) {
        this.toast(message)
    }

    override fun onImageSelection() {
        dismissPopup()
        dismissListPopUp()
        chooseImagePopUp = chooseImageCameraOrFace()
        chooseImagePopUp?.isOutsideTouchable = true
        chooseImagePopUp?.isFocusable = true
        chooseImagePopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        chooseImagePopUp?.showAsDropDown(imImoji, 0, 0, Gravity.START)
    }

    override fun openDatePicker() {
        //TODO : Nothing
    }


    override fun onBackPressed() {
        this.goActivity(LoginIntro::class.java)
        super.onBackPressed()
    }

}


