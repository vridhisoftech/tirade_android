package com.tirade.android.core.trashtalk.notification.data.viewmodel

import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.trashtalk.notification.data.model.Data
import com.tirade.android.core.trashtalk.notification.data.model.Notification
import com.tirade.android.core.trashtalk.notification.data.repo.TrashNotificationRepo
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException

class TrashNotificationViewModel(private val repository: TrashNotificationRepo): ViewModel() {

    private var dataList: ArrayList<Data> = ArrayList()
    private val mutablePostList: MutableLiveData<ArrayList<Data>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)
    var noData: ObservableInt = ObservableInt(View.GONE)

    fun getProjectList(): LiveData<ArrayList<Data>>? {
        progress.set(View.VISIBLE)
        noData.set(View.GONE)
        Coroutines.main {
            try {
                val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user?.user_id
                val payload = Notification(user_id = userId)
                val response = repository.fetchData(payload)

                response.let {
                    dataList = response.data as ArrayList<Data>
                    mutablePostList.postValue(dataList)
                    //repository.saveDataForOffline(dataList)//save data for offline
                    progress.set(View.GONE)
                    if(dataList.size == 0){
                        noData.set(View.VISIBLE)
                    }
                    return@main
                }
            } catch (e: ApiException) {
                //listListener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                //listListener?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutablePostList
    }

}