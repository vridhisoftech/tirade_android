package com.tirade.android.core.trashtalk.home.ui.activitiy

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.home.data.interfaces.CommentListener
import com.tirade.android.core.trashtalk.home.data.interfaces.TrashTalkHomeListener
import com.tirade.android.core.trashtalk.home.data.model.home.AllComments
import com.tirade.android.core.trashtalk.home.data.viewmodel.CommentActivityViewModel
import com.tirade.android.core.trashtalk.home.ui.adapter.CommentsAdapter
import com.tirade.android.databinding.ActivityCommentBinding
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.toast
import kotlinx.android.synthetic.main.activity_comment.view.*

class CommentActivity : AppCompatActivity(), CommentListener {

    private var scroll: ScrollView? = null
    private var commentsAdapter: CommentsAdapter? = null
    private lateinit var binding: ActivityCommentBinding
    private lateinit var viewModel: CommentActivityViewModel
    var listener: TrashTalkHomeListener? = null
    var commentFlag = false

    var postComments: ArrayList<AllComments>? = null
    var postId: String? = null
    var postType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_comment)
        viewModel = ViewModelProvider(
            this,
            TrashTalkModelFactory()
        ).get(CommentActivityViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.listner = this
        scroll = findViewById(R.id.scrollView)
        callMain()
    }

    private fun callMain() {
        binding.root.rlRightView.visibility = View.GONE
        postId = intent.extras!!.getString("postId")
        postType = intent.extras!!.getString("type")
        postComments = intent.getParcelableArrayListExtra<AllComments>("comments")

        Log.v("POstType", postType!!)

        setRecyclerView(postComments!!)
//        subscribeDataCallBack()
        viewModel.post_id_fk.set(postId)
        viewModel.auhtor_id_fk.set((PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user!!.user_id))
    }

    private fun setRecyclerView(dataList: ArrayList<AllComments>) {
        commentsAdapter = CommentsAdapter(this)
        val categoryLinearLayoutManager = LinearLayoutManager(this)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.root.recycler_view.layoutManager = categoryLinearLayoutManager
        commentsAdapter!!.setAppList(dataList)
        binding.root.recycler_view.adapter = commentsAdapter

    }

    override fun getComment(comment: String) {
        binding.root.rlRightView.visibility = View.GONE
//       subscribeDataCallBack()
        val temp =
            AllComments(
                postId,
                "0",
                comment,
                "",
                "",
                ""
            )

        postComments!!.add(temp)
        setRecyclerView(postComments!!)
        commentsAdapter!!.notifyDataSetChanged()

        commentFlag = true
        binding.root.sendMessage.setText("")
    }


    override fun toastShow(message: String) {
        toast(message)
    }

    override fun onBackPressed() {
        if (commentFlag) {
            val intent = Intent()
            intent.putExtra("type", postType)
            setResult(2, intent)
        }
        finish()
        super.onBackPressed()
    }
}