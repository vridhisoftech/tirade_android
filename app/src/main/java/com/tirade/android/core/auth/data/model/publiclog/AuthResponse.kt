package com.tirade.android.core.auth.data.model.publiclog

data class AuthResponse(
    val status : Int,
    val message : String,
    val user : User
)