package com.tirade.android.core.trashtalk.debate.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserData(
    var post_id: Int? = null,
    var user_id: String? = null,
    var username: String? = null,
    var category: String? = null,
    var user_profile: String? = null,
    var checked: Boolean? = null
) : Parcelable