package com.tirade.android.core.intro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.widget.ProgressBar
import com.tirade.android.R
import com.tirade.android.utils.Helper

class IntroPlayerActivity : AppCompatActivity() {

    private lateinit var webView: WebView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro_player)
        Helper.setFullScreen(this, window)

        webView = findViewById(R.id.webview)
        progressBar = findViewById(R.id.progressBar)
        val url = intent.getStringExtra("url")
        Helper.playerView(webView, progressBar, url)
    }

    override fun onPause() {
        super.onPause()
        webView.onPause()
    }

    override fun onResume() {
        super.onResume()
        webView.onResume()
    }
}
