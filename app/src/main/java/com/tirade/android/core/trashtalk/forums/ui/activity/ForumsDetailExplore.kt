package com.tirade.android.core.trashtalk.forums.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.tirade.android.R
import com.tirade.android.core.trashtalk.forums.data.model.Data
import kotlinx.android.synthetic.main.activity_forums_detail_explore.*

class ForumsDetailExplore: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forums_detail_explore)
        val forumData=  intent.extras!!.getParcelableArrayList<Data>("forumData")

        Log.v("ForumData",forumData.toString())

        etAudience.setText(forumData!![0].fourmAudience.toString())
        etGoal.setText(forumData[0].fourmGoal.toString())
        etFollows.setText(forumData[0].followLike.toString())
        etForumName.setText(forumData[0].fourmName.toString())
        etAddUrl.setText(forumData[0].addUrl.toString())
        etMyWebsite.setText(forumData[0].myWebsite.toString())
        etSocialLinks.setText(forumData[0].socialLink.toString())
        profileName.text = forumData[0].profileName.toString()

        back.setOnClickListener { finish() }
    }

}
