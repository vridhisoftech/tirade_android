package com.tirade.android.core.trashtalk.forums.data.repo

import com.tirade.android.core.trashtalk.forums.data.model.ForumDetailsResponse
import com.tirade.android.core.trashtalk.forums.data.model.ForumsListResponse
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.RequestBody

class ForumsListRepo {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun fetchData(): ForumsListResponse {
        return apiServices.getForumsHomeListData().await()
    }
    suspend fun fetchForumData(userId: RequestBody): ForumDetailsResponse {
        return apiServices.getForumsDetailData(userId).await()
    }

    companion object {
        fun getInstance(): ForumsListRepo {
            val mInstance: ForumsListRepo by lazy { ForumsListRepo() }
            return mInstance
        }
    }
}