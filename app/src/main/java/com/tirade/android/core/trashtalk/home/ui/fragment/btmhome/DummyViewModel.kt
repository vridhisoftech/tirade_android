package com.tirade.android.core.trashtalk.home.ui.fragment.btmhome

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DummyViewModel : ViewModel() {
    private val _text = MutableLiveData<String>().apply {
        value = "This is send Dummy"
    }
    val text: LiveData<String> = _text
}
