package com.tirade.android.core.trashtalk.home.data.model.home

import android.os.Parcel
import android.os.Parcelable

 class SDisagree() :Parcelable {
    var s_disagree: String? = null
    var name: String? = null
    var user_img: String? = null


    constructor(parcel: Parcel) : this() {
        name =parcel.readString()
        s_disagree =parcel.readString()
        user_img =parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(s_disagree)
        parcel.writeString(user_img)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SDisagree> {
        override fun createFromParcel(parcel: Parcel): SDisagree {
            return SDisagree(
                parcel
            )
        }

        override fun newArray(size: Int): Array<SDisagree?> {
            android.util.Log.v("ArraySize",""+size)
            return arrayOfNulls<SDisagree>(size)

        }
    }
}
