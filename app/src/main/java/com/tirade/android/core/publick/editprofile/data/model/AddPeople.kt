package com.tirade.android.core.publick.editprofile.data.model

data class AddPeople(
    val s2_userImage: String,
    val s2_userName: String
)