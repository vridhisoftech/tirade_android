package com.tirade.android.core.publick.madskills.data.model.topskills

data class RateTopSkill(
    val actor: List<Any>,
    val artist: List<Artist>,
    val dancer: List<Any>,
    val doctor: List<Any>,
    val media: List<Any>,
    val singer: List<Any>,
    val sports: List<Any>
)