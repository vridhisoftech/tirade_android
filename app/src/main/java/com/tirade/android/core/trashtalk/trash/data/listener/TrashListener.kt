package com.tirade.android.core.trashtalk.trash.data.listener

interface TrashListener {
    fun onChooseToDebate()
    fun onVideo()
    fun onClickPdf()
    fun onVoice()
    fun onPhoto()
    fun onText()
    fun onContinue()
    fun onFacebook()
    fun onGoogle()
    fun onTwitter()
    fun onWhatsUp()
    fun toastShow(message: String)
    fun finishActivity()
    fun onSuccess(message: String)
}