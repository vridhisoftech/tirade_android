package com.tirade.android.core.trashtalk.forums.ui.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.R
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumsListActivityListener
import com.tirade.android.core.trashtalk.forums.data.model.Data
import com.tirade.android.core.trashtalk.forums.ui.activity.ForumsDetailsActivity
import com.tirade.android.databinding.ForumsListItemBinding
import com.tirade.android.dialog.video.PlayerActivity
import com.tirade.android.utils.toast
import java.util.*
import kotlin.collections.ArrayList

class ForumsListAdapter(private var listener: ForumsListActivityListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() ,Filterable{
    private val mCategoryList = ArrayList<Data>()
    private var mFilteredList = ArrayList<Data>()

    fun setAppList(categoryModel: ArrayList<Data>) {
        mFilteredList.clear()
        mCategoryList.addAll(categoryModel)

        mFilteredList=mCategoryList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mFilteredList.size)
        return mFilteredList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mFilteredList[position]
        (holder as RecyclerViewHolder).bind(feeder)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = ForumsListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: ForumsListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Data) {
            applicationBinding.appModelNews = feed

            when {
                feed.fourmImage!!.isNotEmpty() -> {
                    applicationBinding.image.visibility = View.VISIBLE
                    applicationBinding.pdf.visibility = View.GONE
                    applicationBinding.imPlay.visibility = View.GONE
                    applicationBinding.image.setOnClickListener {
                        val intent = Intent(itemView.context, ForumsDetailsActivity::class.java)
                        intent.putExtra("type", "image")
                        intent.putExtra("forumData",mFilteredList)
                        intent.putExtra("userId", feed.user_id)
                        itemView.context.startActivity(intent)
                    }

                }
                feed.forumVideo!!.isNotEmpty() -> {
                    applicationBinding.image.visibility = View.VISIBLE
                    applicationBinding.imPlay.visibility = View.VISIBLE
                    applicationBinding.pdf.visibility = View.GONE
                    applicationBinding.imPlay.setOnClickListener {
                        val intent = Intent(itemView.context, ForumsDetailsActivity::class.java)
                        intent.putExtra("type", "video")
                        intent.putExtra("forumData",mFilteredList)
                        intent.putExtra("userId", feed.user_id)
                        itemView.context.startActivity(intent)
                    }
                }
                feed.forumNotes!!.isNotEmpty() -> {
                    applicationBinding.image.visibility = View.GONE
                    applicationBinding.pdf.visibility = View.VISIBLE
                    itemView.setOnClickListener {
                        if (URLUtil.isValidUrl(feed.forumNotes)) {
                            Log.v("valid", "valid")
                            val intent = Intent(itemView.context, ForumsDetailsActivity::class.java)
                            intent.putExtra("type", "pdf")
                            intent.putExtra("forumData",mFilteredList)
                            intent.putExtra("userId", feed.user_id)
                            itemView.context.startActivity(intent)
                        } else {
                            itemView.context.toast("invalid")
                        }
                    }

                }
            }
        }

    }override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                mFilteredList = if (charString.isEmpty()) {
                    mCategoryList
                } else {
                    var filteredList: ArrayList<Data> = ArrayList()
                    for (row in mCategoryList) {
                        if (row.fourmName!!.toLowerCase().contains(charString.toLowerCase()) ||
                            row.category!!.toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row)
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = mFilteredList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                mFilteredList = filterResults.values as ArrayList<Data>
                notifyDataSetChanged()
            }
        }
    }
}