package com.tirade.android.core.trashtalk.notification.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tirade.android.R
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.notification.data.model.Data
import com.tirade.android.core.trashtalk.notification.data.viewmodel.TrashNotificationViewModel
import com.tirade.android.databinding.ActivityTrashnotificationBinding
import kotlinx.android.synthetic.main.activity_trashnotification.*
import kotlinx.android.synthetic.main.activity_trashnotification.view.*

class TrashNotificationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTrashnotificationBinding
    private lateinit var viewModel: TrashNotificationViewModel

    private var dataList: ArrayList<Data> = ArrayList()
    private lateinit var adapter: TrashNotificationListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trashnotification)
        viewModel = ViewModelProvider(this@TrashNotificationActivity, TrashTalkModelFactory()).get(
            TrashNotificationViewModel::class.java
        )
        binding.viewModel = viewModel

        subscribeDataCallBack()
search_button.setOnClickListener {
        adapter.filter.filter(search_here.text.toString())

}
        back.setOnClickListener{
            finish()
        }
    }

    override fun onResume() {
        setRecyclerView(dataList)
        super.onResume()
    }

    private fun subscribeDataCallBack() {
        viewModel.getProjectList()?.observe(this, Observer {
            if (it != null) {
                adapter.setAppList(it)
            }
        })
    }

    private fun setRecyclerView(dataList: ArrayList<Data>) {
        adapter = TrashNotificationListAdapter(this)
        val categoryLinearLayoutManager = LinearLayoutManager(this)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.root.recycler_view.layoutManager = categoryLinearLayoutManager
        adapter.setAppList(dataList)
        binding.root.recycler_view.adapter = adapter

    }

}
