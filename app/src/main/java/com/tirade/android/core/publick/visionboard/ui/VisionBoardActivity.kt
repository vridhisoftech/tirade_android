package com.tirade.android.core.publick.visionboard.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tirade.android.R
import com.tirade.android.core.publick.visionboard.adapter.VisionStoriesListAdapter
import com.tirade.android.core.publick.visionboard.data.model.StoriesList
import com.tirade.android.core.publick.visionboard.data.viewmodel.VisionBoardViewModel
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.databinding.ActivityVisionBoardBinding
import com.tirade.android.databinding.AdapterStoriesListBinding
import kotlinx.android.synthetic.main.activity_vision_board.view.*

class VisionBoardActivity : AppCompatActivity() {
    var binding:ActivityVisionBoardBinding?=null
    var viewModel:VisionBoardViewModel?=null
    var storiesList:ArrayList<StoriesList>?=null
    private lateinit var storiesListAdapter: VisionStoriesListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_vision_board)

        viewModel = ViewModelProvider(this, TrashTalkModelFactory()).get(
                VisionBoardViewModel::class.java
        )


        subscribeDataCallBack()
    }
    private fun subscribeDataCallBack() {
        viewModel!!.getProjectList()?.observe(this, Observer {
            if (it != null) {
                setRecyclerView(it)
            }
        })
    }
    private fun setRecyclerView(dataList: ArrayList<StoriesList>) {
        storiesListAdapter = VisionStoriesListAdapter(this)
        val categoryLinearLayoutManager = LinearLayoutManager(this)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding!!.root.recyclerView_create_story.layoutManager = categoryLinearLayoutManager
        storiesListAdapter.setAppViewerList(dataList)
        binding!!.root.recyclerView_create_story.adapter = storiesListAdapter as VisionStoriesListAdapter

    }

}
