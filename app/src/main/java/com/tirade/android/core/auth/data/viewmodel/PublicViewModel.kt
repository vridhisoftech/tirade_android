package com.tirade.android.core.auth.data.viewmodel

import android.graphics.Color
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.tirade.android.R
import com.tirade.android.core.auth.data.interfaces.AuthListener
import com.tirade.android.core.auth.data.repo.AuthRepository
import com.tirade.android.network.Connection
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.Helper
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class PublicViewModel(private val repository: AuthRepository) : ViewModel() {
    var authListener: AuthListener? = null

    var firstname: ObservableField<String> = ObservableField("")
    var lastname: ObservableField<String> = ObservableField("")
    var mobileno: ObservableField<String> = ObservableField("")
    var emailid: ObservableField<String> = ObservableField("")
    var password: ObservableField<String> = ObservableField("")
    var age: ObservableField<String> = ObservableField("Birth date")
    var zip: ObservableField<String> = ObservableField("")
    var gender: ObservableField<String> = ObservableField("Male")
    var profileImg: ObservableField<String> = ObservableField("")
    var progress: ObservableInt = ObservableInt(View.GONE)

    //val publicUser: AuthResponse = repository.getDataForPublicUser()


    fun openDatePicker(){
        authListener?.openDatePicker()
        //age.set(text)
    }

    fun onCheckedChanged(radioGroup: RadioGroup, checkedId: Int) {
        if (checkedId == R.id.male) {
            gender.set("Male")
        } else  if (checkedId == R.id.female) {
            gender.set("Female")
        } else  if (checkedId == R.id.other) {
            gender.set("Other")
        }
    }

    fun onImageSelection() {
        authListener?.onImageSelection()
    }

    fun enterSignUp() {
        val name = firstname.get()!! + " " + lastname.get()!!
        val emailid = emailid.get()!!
        val mobileno = mobileno.get()!!
        val password = password.get()!!
        val age = age.get()!!
        val zip = zip.get()!!
        val gender = gender.get()!!
        val profileImg = profileImg.get()!!

        if(TextUtils.isEmpty(firstname.get()!!)){
            authListener?.onFailure("Name filed is invalid!")
            return
        }else if(TextUtils.isEmpty(emailid) || !isValidEmail(emailid) ){
            authListener?.onFailure("Email-Id filed is invalid!")
            return
        }else if(TextUtils.isEmpty(mobileno)){
            authListener?.onFailure("Mobile number fileInd is invalid!")
            return
        }else if(TextUtils.isEmpty(password) || !isPassValid(password)){
            authListener?.onFailure("Password filed is invalid, min length 8!")
            return
        }else if(TextUtils.isEmpty(age)|| age == "Birth date"){
            authListener?.onFailure("Age filed is invalid!")
            return
        }else if(TextUtils.isEmpty(zip)){
            authListener?.onFailure("Zip code filed is invalid!")
            return
        }else if(TextUtils.isEmpty(gender)){
            authListener?.onFailure("Select your gender!")
            return
        }else if(TextUtils.isEmpty(profileImg)){
            authListener?.onFailure("Choose profile image!")
            return
        }

        if (!Connection.isConnected()) {
            authListener?.onFailure("Check Internet Connection!")
            return
        }

        progress.set(View.VISIBLE)
        Coroutines.main {
            try {
                val file = File(profileImg)
                // Parsing any Media type file
                val requestFile: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
                val fileBody: MultipartBody.Part =
                    MultipartBody.Part.createFormData("user_profile", file.name, requestFile)

                val name: RequestBody = createRequestBody(name)
                val emailId: RequestBody = createRequestBody(emailid)
                val phone: RequestBody = createRequestBody(mobileno)
                val password: RequestBody = createRequestBody(password)
                val age: RequestBody = createRequestBody(age)
                val zip: RequestBody = createRequestBody(zip)
                val gender: RequestBody = createRequestBody(gender)

                var params: HashMap<String, RequestBody> = HashMap()
                params["name"] = name
                params["email"] = emailId
                params["phone"] = phone
                params["password"] = password
                params["birthday"] = age
                params["zip"] = zip
                params["gender"] = gender

                val authResponse = repository.userPublicSignUp(params, fileBody)

                repository.saveDataForPublicUser(authResponse)// save data in shared pref
                if (authResponse.status == 0) {
                    //Failed
                    progress.set(View.GONE)
                    val msg = authResponse.message.toString()
                    authListener?.onFailure(msg)
                    return@main
                } else {
                    //Success
                    authResponse.message.let {
                        progress.set(View.GONE)
                        authListener?.onSuccess(it)
                        return@main
                    }
                }
            } catch (e: ApiException) {
                progress.set(View.GONE)
                authListener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                progress.set(View.GONE)
                authListener?.onFailure(e.message!!)
            }
        }
    }

    fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }

    private fun isValidEmail(email: CharSequence): Boolean {
        /*Min a@b.cc*/
        var isValid = false
        if (email.isEmpty()) {
            isValid = false
        } else if (Helper.isEmailValid(email)) {
            isValid = true
        }
        return isValid
    }

    private fun isPassValid(password: CharSequence): Boolean {
        var isValid = false
        if (password.isEmpty()) {
            isValid = false
        } else if (password.length > 7) {
            isValid = true
        }
        return isValid
    }


    fun openLogin() {

    }

}