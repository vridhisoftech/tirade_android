package com.tirade.android.core.trashtalk.forums.ui.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.ListView
import android.widget.PopupWindow
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumsListActivityListener
import com.tirade.android.core.trashtalk.forums.data.model.*
import com.tirade.android.core.trashtalk.forums.ui.activity.ForumCommentActivity
import com.tirade.android.core.trashtalk.forums.ui.activity.ForumsDetailsActivity
import com.tirade.android.core.trashtalk.home.ui.fragment.detail.UserListAdapter
import com.tirade.android.core.trashtalk.home.ui.fragment.detail.ViewersModel
import com.tirade.android.databinding.ForumsDetailsItemBinding
import com.tirade.android.dialog.video.ForumOneItemActivity
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import com.tirade.android.utils.toast
import java.util.*

class ForumsDetailsAdapter(private var listener: ForumsListActivityListener,
                           private var requireActivity: ForumsDetailsActivity):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mCategoryList = ArrayList<CommonModel>()
    private var mFilteredList = ArrayList<CommonModel>()
    private var type: String? = null
    private var chooseUserPopUp: PopupWindow? = null

    fun setForumList(categoryModel: ArrayList<CommonModel>, s: String) {
        mFilteredList.clear()
        mCategoryList.addAll(categoryModel)

        mFilteredList = mCategoryList
        type = s
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mFilteredList.size)
        return mFilteredList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mFilteredList[position]
        (holder as RecyclerViewHolder).bind(feeder)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = ForumsDetailsItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: ForumsDetailsItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: CommonModel) {

            Log.v("forumId", feed!!.forum_id)
            applicationBinding.comment.setOnClickListener {
//            Log.v("userID",userId)
                val intent = Intent(requireActivity, ForumCommentActivity::class.java)
                intent.putExtra("userId", feed.forum_id)
                requireActivity.startActivity(intent)
            }

            applicationBinding.like.setOnClickListener {
                Coroutines.main {
                    try {
                        val user_id = PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user?.user_id
                        val invite = ForumActionPayload(user_id = user_id, forum_id = feed.forum_id, value = "agree")
                        val response: ForumPostLike = postLikeForum(invite)
                        requireActivity.toast(response.message)
                    } catch (e: ApiException) {
                        requireActivity.toast(e.message!!)
                    } catch (e: NoInternetException) {
                        requireActivity.toast(e.message!!)
                    }
                }
            }

            applicationBinding.share.setOnClickListener {
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type="text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, feed.url);
                requireActivity.startActivity(Intent.createChooser(shareIntent, "Share Via"))
            }

            applicationBinding.totalLike.setOnClickListener {
                chooseUserPopUp?.let {
                    if (it.isShowing) {
                        it.dismiss()
                    }
                    chooseUserPopUp = null
                }

                val viewerList = ArrayList<ViewersModel>()

                Coroutines.main {
                    try {
                        val payload = ForumId(forum_id = feed.forum_id)
                        val response: ForumGetLike = postGetLikeForum(payload)
                        for (i in response.agree_data.indices) {
                            val temp = ViewersModel( response.agree_data[i].user_img , response.agree_data[i].user_name)
                            viewerList.add(temp)
                        }
                        if(response.agree_data.isEmpty()) {
                            chooseUserPopUp!!.dismiss()
                        }

                        val adapter = UserListAdapter(requireActivity, viewerList)
                        chooseUserPopUp = selectFromList(adapter,"Total Likes")
                        chooseUserPopUp?.isOutsideTouchable = true
                        chooseUserPopUp?.isFocusable = true
                        chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        chooseUserPopUp?.showAsDropDown(applicationBinding.totalLike, 0, 0, Gravity.BOTTOM)
                    } catch (e: ApiException) {
                        chooseUserPopUp!!.dismiss()
                    } catch (e: NoInternetException) {
                        chooseUserPopUp!!.dismiss()
                    }catch (e: Exception){

                    }
                }
            }

            applicationBinding.totalComments.setOnClickListener {
                chooseUserPopUp?.let {
                    if (it.isShowing) {
                        it.dismiss()
                    }
                    chooseUserPopUp = null
                }

                val viewerList = ArrayList<ViewersModel>()

                Coroutines.main {
                    try {
                        val payload = ForumId(forum_id = feed.forum_id)
                        val response: ForumGetComment = postGetCommentForum(payload)
                        for (i in response.comments.indices) {
                            val temp = ViewersModel( response.comments[i].user_img , response.comments[i].name)
                            viewerList.add(temp)
                        }
                        if(response.comments.isEmpty()) {
                            chooseUserPopUp!!.dismiss()
                        }

                        val adapter = UserListAdapter(requireActivity, viewerList)
                        chooseUserPopUp = selectFromList(adapter,"Total Comments")
                        chooseUserPopUp?.isOutsideTouchable = true
                        chooseUserPopUp?.isFocusable = true
                        chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        chooseUserPopUp?.showAsDropDown(applicationBinding.totalLike, 0, 0, Gravity.BOTTOM)
                    } catch (e: ApiException) {
                        chooseUserPopUp!!.dismiss()
                    } catch (e: NoInternetException) {
                        chooseUserPopUp!!.dismiss()
                    } catch (e: Exception){

                    }
                }
            }

            applicationBinding.imPlay.visibility = View.GONE

            applicationBinding.totalLike.text = "Total Likes: " + feed.total_agree
            applicationBinding.totalComments.text = "Total Comments: " + feed.total_comment
            if (feed.url!!.isNotEmpty() && type == "images") {

                Glide.with(requireActivity).load(feed.url).into(applicationBinding.image)
                applicationBinding.image.visibility = View.VISIBLE
                applicationBinding.pdf.visibility = View.GONE
                applicationBinding.imPlay.visibility = View.GONE
                applicationBinding.image.setOnClickListener {
                    val intent = Intent(requireActivity, ForumOneItemActivity::class.java)
                    intent.putExtra("type", "image")
                    intent.putExtra("url", feed.url)
                    requireActivity.startActivity(intent)
                }

            } else if (feed.url!!.isNotEmpty() && type == "videos") {
                applicationBinding.image.visibility = View.VISIBLE
                applicationBinding.imPlay.visibility = View.VISIBLE
                applicationBinding.pdf.visibility = View.GONE
                applicationBinding.imPlay.setOnClickListener {
                    val intent = Intent(requireActivity, ForumOneItemActivity::class.java)
                    intent.putExtra("url", feed.url)
                    intent.putExtra("type", "video")
                    requireActivity.startActivity(intent)
                }

            } else if (feed.url!!.isNotEmpty() && type == "files") {
                applicationBinding.image.visibility = View.GONE
                applicationBinding.pdf.visibility = View.VISIBLE
                itemView.setOnClickListener {
                    if (URLUtil.isValidUrl(feed.url)) {
                        Log.v("valid", "valid")
                        val intent = Intent(requireActivity, ForumOneItemActivity::class.java)
                        intent.putExtra("type", "pdf")
                        intent.putExtra("url", feed.url)
                        requireActivity.startActivity(intent)
                    } else {
                        requireActivity.toast("invalid")
                    }
                }

            }
        }

        suspend fun postGetLikeForum(params: ForumId): ForumGetLike {
            val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }
            return apiServices.getLikeForum(params).await()
        }

        suspend fun postGetCommentForum(params: ForumId): ForumGetComment {
            val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }
            return apiServices.getCommentForum(params).await()
        }

        suspend fun postLikeForum(params: ForumActionPayload): ForumPostLike {
            val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }
            return apiServices.postForumLike(params).await()
        }


        private fun selectFromList(adapter: UserListAdapter, title: String): PopupWindow {
            val inflater = requireActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.user_list, null)

            view.findViewById<TextView>(R.id.title).also {
                it.text = title
            }
            val listView = view.findViewById<ListView>(R.id.listView)
            listView.adapter = adapter

            return PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }

    }
}