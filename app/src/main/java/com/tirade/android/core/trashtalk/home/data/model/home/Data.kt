package com.tirade.android.core.trashtalk.home.data.model.home

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(val post_id: Int? = null,
                val username: String? = null,
                val name: String? = null,
                val post_type: String? = null,
                val category: String? = null,
                val user_id: String? = null,
                val who: String? = null,
                val what: String? = null,
                val how: String? = null,
                val post_content: String? = null,
                val agree: ArrayList<Agree>? = null,
                val disagree: ArrayList<Disagree>? = null,
                val s_agree: ArrayList<SAgree>? = null,
                val s_disagree: ArrayList<SDisagree>? = null,
                val boo: ArrayList<Boo>? = null,
                val cheers: ArrayList<Cheer>? = null,
                val created_date: String? = null,
                val post_file: String? = null,
                val user_profile: String? = null,
                val image_path: String? = null,
                val video_path: String? = null,
                val audio_path: String? = null,
                val pdf_path: String? = null,
                val txtfile_path: String? = null,
                val comments: ArrayList<AllComments>? = null,
                val notification: ArrayList<AllNotification>? = null
) : Parcelable