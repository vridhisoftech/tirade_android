package com.tirade.android.core.trashtalk.home.ui.fragment.sex

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SexViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Sex Fragment"
    }
    val text: LiveData<String> = _text
}