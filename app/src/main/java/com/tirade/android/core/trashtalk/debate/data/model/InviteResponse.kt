package com.tirade.android.core.trashtalk.debate.data.model

data class InviteResponse(
    val message: String,
    val status: Int,
    val user: User
)

data class User(
    val debate_reason: String,
    val invited_id: String,
    val sender_id: String
)