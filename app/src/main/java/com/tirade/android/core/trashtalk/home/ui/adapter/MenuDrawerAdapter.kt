package com.tirade.android.core.trashtalk.home.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.R
import com.tirade.android.core.trashtalk.home.data.interfaces.MenuListItemListener
import java.util.*

class MenuDrawerAdapter(private var listener: MenuListItemListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mCategoryList = ArrayList<String>()

    fun setAppList(categoryModel: ArrayList<String>) {
        mCategoryList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mCategoryList.size)
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]
        (holder as RecyclerViewHolder).bind(feeder)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //val layoutInflater = LayoutInflater.from(parent.context)
       // val applicationBinding = ForumsListItemBinding.inflate(layoutInflater, parent, false)
        val inflatedView = parent.inflate(R.layout.menu_list_item, false)
        return RecyclerViewHolder(inflatedView)
    }

    fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    inner class RecyclerViewHolder(inflatedView: View) : RecyclerView.ViewHolder(inflatedView), View.OnClickListener {

        private val tvItem = inflatedView.findViewById(R.id.title) as TextView
        fun bind(feed: String) {
            tvItem.text = feed
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v) {
                itemView -> {
                    listener.onItemSelected(adapterPosition)
                }
            }
        }
    }
}