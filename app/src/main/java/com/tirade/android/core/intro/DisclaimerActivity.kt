package com.tirade.android.core.intro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.tirade.android.R
import com.tirade.android.utils.Helper
import com.tirade.android.utils.goActivity

class DisclaimerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disclaimer)
        Helper.setFullScreen(this,window)

        findViewById<TextView>(R.id.tvAgree).setOnClickListener {
            this.goActivity(DisclaimerActivityNext::class.java)
            finish()
        }
        findViewById<TextView>(R.id.tvDontAgree).setOnClickListener {
            finish()
        }
    }
}
