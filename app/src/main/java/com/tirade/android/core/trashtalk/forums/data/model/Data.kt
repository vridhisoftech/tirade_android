package com.tirade.android.core.trashtalk.forums.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    val user_id:String?=null,
    val addUrl: String?=null,
    val category: String?=null,
    val customLink: String?=null,
    val followLike: String?=null,
    val forumNotes: String?=null,
    val forumVideo: String?=null,
    val fourmAudience: String?=null,
    val fourmGoal: String?=null,
    val fourmImage: String?=null,
    val fourmName: String?=null,
    val fourmsDescription: String?=null,
    val linkSite: String?=null,
    val myWebsite: String?=null,
    val profileName: String?=null,
    val socialLink: String?=null,
    val titleLink: String?=null
):Parcelable{

}