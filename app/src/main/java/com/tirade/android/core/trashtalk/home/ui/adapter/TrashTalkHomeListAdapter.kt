package com.tirade.android.core.trashtalk.home.ui.adapter

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.trashtalk.home.data.interfaces.TrashTalkHomeListener
import com.tirade.android.core.trashtalk.home.data.model.home.AllComments
import com.tirade.android.core.trashtalk.home.data.model.home.Data
import com.tirade.android.core.trashtalk.home.ui.activitiy.CommentActivity
import com.tirade.android.core.trashtalk.home.ui.fragment.detail.TrashTalkDetailsActivity
import com.tirade.android.databinding.HomeListItemBinding
import com.tirade.android.dialog.video.PlayerActivity
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.Helper
import com.tirade.android.utils.toast
import com.tirade.expendablemenu.ExpandedMenuClickListener
import com.tirade.expendablemenu.ExpandedMenuItem

class TrashTalkHomeListAdapter(
    private var listener: TrashTalkHomeListener,
    private var requireActivity: FragmentActivity,
    private var categorySelected: String
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    Filterable {
    private val mCategoryList = ArrayList<Data>()
    private var mFilteredList = ArrayList<Data>()
    private var mCommentList = ArrayList<AllComments>()
    private var categorySelected2: String?=null

    fun setAppList(categoryModel: ArrayList<Data>) {
        mCategoryList.clear()
        mCategoryList.addAll(categoryModel)
        Log.v("categorySelected", "" + categorySelected)
        categorySelected2=categorySelected
        if (categorySelected.toLowerCase().trim() == "All Posts".toLowerCase().trim()) {
            mFilteredList = mCategoryList
        } else {
            for (i in 0 until mCategoryList.size) {
                if (mCategoryList[i].category!!.toLowerCase().trim()
                        .contains(categorySelected.toLowerCase().trim())
                ) {
                    mFilteredList.add(mCategoryList[i])
                }
            }
        }

        Log.v("FilteredList", "" + mFilteredList.size)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mFilteredList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mFilteredList[position]

        (holder as RecyclerViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = HomeListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: HomeListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Data) {
            applicationBinding.viewModel = feed
            val postProfile = applicationBinding.postImg
            val postTextTrend = applicationBinding.textTrend
            val postTextPostType = applicationBinding.textPostType
            postTextPostType.text = feed.category.toString()
            applicationBinding.date.text = Helper.getAmericanDateFormat(feed.created_date.toString())

            when {
                feed.s_agree!!.size!! > 10 -> {
                    postTextTrend.text = "Top Spot"
                    postTextTrend.setTextColor(Color.GREEN)
                }
                feed.s_agree.size < 10 -> {
                    postTextTrend.text = "Random"
                    postTextTrend.setTextColor(Color.YELLOW)
                }
                feed.s_disagree!!.size > 10 -> {
                    postTextTrend.text = "Most Hated"
                    postTextTrend.setTextColor(Color.RED)
                }
                feed.user_id!! == PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user!!.user_id -> {
                    postTextTrend.text = "Local"
                    postTextTrend.setTextColor(Color.BLUE)
                }
                feed.cheers!!.size > 10 -> {
                    postTextTrend.text = "Much Props"
                    postTextTrend.setTextColor(Color.DKGRAY)
                }
            }

            when {
                feed.post_type.toString().toLowerCase() == "trash" -> {
                    Glide.with(postProfile.context).load(R.drawable.ic_trash).into(postProfile)

                }
                feed.post_type.toString().toLowerCase() == "debate" -> {
                    Glide.with(postProfile.context).load(R.drawable.ic_debate).into(postProfile)
                }
                feed.post_type.toString().toLowerCase() == "whistle blower" -> {
                    Glide.with(postProfile.context).load(R.drawable.ic_whistle_blower)
                        .into(postProfile)
                }
                feed.post_type.toString().toLowerCase() == "jokes" -> {
                    Glide.with(postProfile.context).load(R.drawable.ic_jokes).into(postProfile)
                }
            }

            Log.v("Category", "CATE" + feed.category)
            if (feed.post_id == 0) {
                //applicationBinding.expMenu.visibility = View.GONE
                applicationBinding.llMain.visibility = View.GONE
            } else {
                applicationBinding.llMain.visibility = View.VISIBLE
            }
            applicationBinding.image.visibility = View.VISIBLE
            applicationBinding.expMenu.visibility = View.VISIBLE

            applicationBinding.rlContentType.visibility = View.GONE
            if (!feed.image_path!!.isBlank()) {
                applicationBinding.image.visibility = View.VISIBLE

                applicationBinding.rlContentType.visibility = View.GONE
                Glide.with(itemView.context).load(feed.image_path).into(applicationBinding.image)

                applicationBinding.image.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("type", "image")
                    intent.putExtra("url", feed.image_path)
                    requireActivity.startActivity(intent)
                }

            } else if (!feed.video_path!!.isBlank()) {
                applicationBinding.image.visibility = View.GONE
                applicationBinding.fileName.text =
                    feed.video_path.substring(feed.video_path.lastIndexOf('/') + 1)
                applicationBinding.rlContentType.visibility = View.VISIBLE
                applicationBinding.contentIcon.setBackgroundResource(R.drawable.ic_video)

                applicationBinding.rlContentType.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("url", feed.video_path)
                    intent.putExtra("type", "video")
                    requireActivity.startActivity(intent)
                }

            } else if (!feed.audio_path!!.isBlank()) {
                applicationBinding.image.visibility = View.GONE
                applicationBinding.rlContentType.visibility = View.VISIBLE
                applicationBinding.contentIcon.setBackgroundResource(R.drawable.ic_audio)
                applicationBinding.fileName.text =
                    feed.audio_path.substring(feed.audio_path.lastIndexOf('/') + 1)

                applicationBinding.rlContentType.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("type", "audio")
                    intent.putExtra("url", feed.audio_path)
                    requireActivity.startActivity(intent)
                }

            } else if (!feed.pdf_path!!.isBlank()) {
                applicationBinding.image.visibility = View.GONE
                applicationBinding.rlContentType.visibility = View.VISIBLE
                applicationBinding.contentIcon.setBackgroundResource(R.drawable.ic_file)
                applicationBinding.fileName.text =
                    feed.pdf_path.substring(feed.pdf_path.lastIndexOf('/') + 1)

                applicationBinding.rlContentType.setOnClickListener {
                    if (URLUtil.isValidUrl(feed.pdf_path)) {
                        Log.v("valid", "valid")
                        val intent = Intent(requireActivity, PlayerActivity::class.java)
                        intent.putExtra("type", "pdf")
                        intent.putExtra("url", feed.pdf_path)
                        requireActivity.startActivity(intent)
                    } else {
                        requireActivity.toast("invalid")
                    }
                }

            } else if (!feed.txtfile_path!!.isBlank()) {
                applicationBinding.image.visibility = View.GONE
                applicationBinding.rlContentType.visibility = View.VISIBLE
                applicationBinding.contentIcon.setBackgroundResource(R.drawable.ic_text)
                applicationBinding.fileName.text =
                    feed.txtfile_path.substring(feed.txtfile_path.lastIndexOf('/') + 1)

                applicationBinding.rlContentType.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("type", "txt")
                    intent.putExtra("url", feed.txtfile_path)
                    requireActivity.startActivity(intent)
                }

            }

            itemView.setOnClickListener {
                listener.onCategoryClick(adapterPosition)
                val intent = Intent(requireActivity, TrashTalkDetailsActivity::class.java)
                val gson = Gson()
                intent.putExtra("feed", gson.toJson(feed))
                requireActivity.startActivity(intent)
            }

            applicationBinding.imMore.setOnClickListener {
                val popupMenu = PopupMenu(itemView.context, applicationBinding.imMore)
                popupMenu.menuInflater.inflate(R.menu.popup_trashtalk_menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.action_facebook ->
                            shareOnSocial(feed, "com.facebook.katana")
                        R.id.action_instagram ->
                            shareOnSocial(feed, "com.instagram.android")
                        R.id.action_snapchat ->
                            shareOnSocial(feed, "com.snapchat.android")
                        R.id.action_tictok ->
                            shareOnSocial(feed, "com.tiktok.android")
                        R.id.action_twitter ->
                            shareOnSocial(feed, "com.twitter.android")
                    }
                    true
                })
                popupMenu.show()

            }

            //val expMenu = itemView.findViewById(R.id.expMenu) as com.tirade.expendablemenu.ExpandedMenu
            applicationBinding.expMenu.setIcons(
                ExpandedMenuItem(R.drawable.agree_copy, "Like", feed.s_agree!!.size.toString()),
                ExpandedMenuItem(R.drawable.dis_agree_copy, "Dislike", feed.s_disagree!!.size.toString()),
                ExpandedMenuItem(R.drawable.energy, "Boo", feed.boo!!.size.toString()),
                ExpandedMenuItem(R.drawable.comment, "Comment", feed.comments!!.size.toString()),
                ExpandedMenuItem(R.drawable.agree, "Agree", feed.agree!!.size.toString()),
                ExpandedMenuItem(R.drawable.clapings, "Cheer", feed.cheers!!.size.toString()),
                ExpandedMenuItem(R.drawable.dis_agree, "DisAgree", feed.disagree!!.size.toString())
            )

            applicationBinding.expMenu.setOnItemClickListener(object : ExpandedMenuClickListener {
                override fun onItemClick(position: Int) {
                    when (position) {
                        0 -> {
                            applicationBinding.expMenu.textColor
                            itemView.context.toast("Agreed")
                            Log.v("postIdClicked",feed.post_id.toString())
                            listener.onPostActionClick(feed.post_id.toString().toInt(), feed.user_id.toString().toInt(),"s_agree")
                        }
                        1 -> {
                            itemView.context.toast("DisAgreed")
                            listener.onPostActionClick(feed.post_id.toString().toInt(), feed.user_id.toString().toInt(),"s_disagree")
                        }
                        2 -> {
                            listener.onPostActionClick(feed.post_id.toString().toInt(), feed.user_id.toString().toInt(),"boo")
                            itemView.context.toast("Boo")
                        }
                        3 -> {
                            Log.v("PostId", feed.post_id.toString())
                            Log.v("POstType",categorySelected!!)

                            val intent = Intent(requireActivity, CommentActivity::class.java)
                            intent.putExtra("postId", feed.post_id.toString())
                            intent.putExtra("type", categorySelected)

                            intent.putExtra("comments", feed.comments)
                            (requireActivity).startActivityForResult(intent,2)
                            //itemView.context.toast("Comment ${feed.post_id} $postId")
                        }
                        4 -> {
                            itemView.context.toast("Agreed")
                            listener.onPostActionClick(feed.post_id.toString().toInt(), feed.user_id.toString().toInt(),"agree")

                        }
                        5 -> {
                            itemView.context.toast("Cheer")
                            listener.onPostActionClick(feed.post_id.toString().toInt(), feed.user_id.toString().toInt(),"cheers")

                        }
                        6 -> {
                            itemView.context.toast("Disagreed")
                            listener.onPostActionClick(feed.post_id.toString().toInt(), feed.user_id.toString().toInt(),"disagree")

                        }
                    }

                }

            })
        }


        private fun shareOnSocial(feed: Data, packageName: String) {
            var path = ""
            var type = ""
            if (!feed.image_path!!.isBlank()) {
                path = feed.image_path
                type = "Image"
            } else if (!feed.video_path!!.isBlank()) {
                path = feed.video_path
                type = "Video"
            } else if (!feed.audio_path!!.isBlank()) {
                path = feed.audio_path
                type = "Audio"
            } else if (!feed.pdf_path!!.isBlank()) {
                path = feed.pdf_path
                type = "PDF"
            } else if (!feed.txtfile_path!!.isBlank()) {
                path = feed.txtfile_path
                type = "TEXT"
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.setPackage(packageName)
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, type)
            shareIntent.putExtra(Intent.EXTRA_TEXT, path)
            try {
                requireActivity.startActivity(Intent.createChooser(shareIntent, "Share:"))
            } catch (ex: ActivityNotFoundException) {
                itemView.context.toast("Failed: Application not found to share.")
            }

        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                mFilteredList = if (charString.isEmpty()) {
                    mCategoryList
                } else {
                    var filteredList: ArrayList<Data> = ArrayList()
                    for (row in mCategoryList) {
                        if (row.name!!.toLowerCase().trim().contains(charString.toLowerCase().trim()) ||
                            row.category!!.toLowerCase().trim().contains(charString.toLowerCase().trim())||
                            row.post_type!!.toLowerCase().trim().contains(charString.toLowerCase().trim()))
                            filteredList.add(row)
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = mFilteredList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                mFilteredList = filterResults.values as ArrayList<Data>
                notifyDataSetChanged()
            }
        }
    }
}


