package com.tirade.android.core.trashtalk.home.ui.fragment.detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tirade.android.R;

import java.util.ArrayList;


public class UserListAdapter extends BaseAdapter {
    private ArrayList<ViewersModel> data;
    private LayoutInflater inflater;

    public UserListAdapter(Context context, ArrayList<ViewersModel> array) {
        data = array;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        final ViewersModel current = data.get(i);

        if (view == null) {
            view = inflater.inflate(R.layout.list_item_user, viewGroup, false);

            viewHolder = new UserListAdapter.ViewHolder();
            viewHolder.name = view.findViewById(R.id.item_name);
            viewHolder.userImg = view.findViewById(R.id.userImg);

            view.setTag(viewHolder);

        } else {
            viewHolder = (UserListAdapter.ViewHolder) view.getTag();
        }

        Glide.with(view.getContext()).load(current.getUser_img()).error(R.drawable.img_placeholder).into(viewHolder.userImg);
        viewHolder.name.setText(current.getName());

        return view;
    }

    private static class ViewHolder {
        ImageView userImg;
        TextView name;
    }
}

