package com.tirade.android.core.trashtalk.home.data.model.home

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
 class ViewersModel(var name: String? = null,
                    var user_img: String? = null
 ) :Parcelable