package com.tirade.android.core.trashtalk.forums.data.viewmodel

import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumCommentListener
import com.tirade.android.core.trashtalk.forums.data.model.ForumCommentResponse
import com.tirade.android.core.trashtalk.forums.data.model.ForumComments
import com.tirade.android.core.trashtalk.forums.data.repo.ForumsRepo
import com.tirade.android.core.trashtalk.home.data.interfaces.TrashTalkHomeListener
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.RequestBody

class ForumCommentActivityViewModel(private val repository: ForumsRepo):ViewModel() {
    var listner: ForumCommentListener? = null
    var listner2: TrashTalkHomeListener? = null
    var forum_id: ObservableField<String> = ObservableField("")
    var comment: ObservableField<String> = ObservableField("")


    fun onClickSent() {
        Log.v("POstIdInside","OOAa"+forum_id.get()!!)
        val userId = forum_id.get()!!
        val comments = comment.get()!!

        when {
            TextUtils.isEmpty(comments) -> {
                listner?.toastShow("Comment field is empty!")
                return
            }
        }

        Coroutines.main {
            try {

                val params = ForumComments()
                params.forum_id=userId
                params.comment=comments


                val response = repository.postCommentToServer(params)
                if (response.status == 1) {
                    sentComment(response.comments)
                    listner?.toastShow(response.message!!)
                }

            } catch (e: ApiException) {
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                listner?.toastShow(e.message!!)
            }
        }
    }

    private fun sentComment(comment: ArrayList<ForumComments>?) {
        listner!!.getComment(comment!!)
    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }


    private var dataList= ForumCommentResponse()
    private val mutablePostList: MutableLiveData<ArrayList<ForumComments>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)
    var noData: ObservableInt = ObservableInt(View.GONE)

    fun getForumCommentList(): LiveData<ArrayList<ForumComments>>? {
        progress.set(View.VISIBLE)
        noData.set(View.GONE)
        val userId = forum_id.get()!!
        val mediaType: MediaType = MediaType.parse("application/json")!!
        val body = RequestBody.create(mediaType, "{\n\n\"forum_id\":\"${userId}\"\n\n}")

        Coroutines.main {
            try {
                val response = repository.fetchCommentData(body)
                response.comments.let {
                    dataList = response
                    // dataList = response as ArrayList<TrashHomeResponse>
                    mutablePostList.postValue(dataList.comments)//notify observable
                    //repository.saveDataForOffline(dataList)//save data for offline
                    progress.set(View.GONE)
                    if(dataList.comments!!.size == 0){
                        noData.set(View.VISIBLE)
                    }
                    return@main
                }
            } catch (e: ApiException) {
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                listner?.toastShow(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutablePostList
    }

}