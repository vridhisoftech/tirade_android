package com.tirade.android.core.trashtalk.home.data.repo

import com.tirade.android.core.trashtalk.home.data.model.home.CommentResponse
import com.tirade.android.core.trashtalk.home.data.model.home.TrashHomeResponse
import com.tirade.android.core.trashtalk.home.data.model.temp.PostComment
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient

class CommentRepo {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun postCommentToServer(params:  PostComment): CommentResponse {
        return apiServices.postComment("token", params).await()
    }
    suspend fun postTiradeCommentToServer(params:  PostComment): CommentResponse {
        return apiServices.postTiradeComment("token", params).await()
    }
    suspend fun fetchData(): TrashHomeResponse {
        val response = apiServices.getTrashTalkHome().await()
        return response

    }

    companion object {
        fun getInstance(): CommentRepo {
            val mInstance: CommentRepo by lazy { CommentRepo() }
            return mInstance
        }
    }
}