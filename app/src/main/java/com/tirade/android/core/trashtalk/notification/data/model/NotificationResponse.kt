package com.tirade.android.core.trashtalk.notification.data.model

data class NotificationResponse(
    val data : List<Data>
)

data class Data(
    val created_date: String,
    val debate_reason: String,
    val message: String,
    val sender_id: String,
    val sender_user_profile: String,
    val sender_username: String
)