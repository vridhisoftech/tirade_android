package com.tirade.android.core.trashtalk.home.ui.fragment.government

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tirade.android.R

class GovernmentFragment : Fragment() {

    private lateinit var governmentViewModel: GovernmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        governmentViewModel =
            ViewModelProviders.of(this).get(GovernmentViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_government, container, false)
        val textView: TextView = root.findViewById(R.id.text_slideshow)
        governmentViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}