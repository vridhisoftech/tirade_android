package com.tirade.android.core.publick.blackmarket.ui

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.drawable.Animatable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ActivityNavigator
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.GridLayoutManager
import com.tirade.android.R
import com.tirade.android.core.publick.blackmarket.data.viewmodel.BlvckViewModel
import com.tirade.android.core.publick.blackmarket.ui.fragment.BlvckMarketHomeFragment
import com.tirade.android.core.publick.factory.PublickFactory
import com.tirade.android.core.trashtalk.home.data.interfaces.MenuListItemListener
import com.tirade.android.core.trashtalk.home.ui.adapter.MenuDrawerAdapter
import com.tirade.android.core.trashtalk.shame.ui.ShameActivity
import com.tirade.android.databinding.ActivityBlvckMarketBinding
import com.tirade.android.utils.toast
import com.tirade.android.widget.BottomSheetTiradeCreatePost
import com.tirade.menuarc.StateChangeListener
import kotlinx.android.synthetic.main.activity_blvck_market.*
import kotlinx.android.synthetic.main.activity_blvck_market.view.*
import kotlinx.android.synthetic.main.app_bar_blvck_market.*
import kotlinx.android.synthetic.main.content_trash_talk_home.view.*

class BlvckMarketActivity : AppCompatActivity(), MenuListItemListener {

    private lateinit var searchListener: SearchListener
    private lateinit var binding: ActivityBlvckMarketBinding
    private var dataList: ArrayList<String> = ArrayList()
    private val catArray = arrayOf<String>(
       "blvck Market" )

    interface SearchListener {
        fun onSearchClick()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_blvck_market)

        menuIcon.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        addFragment(BlvckMarketHomeFragment.newInstance("All Posts"))



        tv_search.setOnClickListener {
            searchListener.onSearchClick()
        }

        //navController = Navigation.findNavController(this, R.id.nav_host)
        //NavigationUI.setupWithNavController(navView, navController)//drawer navigation
        //NavigationUI.setupWithNavController(bottomNavView, navController)//bottom navigation

        setUpBottomNavigationBar()

        val tiradeLogo = findViewById<View>(R.id.tirade_logo)

        arcMenu.setNothingClickListener(null)
        //arcMenu.setMenuIcon(ContextCompat.getDrawable(this,R.drawable.vision_tirade_logo))
        arcMenu.setStateChangeListener(object : StateChangeListener {
            override fun onMenuOpened() {
                //this@TrashTalkHome.toast("Menu Opened")
                if (arcMenu.mDrawable is Animatable) (arcMenu.mDrawable as Animatable).start()
                tiradeLogo.visibility = View.VISIBLE
            }

            override fun onMenuClosed() {
                //this@TrashTalkHome.toast("Menu Closed")
                tiradeLogo.visibility = View.GONE
            }
        })

        findViewById<View>(R.id.golive).setOnClickListener(subMenuClickListener)

        for (item in catArray) {
            dataList.add(item)
        }

        setRecyclerView(dataList)
    }

    fun registerSearchListner(searchListener: SearchListener) {
        this.searchListener = searchListener
    }

    private fun setRecyclerView(dataList: ArrayList<String>) {
        val categoryAdapter = MenuDrawerAdapter(this)
        val categoryLinearLayoutManager =
            GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false)
        recycler_view_menu.layoutManager = categoryLinearLayoutManager
        categoryAdapter.setAppList(dataList)
        recycler_view_menu.adapter = categoryAdapter
    }

    private fun addFragment(fragment: Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.containerFrag, fragment)
        ft.commit()
    }

    private val subMenuClickListener = View.OnClickListener {
        when (it.id) {
            R.id.golive -> {
                this.toast("GO LIVE is in Under Construction")
            }
        }
        arcMenu.toggleMenu()
    }

    private fun setUpBottomNavigationBar() {
        binding.root.bottomNavView.itemIconTintList = null//To show SVG
        //bottomNavView.getOrCreateBadge(R.id.bottom_nav_notification) // Show badge
        //bottomNavView.removeBadge(R.id.bottom_nav_notification) // Remove badge
        //val badge = bottomNavView.getBadge(R.id.bottom_nav_notification) // Get badge

        binding.root.bottomNavView.setOnNavigationItemSelectedListener { item: MenuItem ->
            return@setOnNavigationItemSelectedListener when (item.itemId) {
                R.id.bottom_nav_home -> {
                    toast("Bottom HOME item click")
                    //navController.navigate(R.id.bottom_nav_home)
                    true
                }
                R.id.bottom_nav_settings -> {
                    //toast("Settings item clicked")
                    //navController.navigate(R.id.bottom_nav_settings)
//                    this.goActivity(TrashSettingActivity::class.java)
                    true
                }
                R.id.bottom_nav_plus -> {
                    //toast("ADD item clicked")
                    //navController.navigate(R.id.bottom_nav_plus)
                    binding.root.arcMenu.toggleMenu()
                    true
                }
                R.id.bottom_nav_notification -> {
                    //toast("Notification item clicked")
                    // navController.navigate(R.id.bottom_nav_notification)
//                    this.goActivity(TrashNotificationActivity::class.java)
                    true
                }
                R.id.bottom_nav_info -> {
                    //toast("INFO item clicked")
                    //navController.navigate(R.id.bottom_nav_info)
//                    this.goActivity(ShameActivity::class.java)
                    true
                }
                else -> false
            }
        }

    }

    private fun toast(s: String) {
        Toast.makeText(applicationContext, s, Toast.LENGTH_SHORT).show()
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            Navigation.findNavController(this, R.id.navController),
            drawerLayout
        )
    }

    override fun onItemSelected(position: Int) {
        addFragment(BlvckMarketHomeFragment.newInstance(catArray[position]))
        drawerLayout.closeDrawer(GravityCompat.START)
    }


}