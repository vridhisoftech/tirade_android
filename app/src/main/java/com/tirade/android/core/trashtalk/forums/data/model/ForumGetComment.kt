package com.tirade.android.core.trashtalk.forums.data.model

data class ForumGetComment(
    val comments: List<Comment>
)