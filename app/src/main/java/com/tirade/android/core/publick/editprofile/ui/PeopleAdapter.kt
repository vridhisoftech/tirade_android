package com.tirade.android.core.publick.editprofile.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.core.publick.editprofile.data.model.AddPeople
import com.tirade.android.core.publick.tirade.data.listener.TiradeListener
import com.tirade.android.core.publick.tirade.data.model.Data
import com.tirade.android.core.trashtalk.home.data.model.temp.News
import com.tirade.android.databinding.PeopleYouMayKnowListItemBinding
import com.tirade.android.databinding.TrendingVideosListItemBinding
import java.util.*

class PeopleAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mCategoryList = ArrayList<AddPeople>()

    fun setAppList(categoryModel: ArrayList<AddPeople>) {
        mCategoryList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mCategoryList.size)
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]
        (holder as RecyclerViewHolder).bind(feeder)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = PeopleYouMayKnowListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: PeopleYouMayKnowListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: AddPeople) {
            applicationBinding.dataModel = feed
            itemView.setOnClickListener{
              //TODO element click
            }
        }
    }
}