package com.tirade.android.core.publick.editprofile.data.model

data class Biography(
    val s3_born: String,
    val s3_education: String,
    val s3_live: String,
    val s3_liveIn: String,
    val s3_status: String,
    val s3_work: String
)