package com.tirade.android.core.trashtalk.home.data.model.newsfeed

data class NewsFeedResponse(
    val data: List<Data>
)