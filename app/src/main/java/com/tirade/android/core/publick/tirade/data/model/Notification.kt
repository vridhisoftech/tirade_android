package com.tirade.android.core.publick.tirade.data.model

data class Notification(
    val created_at: String,
    val debate_reason: String,
    val sender_id: String,
    val sender_username: String,
    val user_img: String
)