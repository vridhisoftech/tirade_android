package com.tirade.android.core.trashtalk.forums.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class VideosModel(var total_agree:String?=null, var total_comment:String?=null,
                  var forum_id:String?=null, var forumVideo:String?=null):Parcelable {
}