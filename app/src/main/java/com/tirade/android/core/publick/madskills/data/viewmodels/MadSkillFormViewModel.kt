package com.tirade.android.core.publick.madskills.data.viewmodels

import android.graphics.Color
import android.system.Os
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.publick.madskills.data.listener.MadSkillListener
import com.tirade.android.core.publick.madskills.data.repo.MadSkillRepo
import com.tirade.android.core.trashtalk.trash.data.listener.TrashListener
import com.tirade.android.core.trashtalk.trash.data.repo.TrashRepo
import com.tirade.android.network.Connection
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import kotlin.concurrent.fixedRateTimer

class MadSkillFormViewModel(private val repository: MadSkillRepo): ViewModel() {

    var listner: MadSkillListener? = null
    var user_id: ObservableField<String> = ObservableField("")
    var user_name: ObservableField<String> = ObservableField("")
    var business_name: ObservableField<String> = ObservableField("Business Madskill")
    var business_category: ObservableField<String> = ObservableField("")
    var experience: ObservableField<String> = ObservableField("")
    var youRepresent: ObservableField<String> = ObservableField("")
    var height: ObservableField<String> = ObservableField("")
    var weight: ObservableField<String> = ObservableField("")
    var otherSkills: ObservableField<String> = ObservableField("")
    var mediaType: ObservableField<String> = ObservableField("")
    var post_video: ObservableField<String> = ObservableField("")
    var post_audio: ObservableField<String> = ObservableField("")
    var post_image: ObservableField<String> = ObservableField("")
    var post_file: ObservableField<String> = ObservableField("")
    var progress: ObservableField<Int> = ObservableField(View.GONE)
    var file: ObservableField<File> = ObservableField()
    var postTypeKey: ObservableField<String> = ObservableField("")


    fun onSelectItem(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
//        category.set(parent!!.selectedItem.toString())
        val textView = parent!!.getChildAt(0) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 20f
        textView.setPadding(20,0,0,0)
    }

    fun onVideo(){
        mediaType.set("video")
        listner?.onVideo()
    }

    fun onClickPdf(){
        mediaType.set("pdf")
        listner?.onClickPdf()
    }

    fun onVoice(){
        mediaType.set("audio")
        listner?.onVoice()
    }

    fun onPhoto(){
        mediaType.set("image")
        listner?.onPhoto()
    }

    fun onText(){
        mediaType.set("doc")
        listner?.onText()
    }

    fun onContinue(){
        //listner?.onContinue()
//        val category = category.get()!!
        val user_name = user_name.get()!!
        val experience = experience.get()!!
        val youRepresent = youRepresent.get()!!
        val height = height.get()!!
        val weight = weight.get()!!
        val postAudio = post_audio.get()!!
        val postVideo = post_video.get()!!
        val postImage = post_image.get()!!
        val postFile = post_file.get()!!
        val otherSkills = otherSkills.get()!!
        val business_name = business_name.get()!!
        val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_id
        when {
            TextUtils.isEmpty(user_name) -> {
                listner?.toastShow("Member Name field is empty!")
                return
            }
            TextUtils.isEmpty(experience) -> {
                listner?.toastShow("Experience field is empty!")
                return
            }TextUtils.isEmpty(experience) -> {
                listner?.toastShow("Represent field is empty!")
                return
            }TextUtils.isEmpty(experience) -> {
                listner?.toastShow("Height field is empty!")
                return
            }TextUtils.isEmpty(experience) -> {
                listner?.toastShow("Weight field is empty!")
                return
            }TextUtils.isEmpty(otherSkills) -> {
                listner?.toastShow("otherSkills field is empty!")
                return
            }

        }

        if (TextUtils.isEmpty(postAudio)
            && TextUtils.isEmpty(postVideo)
            && TextUtils.isEmpty(postImage)
            && TextUtils.isEmpty(postFile)
        ) {
            listner?.toastShow("Please select any media source!")
            return
        }

        if (!Connection.isConnected()) {
            listner?.toastShow("Check Internet Connection!")
            return
        }

        progress.set(View.VISIBLE)

        Coroutines.main {
            try {
                when {
                    mediaType.get().equals("image") -> {
                        file.set(File(postImage))
                        postTypeKey.set("post_image")
                    }
                    mediaType.get().equals("video") -> {
                        file.set(File(postVideo))
                        postTypeKey.set("post_video")
                    }
                    mediaType.get().equals("audio") -> {
                        file.set(File(postAudio))
                        postTypeKey.set("post_audio")
                    }
                    mediaType.get().equals("doc") -> {
                        file.set(File(postFile))
                        postTypeKey.set("post_txt")
                    }
                    mediaType.get().equals("pdf") -> {
                        file.set(File(postFile))
                        postTypeKey.set("post_pdf")
                    }
                }

                val file = file.get()!!
                // Parsing any Media type file
                val requestFile: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
                val fileBody: MultipartBody.Part = MultipartBody.Part.createFormData(postTypeKey.get()!!, file.name, requestFile)

                // create a map of data to pass along
                val userId: RequestBody = createRequestBody(userId.toString())
//                val category: RequestBody = createRequestBody(category)
                val username: RequestBody = createRequestBody(user_name)
                val experience: RequestBody = createRequestBody(experience)
                val youRepresent: RequestBody = createRequestBody(youRepresent)
                val weight: RequestBody = createRequestBody(weight)
                val height: RequestBody = createRequestBody(height)
                val business_name: RequestBody = createRequestBody(business_name)
//                val post: RequestBody = createRequestBody(post)


                var params:HashMap<String, RequestBody> = HashMap()
                params["user_id"] = userId
//                params["created_by_fk"] = userId
                params["experience"] = experience
                params["user_name"] = username
                params["youRepresent"] = youRepresent
                params["weight"] = weight
                params["height"] = height
                params["business_name"] = business_name

                val response = repository.postDataToServer(params, fileBody)
                if(response.status == 1){
                    listner?.onSuccess(response.message!!)
                }
                progress.set(View.GONE)
            } catch (e: ApiException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            }
        }
    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }


    fun onFacebook(){
        listner?.onFacebook()
    }

    fun onGoogle(){
        listner?.onGoogle()
    }

    fun onTwitter(){
        listner?.onTwitter()
    }

    fun onWhatsUp(){
        listner?.onWhatsUp()
    }

}