package com.tirade.android.core.intro

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.tirade.android.R
import com.tirade.android.Tirade
import com.tirade.android.core.trashtalk.home.ui.activitiy.TrashTalkHome
import com.tirade.android.network.TiradeClient
import com.tirade.android.utils.Helper
import com.tirade.player.core.exoplayer2.ExoPlayerFactory
import com.tirade.player.core.exoplayer2.Player
import com.tirade.player.core.exoplayer2.SimpleExoPlayer
import com.tirade.player.core.exoplayer2.database.ExoDatabaseProvider
import com.tirade.player.core.exoplayer2.source.ProgressiveMediaSource
import com.tirade.player.core.exoplayer2.upstream.DataSource
import com.tirade.player.core.exoplayer2.upstream.DefaultDataSourceFactory
import com.tirade.player.core.exoplayer2.upstream.cache.Cache
import com.tirade.player.core.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.tirade.player.core.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.tirade.player.core.exoplayer2.upstream.cache.SimpleCache
import com.tirade.player.core.exoplayer2.util.Util
import com.tirade.player.core.ui.PlayerView
import java.io.File

class TrashTalkIntro : AppCompatActivity() {

    private val TAG = "TrashTalkIntro"
    private val STREAM_URL = TiradeClient.baseURL+"intro_videos/trash_talk_intro.mp4";
    private lateinit var progressBar: ProgressBar
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private lateinit var mediaDataSourceFactory: DataSource.Factory
    private lateinit var playerView: PlayerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trash_talk_intro)
        Helper.setFullScreen(this,window)

        findViewById<TextView>(R.id.tvGo).setOnClickListener {
           val intent = Intent(this, TrashTalkHome::class.java).also {
                startActivity(it)
            }
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
            finish()
        }

        playerView = findViewById(R.id.playerView)
        progressBar = findViewById(R.id.progressBar)
    }

    private fun newCache(): Cache? {
        return SimpleCache(
            File(Tirade.getInstance().externalCacheDir, "trash-talk-intro-cache"),  //Cache directory
            LeastRecentlyUsedCacheEvictor(512 * 1024 * 1024),  //Buffer size, default 512M, implemented using LRU algorithm
            ExoDatabaseProvider(Tirade.getInstance())
        )
    }
    private fun initializePlayer() {

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this)

        mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "Tirade"))

        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(
            Uri.parse(STREAM_URL))

        simpleExoPlayer.prepare(mediaSource, false, false)
        simpleExoPlayer.playWhenReady = true

        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = simpleExoPlayer
        playerView.requestFocus()

        /** Default repeat mode is REPEAT_MODE_ONE */
        simpleExoPlayer.repeatMode = Player.REPEAT_MODE_ONE


        simpleExoPlayer.addListener( object : Player.EventListener{

            /** 4 playbackState exists */
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when(playbackState){
                    Player.STATE_BUFFERING -> {
                        progressBar.visibility = View.VISIBLE
                        Log.d(TAG, "onPlayerStateChanged - STATE_BUFFERING" )
                    }
                    Player.STATE_READY -> {
                        progressBar.visibility = View.INVISIBLE
                        Log.d(TAG, "onPlayerStateChanged - STATE_READY" )
                    }
                    Player.STATE_IDLE -> {
                        Log.d(TAG, "onPlayerStateChanged - STATE_IDLE" )
                    }
                    Player.STATE_ENDED -> {
                        Log.d(TAG, "onPlayerStateChanged - STATE_ENDED" )
                    }
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                Log.d(TAG, "onLoadingChanged: ")
            }

            override fun onPositionDiscontinuity(reason: Int) {
                Log.d(TAG, "onPositionDiscontinuity: ")
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
                Log.d(TAG, "onRepeatModeChanged: ")
                Toast.makeText(baseContext, "repeat mode changed", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun releasePlayer() {
        simpleExoPlayer.release()
    }

    public override fun onStart() {
        super.onStart()

        if (Util.SDK_INT > 23) initializePlayer()
    }

    public override fun onResume() {
        super.onResume()

        if (Util.SDK_INT <= 23) initializePlayer()
    }

    public override fun onPause() {
        super.onPause()

        if (Util.SDK_INT <= 23) releasePlayer()
    }

    public override fun onStop() {
        super.onStop()

        if (Util.SDK_INT > 23) releasePlayer()
    }
}
