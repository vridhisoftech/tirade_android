package com.tirade.android.core.trashtalk.forums.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class NotesModel(var forum_id:String?=null, var forumNotes:String?=null):Parcelable {
}