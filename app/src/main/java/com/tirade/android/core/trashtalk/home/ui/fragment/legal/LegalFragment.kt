package com.tirade.android.core.trashtalk.home.ui.fragment.legal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tirade.android.R

class LegalFragment : Fragment() {

    private lateinit var legalViewModel: LegalViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        legalViewModel =
            ViewModelProviders.of(this).get(LegalViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_legal, container, false)
        val textView: TextView = root.findViewById(R.id.text_send)
        legalViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}