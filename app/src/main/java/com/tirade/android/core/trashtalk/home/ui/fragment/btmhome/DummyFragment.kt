package com.tirade.android.core.trashtalk.home.ui.fragment.btmhome

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer

import com.tirade.android.R

class DummyFragment : Fragment() {

    private lateinit var viewModel: DummyViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(this).get(DummyViewModel::class.java)
        val root = inflater.inflate(R.layout.dummy_fragment, container, false)
        val textView: TextView = root.findViewById(R.id.text)
        viewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }

}
