package com.tirade.android.core.publick.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tirade.android.core.publick.blackmarket.data.repo.BlvckRepo
import com.tirade.android.core.publick.blackmarket.data.viewmodel.BlvckViewModel
import com.tirade.android.core.publick.editprofile.data.repo.EditProfileRepo
import com.tirade.android.core.publick.editprofile.data.viewmodel.EditProfileViewModel
import com.tirade.android.core.publick.madskills.data.repo.MadSkillsHomeRepo
import com.tirade.android.core.publick.madskills.data.viewmodel.MadSkillsHomeViewModel
import com.tirade.android.core.publick.tirade.data.repo.TiradeRepository
import com.tirade.android.core.publick.tirade.data.viewmodel.TiradeViewModel

class PublickFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(TiradeViewModel::class.java)) {
            TiradeViewModel(TiradeRepository.getInstance()) as T
        } else return if (modelClass.isAssignableFrom(EditProfileViewModel::class.java)) {
            EditProfileViewModel(EditProfileRepo.getInstance()) as T
        } else return if (modelClass.isAssignableFrom(BlvckViewModel::class.java)) {
            BlvckViewModel(BlvckRepo.getInstance()) as T
        }else return if (modelClass.isAssignableFrom(MadSkillsHomeViewModel::class.java)) {
            MadSkillsHomeViewModel(MadSkillsHomeRepo.getInstance()) as T
        } else {
            //TODO for others view model repository
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

}