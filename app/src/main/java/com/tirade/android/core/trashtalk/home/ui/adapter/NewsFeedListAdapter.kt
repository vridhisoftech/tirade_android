package com.tirade.android.core.trashtalk.home.ui.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tirade.android.R
import com.tirade.android.core.trashtalk.home.data.interfaces.TrashTalkHomeListener
import com.tirade.android.core.trashtalk.home.data.model.newsfeed.Data
import com.tirade.android.core.trashtalk.home.ui.fragment.detail.TrashTalkDetailsActivity
import com.tirade.android.databinding.NewsFeedListItemBinding
import com.tirade.android.dialog.video.PlayerActivity
import com.tirade.android.utils.Helper
import com.tirade.android.utils.toast

class NewsFeedListAdapter(
    private var listener: TrashTalkHomeListener,
    private var requireActivity: FragmentActivity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mCategoryList = ArrayList<Data>()

    fun setAppList(categoryModel: ArrayList<Data>) {
        mCategoryList.clear()
        mCategoryList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]
        (holder as RecyclerViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = NewsFeedListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }

    inner class RecyclerViewHolder(private var applicationBinding: NewsFeedListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Data) {
            applicationBinding.viewModel = feed
            applicationBinding.date.text = Helper.getAmericanDateFormat(feed.created_date.toString())
            applicationBinding.image.visibility = View.GONE
            applicationBinding.play.visibility = View.VISIBLE
            if (feed.video_path.isNotEmpty()) {
                Glide.with(itemView.context).load(R.drawable.ic_video).into(applicationBinding.play)
                applicationBinding.llMain.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("url", feed.video_path)
                    intent.putExtra("type", "video")
                    requireActivity.startActivity(intent)
                }

            } else if (feed.audio_path.isNotEmpty()) {
                Glide.with(itemView.context).load(R.drawable.ic_audio).into(applicationBinding.play)
                applicationBinding.llMain.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("type", "audio")
                    intent.putExtra("url", feed.audio_path)
                    requireActivity.startActivity(intent)
                }

            } else if (feed.pdf_path.isNotEmpty()) {
                Glide.with(itemView.context).load(R.drawable.ic_file).into(applicationBinding.play)
                applicationBinding.llMain.setOnClickListener {
                    if (URLUtil.isValidUrl(feed.pdf_path)) {
                        Log.v("valid", "valid")
                        val intent = Intent(requireActivity, PlayerActivity::class.java)
                        intent.putExtra("type", "pdf")
                        intent.putExtra("url", feed.pdf_path)
                        requireActivity.startActivity(intent)
                    } else {
                        requireActivity.toast("invalid")
                    }
                }
            } else if (feed.txtfile_path.isNotEmpty()) {
                Glide.with(itemView.context).load(R.drawable.ic_text).into(applicationBinding.play)
                applicationBinding.llMain.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("type", "txt")
                    intent.putExtra("url", feed.txtfile_path)
                    requireActivity.startActivity(intent)
                }
            } else if (feed.image_path.isNotEmpty()) {
                applicationBinding.image.visibility = View.VISIBLE
                applicationBinding.play.visibility = View.GONE
                Glide.with(itemView.context).load(feed.image_path).into(applicationBinding.image)
                applicationBinding.image.setOnClickListener {
                    val intent = Intent(requireActivity, PlayerActivity::class.java)
                    intent.putExtra("type", "image")
                    intent.putExtra("url", feed.image_path)
                    requireActivity.startActivity(intent)
                }
            } else {
                applicationBinding.play.visibility = View.GONE
                applicationBinding.imageContainer.visibility = View.GONE
            }

           /* itemView.setOnClickListener {
                listener.onCategoryClick(adapterPosition)
                val intent = Intent(requireActivity, TrashTalkDetailsActivity::class.java)
                val gson = Gson()
                intent.putExtra("feed", gson.toJson(feed))
                requireActivity.startActivity(intent)
            }*/
        }
    }
}
