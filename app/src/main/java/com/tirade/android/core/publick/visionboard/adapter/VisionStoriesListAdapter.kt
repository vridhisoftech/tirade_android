package com.tirade.android.core.publick.visionboard.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tirade.android.core.publick.visionboard.data.model.StoriesList
import com.tirade.android.databinding.AdapterStoriesListBinding

class VisionStoriesListAdapter(
    private var requireActivity: Activity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mCategoryList = ArrayList<StoriesList>()


    fun setAppViewerList(categoryModel: ArrayList<StoriesList>) {
        mCategoryList.clear()
        mCategoryList.addAll(categoryModel)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]

        (holder as RecyclerViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = AdapterStoriesListBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: AdapterStoriesListBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: StoriesList) {
            applicationBinding.viewModel = feed
            Glide.with(requireActivity).load(feed.url).into(applicationBinding.visionPic1)
        }
    }

}


