package com.tirade.android.core.auth.data.model.incognito

data class Incognito(
    val message: String,
    val status: Int,
    val user: User
)