package com.tirade.android.core.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.intro.IntroActivity
import com.tirade.android.core.intro.LoginIntro
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.Helper
import com.tirade.android.utils.goActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Helper.setFullScreen(this, window)
        Handler().postDelayed({
            if (PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.status == 1) {
                Intent(this, LoginIntro::class.java).also {
                    startActivity(it)
                }
            } else if (PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.status == 1) {
                Intent(this, LoginIntro::class.java).also {
                    startActivity(it)
                }
            } else {
                this.goActivity(IntroActivity::class.java)
            }
            this.finish()
        }, 3000)
    }
}
