package com.tirade.android.core.trashtalk.debate.data.model

data class Invite(
    var sender_id: String? = null,
    var invited_id: String? = null,
    var debate_reason: String? = null
)