package com.tirade.android.core.publick.editprofile.data.model

data class EditProfileResponse(
    val getProfileData: ArrayList<GetProfileData>
)