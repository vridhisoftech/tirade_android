package com.tirade.android.core.trashtalk.settings.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.tirade.android.R
import com.tirade.android.databinding.ActivityTrashSettingBinding
import com.tirade.android.utils.goActivity
import kotlinx.android.synthetic.main.activity_trash_setting.*

class TrashSettingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTrashSettingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trash_setting)

        back.setOnClickListener{
            finish()
        }

        termUse.setOnClickListener{
            val termUse = "file:///android_asset/terms_of_use.html"
            val intent = Intent(this, HtmlReaderActivity::class.java)
            val bundle = Bundle()
            bundle.putString("url", termUse)
            bundle.putString("label", "TERMS OF USE")
            intent.putExtras(bundle)
            startActivity(intent)
        }

        termService.setOnClickListener{
            val termService = "file:///android_asset/terms_of_service.html"
            val intent = Intent(this, HtmlReaderActivity::class.java)
            val bundle = Bundle()
            bundle.putString("url", termService)
            bundle.putString("label", "TERMS OF SERVICE")
            intent.putExtras(bundle)
            startActivity(intent)
        }

        termPolicy.setOnClickListener{
            val termPolicy = "file:///android_asset/terms_of_policy.html"
            val intent = Intent(this, HtmlReaderActivity::class.java)
            val bundle = Bundle()
            bundle.putString("url", termPolicy)
            bundle.putString("label", "PRIVACY POLICY")
            intent.putExtras(bundle)
            startActivity(intent)
        }

    }
}
