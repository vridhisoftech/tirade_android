package com.tirade.android.core.trashtalk.home.data.model.home

import android.os.Parcel
import android.os.Parcelable

 class Cheer() :Parcelable {
    var cheers: String? = null
    var name: String? = null
    var user_img: String? = null


    constructor(parcel: Parcel) : this() {
        name =parcel.readString()
        cheers =parcel.readString()
        user_img =parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(cheers)
        parcel.writeString(user_img)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Cheer> {
        override fun createFromParcel(parcel: Parcel): Cheer {
            return Cheer(
                parcel
            )
        }

        override fun newArray(size: Int): Array<Cheer?> {
            android.util.Log.v("ArraySize",""+size)
            return arrayOfNulls<Cheer>(size)

        }
    }
}
