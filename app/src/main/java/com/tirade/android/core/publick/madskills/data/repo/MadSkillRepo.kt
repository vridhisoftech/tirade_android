package com.tirade.android.core.publick.madskills.data.repo

import com.tirade.android.core.publick.madskills.data.model.form.MadSkillFormResponse
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class MadSkillRepo {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun postDataToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): MadSkillFormResponse {
        return apiServices.postMultipartMadSkillData("token", params, fileBody).await()
    }

    companion object {
        fun getInstance(): MadSkillRepo {
            val mInstance: MadSkillRepo by lazy { MadSkillRepo() }
            return mInstance
        }
    }
}