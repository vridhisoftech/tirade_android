package com.tirade.android.core.trashtalk.home.data.viewmodel

import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.trashtalk.home.data.interfaces.TrashTalkHomeListener
import com.tirade.android.core.trashtalk.home.data.model.temp.PostAction
import com.tirade.android.core.trashtalk.home.data.repo.TrashTalkHomeRepository
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException

class TrashTalkHomeViewModel(private val repository: TrashTalkHomeRepository) : ViewModel() {

    var homeListListener: TrashTalkHomeListener? = null

    private var dataListHome: ArrayList<com.tirade.android.core.trashtalk.home.data.model.home.Data> = ArrayList()
    private val mutableHomeList: MutableLiveData<ArrayList<com.tirade.android.core.trashtalk.home.data.model.home.Data>> = MutableLiveData()
    private var dataListNewsFeed: ArrayList<com.tirade.android.core.trashtalk.home.data.model.newsfeed.Data> = ArrayList()
    private val mutableNewsFeedList: MutableLiveData<ArrayList<com.tirade.android.core.trashtalk.home.data.model.newsfeed.Data>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)
    var noData: ObservableInt = ObservableInt(View.GONE)

    fun getHomeListItems(): LiveData<ArrayList<com.tirade.android.core.trashtalk.home.data.model.home.Data>>? {
        progress.set(View.VISIBLE)
        noData.set(View.GONE)
        Coroutines.main {
            try {
                val response = repository.fetchHomeData()
                response.let {
                    dataListHome = response.data as ArrayList<com.tirade.android.core.trashtalk.home.data.model.home.Data>
                    // dataList = response as ArrayList<TrashHomeResponse>
                    mutableHomeList.postValue(dataListHome)//notify observable
                    //repository.saveDataForOffline(dataList)//save data for offline
                    progress.set(View.GONE)
                    if(dataListHome.size == 0){
                        noData.set(View.VISIBLE)
                    }
                    return@main
                }
            } catch (e: ApiException) {
                homeListListener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                homeListListener?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutableHomeList
    }

    fun getNewsFeedListItems(): LiveData<ArrayList<com.tirade.android.core.trashtalk.home.data.model.newsfeed.Data>>? {
        //progress.set(View.VISIBLE)
        //noData.set(View.GONE)
        Coroutines.main {
            try {
                val response = repository.fetchNewsFeedData()
                response.let {
                    dataListNewsFeed = response.data as ArrayList<com.tirade.android.core.trashtalk.home.data.model.newsfeed.Data>
                    mutableNewsFeedList.postValue(dataListNewsFeed)//notify observable
                    //progress.set(View.GONE)
                    return@main
                }
            } catch (e: ApiException) {
                homeListListener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                homeListListener?.onFailure(e.message!!)
            }
            //progress.set(View.GONE)
        }

        return mutableNewsFeedList
    }

    fun onShoutOutClick () {
        homeListListener?.onShoutOutClick()
    }

    fun onRepresentClick() {
        homeListListener?.onRepresentClick()
    }

    fun onTrashClick() {
        homeListListener?.onTrashClick()
    }

    fun onDebateClick() {
        homeListListener?.onDebateClick()
    }

    fun onWblowerClick() {
        homeListListener?.onWblowerClick()
    }

    fun onJokesClick() {
        homeListListener?.onJokesClick()
    }

    fun onForumsClick() {
        homeListListener?.onForumsClick()
    }

    fun onShameClick() {
        homeListListener?.onShameClick()
    }

    fun onPostAction(postId: Int,userId:Int, value: String) {
        Coroutines.main {
            try {
                val params = PostAction()
                params.post_id = postId
                params.user_id = userId
                params.value = value

                Log.v("PosstID", "$postId  $value")
                val response = repository.postActionToServer(params)
                if (response.status == 1) {
                    homeListListener?.onPostActionResponse(response.message!!)
                }

            } catch (e: ApiException) {
                homeListListener?.onPostActionResponse(e.message!!)
            } catch (e: NoInternetException) {
                homeListListener?.onPostActionResponse(e.message!!)
            }
        }
    }

}