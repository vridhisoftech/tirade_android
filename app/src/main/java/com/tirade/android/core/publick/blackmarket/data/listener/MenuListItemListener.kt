package com.tirade.android.core.publick.blackmarket.data.listener

interface MenuListItemListener {
    fun onItemSelected(position: Int)
}