package com.tirade.android.core.intro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.tirade.android.R
import com.tirade.android.utils.Helper
import com.tirade.android.utils.goActivity

class DisclaimerActivityNext : AppCompatActivity(), View.OnClickListener {

    private lateinit var tvAgree: TextView
    private lateinit var tvDontAgree: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disclaimer_next)
        Helper.setFullScreen(this,window)

        tvAgree = findViewById(R.id.tvAgree)
        tvDontAgree = findViewById(R.id.tvDontAgree)


        tvAgree.setOnClickListener(this)
        tvDontAgree.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            tvAgree->{
                this.goActivity(LoginIntro::class.java)
                finish()
            }
            tvDontAgree->{
                finish()
            }
        }
    }
}
