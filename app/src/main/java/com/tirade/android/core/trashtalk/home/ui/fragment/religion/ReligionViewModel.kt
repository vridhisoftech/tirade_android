package com.tirade.android.core.trashtalk.home.ui.fragment.religion

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ReligionViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Religion Fragment"
    }
    val text: LiveData<String> = _text
}