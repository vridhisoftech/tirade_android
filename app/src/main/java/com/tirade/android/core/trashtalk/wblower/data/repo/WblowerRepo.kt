package com.tirade.android.core.trashtalk.wblower.data.repo

import com.tirade.android.core.trashtalk.trash.data.model.PostResponse
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class WblowerRepo {


    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun postDataToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): PostResponse {
        return apiServices.postMultipartFromData("token", params, fileBody).await()
    }

    companion object {
        fun getInstance(): WblowerRepo {
            val mInstance: WblowerRepo by lazy { WblowerRepo() }
            return mInstance
        }
    }
}