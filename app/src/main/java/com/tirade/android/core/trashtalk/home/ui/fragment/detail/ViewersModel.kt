package com.tirade.android.core.trashtalk.home.ui.fragment.detail

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class ViewersModel(
    var user_img: String? = null,
    var name: String? = null
) : Parcelable