package com.tirade.android.core.publick.tirade.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.core.publick.tirade.data.listener.TiradeListener
import com.tirade.android.core.publick.tirade.data.model.Video
import com.tirade.android.databinding.TrendingVideosListItemBinding
import java.util.*

class TrendingAdapter(private var listener: TiradeListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mCategoryList = ArrayList<Video>()

    fun setAppList(categoryModel: ArrayList<Video>) {
        mCategoryList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mCategoryList.size)
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]
        (holder as RecyclerViewHolder).bind(feeder)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = TrendingVideosListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: TrendingVideosListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Video) {
            applicationBinding.appModelNews = feed
            itemView.setOnClickListener{
                listener.onItemClick(feed)
            }
        }

    }
}