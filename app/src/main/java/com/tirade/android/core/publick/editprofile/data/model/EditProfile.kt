package com.tirade.android.core.publick.editprofile.data.model

data class EditProfile(
    val s1_userBirthdate: String,
    val s1_userGender: String,
    val s1_userImage: String,
    val s1_userName: String
)