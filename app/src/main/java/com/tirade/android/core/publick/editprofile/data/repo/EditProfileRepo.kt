package com.tirade.android.core.publick.editprofile.data.repo

import com.tirade.android.core.publick.editprofile.data.model.EditProfileResponse
import com.tirade.android.core.publick.tirade.data.model.Data
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.RequestBody

class EditProfileRepo {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun fetchData(params: HashMap<String, RequestBody>): EditProfileResponse {
        return apiServices.getEditProfileData("token", params).await()
    }

    fun saveDataForOffline(it: List<Data>) {
        //Save response data for offline
    }

   /* suspend fun postProfileDataToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part) {
        return apiServices.postProfileDataToServer("token", params, fileBody).await()
    }*/

    companion object{
        fun getInstance(): EditProfileRepo{
            val mInstance: EditProfileRepo by lazy { EditProfileRepo() }
            return mInstance
        }
    }
}