package com.tirade.android.core.trashtalk.home.data.model.temp

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PostComment() {

    var post_id_fk: String? = null
    var author_id_fk: String? = null
    var comment: String? = null
    fun PostComment(author_id_fk: String, post_id_fk: String,comment:String) {
        this.author_id_fk=author_id_fk
        this.post_id_fk=post_id_fk
        this.comment=comment

    }
}