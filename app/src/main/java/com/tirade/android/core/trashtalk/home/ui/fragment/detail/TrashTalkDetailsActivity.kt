package com.tirade.android.core.trashtalk.home.ui.fragment.detail

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.PopupWindow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.tirade.android.R
import com.tirade.android.core.trashtalk.home.data.model.home.Data
import com.tirade.android.databinding.ActivityTrashTalkDetailsBinding
import kotlinx.android.synthetic.main.activity_trash_talk_details.*

class TrashTalkDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTrashTalkDetailsBinding
    private var chooseUserPopUp: PopupWindow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trash_talk_details)
        val gson = Gson()
        val feed = gson.fromJson<Data>(intent.getStringExtra("feed"), Data::class.java)
        Log.e("ncnnc", feed.toString())
        binding.viewModel = feed

        if (feed.image_path!!.isNullOrBlank()) {
            textPost.visibility = View.GONE
            imPost.visibility = View.GONE
        } else {
            textPost.visibility = View.VISIBLE
            imPost.visibility = View.VISIBLE
        }
        if (feed.post_type!!.toLowerCase().trim() == "trash".toLowerCase().trim()) {
            Glide.with(this).load(R.drawable.ic_trash).into(imLogo)
        }
        if (feed.post_type!!.toLowerCase().trim() == "debate".toLowerCase().trim()) {
            Glide.with(this).load(R.drawable.ic_debate).into(imLogo)
        }
        if (feed.post_type!!.toLowerCase().trim() == "whistle blower".toLowerCase().trim()) {
            Glide.with(this).load(R.drawable.ic_whistle_blower).into(imLogo)
        }
        if (feed.post_type!!.toLowerCase().trim() == "Jokes".toLowerCase().trim()) {
            Glide.with(this).load(R.drawable.ic_jokes).into(imLogo)
        }

        sLike.setOnClickListener {
            dismissPopup()
            val viewerList = ArrayList<ViewersModel>()
            for (i in 0 until feed.s_agree!!.size) {
                val temp = ViewersModel(feed.s_agree[i].user_img, feed.s_agree[i].name)
                viewerList.add(temp)
            }
            val adapter = UserListAdapter(this, viewerList)
            chooseUserPopUp = selectFromList(adapter, "Total Likes")
            chooseUserPopUp?.isOutsideTouchable = true
            chooseUserPopUp?.isFocusable = true
            chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseUserPopUp?.showAsDropDown(sLike, 0, 0, Gravity.BOTTOM)
        }

        sDislike.setOnClickListener {
            dismissPopup()
            val viewerList = ArrayList<ViewersModel>()
            for (i in 0 until feed.s_disagree!!.size) {
                val temp = ViewersModel(feed.s_disagree[i].user_img, feed.s_disagree[i].name)
                viewerList.add(temp)
            }
            val adapter = UserListAdapter(this, viewerList)
            chooseUserPopUp = selectFromList(adapter, "Total DisLikes")
            chooseUserPopUp?.isOutsideTouchable = true
            chooseUserPopUp?.isFocusable = true
            chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseUserPopUp?.showAsDropDown(sDislike, 0, 0, Gravity.BOTTOM)
        }

        boo.setOnClickListener {
            dismissPopup()
            val viewerList = ArrayList<ViewersModel>()
            for (i in 0 until feed.boo!!.size) {
                val temp = ViewersModel(feed.boo[i].user_img, feed.boo[i].name)
                viewerList.add(temp)
            }
            val adapter = UserListAdapter(this, viewerList)
            chooseUserPopUp = selectFromList(adapter, "Total Boo")
            chooseUserPopUp?.isOutsideTouchable = true
            chooseUserPopUp?.isFocusable = true
            chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseUserPopUp?.showAsDropDown(boo, 0, 0, Gravity.BOTTOM)
        }
        comment.setOnClickListener {
            dismissPopup()
            val viewerList = ArrayList<ViewersModel>()
            for (i in 0 until feed.comments!!.size) {
                val temp = ViewersModel(feed.comments[i].user_img, feed.comments[i].user_name)
                viewerList.add(temp)
            }
            val adapter = UserListAdapter(this, viewerList)
            chooseUserPopUp = selectFromList(adapter, "Total Comments")
            chooseUserPopUp?.isOutsideTouchable = true
            chooseUserPopUp?.isFocusable = true
            chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseUserPopUp?.showAsDropDown(comment, 0, 0, Gravity.BOTTOM)
        }

        agree.setOnClickListener {
            dismissPopup()
            val viewerList = ArrayList<ViewersModel>()
            for (i in 0 until feed.agree!!.size) {
                val temp = ViewersModel(feed.agree[i].user_img, feed.agree[i].name)
                viewerList.add(temp)
            }
            val adapter = UserListAdapter(this, viewerList)
            chooseUserPopUp = selectFromList(adapter, "Total Likes")
            chooseUserPopUp?.isOutsideTouchable = true
            chooseUserPopUp?.isFocusable = true
            chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseUserPopUp?.showAsDropDown(agree, 0, 0, Gravity.BOTTOM)
        }

        cheer.setOnClickListener {
            dismissPopup()
            val viewerList = ArrayList<ViewersModel>()
            for (i in 0 until feed.cheers!!.size) {
                val temp = ViewersModel(feed.cheers[i].user_img, feed.cheers[i].name)
                viewerList.add(temp)
            }
            val adapter = UserListAdapter(this, viewerList)
            chooseUserPopUp = selectFromList(adapter, "Total Clapping")
            chooseUserPopUp?.isOutsideTouchable = true
            chooseUserPopUp?.isFocusable = true
            chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseUserPopUp?.showAsDropDown(cheer, 0, 0, Gravity.BOTTOM)
        }

        disagree.setOnClickListener {
            dismissPopup()
            val viewerList = ArrayList<ViewersModel>()
            for (i in 0 until feed.disagree!!.size) {
                val temp = ViewersModel(feed.disagree[i].user_img, feed.disagree[i].name)
                viewerList.add(temp)
            }
            val adapter = UserListAdapter(this, viewerList)
            chooseUserPopUp = selectFromList(adapter, "Total DisLikes")
            chooseUserPopUp?.isOutsideTouchable = true
            chooseUserPopUp?.isFocusable = true
            chooseUserPopUp?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            chooseUserPopUp?.showAsDropDown(disagree, 0, 0, Gravity.BOTTOM)
        }

        Glide.with(this).load(R.drawable.dis_agree_copy).into(im_sDisagree)
        Glide.with(this).load(R.drawable.dis_agree).into(im_disAgree)
        Glide.with(this).load(R.drawable.agree).into(im_agree)
        Glide.with(this).load(R.drawable.agree_copy).into(im_sAgree)
        Glide.with(this).load(R.drawable.energy).into(im_boo)
        Glide.with(this).load(R.drawable.clapings).into(im_cheers)
        Glide.with(this).load(R.drawable.comment).into(im_comment)


        imBack.setOnClickListener {
            finish()
        }
    }

    private fun dismissPopup() {
        chooseUserPopUp?.let {
            if (it.isShowing) {
                it.dismiss()
            }
            chooseUserPopUp = null
        }
    }

    private fun selectFromList(adapter: UserListAdapter, title: String): PopupWindow {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.user_list, null)

        view.findViewById<TextView>(R.id.title).also {
            it.text = title
        }
        val listView = view.findViewById<ListView>(R.id.listView)
        listView.adapter = adapter

        return PopupWindow(
            view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }
}
