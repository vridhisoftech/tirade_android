package com.tirade.android.core.trashtalk.home.ui.fragment.lgbt

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LgbtViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is LGBT Fragment"
    }
    val text: LiveData<String> = _text
}