package com.tirade.android.core.publick.tirade.data.model

data class Video(
    val likes: String,
    val post_id: String,
    val post_video: String
)