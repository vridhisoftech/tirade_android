package com.tirade.android.core.trashtalk.home.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tirade.android.core.trashtalk.home.data.model.home.SAgree
import com.tirade.android.databinding.ViewersListItemBinding

class TrashTalkViewersListAdapter(
    private var requireActivity: FragmentActivity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mCategoryList = ArrayList<SAgree>()


    fun setAppViewerList(categoryModel: ArrayList<SAgree>) {
        mCategoryList.clear()
        mCategoryList.addAll(categoryModel)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]

        (holder as RecyclerViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = ViewersListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: ViewersListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: SAgree) {
            applicationBinding.viewModel = feed
            applicationBinding.viewerName.text=feed.name
            Glide.with(requireActivity).load(feed.user_img).into(applicationBinding.imViewer)
        }
    }

}


