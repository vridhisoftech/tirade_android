package com.tirade.android.core.trashtalk.wblower.ui

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.iceteck.silicompressorr.SiliCompressor
import com.nbsp.materialfilepicker.MaterialFilePicker
import com.nbsp.materialfilepicker.ui.FilePickerActivity
import com.tirade.android.R
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.trash.data.listener.TrashListener
import com.tirade.android.core.trashtalk.trash.ui.TrashActivity
import com.tirade.android.core.trashtalk.wblower.data.viewmodel.WblowerViewModel
import com.tirade.android.databinding.ActivityWblowerBinding
import com.tirade.android.utils.toast
import java.io.File
import java.util.regex.Pattern

class WblowerActivity : AppCompatActivity(), TrashListener {

    private lateinit var binding: ActivityWblowerBinding
    private lateinit var viewModel: WblowerViewModel
    companion object {
        private val PERMISSION_CODE = 1000
        private val IMAGE_PICK_CODE = 1001
        private val REQUEST_PICK_VIDEO = 1002
        private val REQUEST_PICK_AUDIO = 1003
        private val REQUEST_PICK_TXT = 1004
        private val REQUEST_PICK_PDF = 1005
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wblower)
        viewModel = ViewModelProvider(this, TrashTalkModelFactory()).get(WblowerViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.listner = this
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    this.toast("Permission denied")
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (data != null){
                when(requestCode){
                    IMAGE_PICK_CODE -> {
                        val image = data.data
                        val projection = arrayOf(MediaStore.Images.Media.DATA)
                        val cursor = contentResolver.query(image!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()

                        val fileDirSave = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() +  "/TIRADE_IMAGES")
                        val filePath = SiliCompressor.with(this).compress(mediaPath, fileDirSave)
                        viewModel.post_image.set(filePath)

                    }

                    REQUEST_PICK_VIDEO -> {
                        val video = data.data
                        val projection = arrayOf(MediaStore.Video.Media.DATA)
                        val cursor = contentResolver.query(video!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()
                        viewModel.post_video.set(mediaPath)
                    }

                    REQUEST_PICK_AUDIO -> {
                        val audio = data.data
                        val projection = arrayOf(MediaStore.Audio.Media.DATA)
                        val cursor = contentResolver.query(audio!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()
                        viewModel.post_audio.set(mediaPath)
                    }

                    REQUEST_PICK_TXT -> {
                        val textFileUri = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                        viewModel.post_file.set(textFileUri)
                    }

                    REQUEST_PICK_PDF -> {
                        val textFileUri = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                        viewModel.post_file.set(textFileUri)
                    }
                }
            }
        }
    }


    private fun checkPermissionAndPickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery()
            }
        } else{
            //system OS is < Marshmallow
            pickImageFromGallery()
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select a file"),
            IMAGE_PICK_CODE
        )
    }

    private fun pickVideoFromGallery(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "video/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select a file"),
            REQUEST_PICK_VIDEO
        )
    }

    private fun pickAudioFromFileManager(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
        //intent.action = Intent.ACTION_GET_CONTENT
        //intent.type = "audio/mpeg"
        startActivityForResult(
            Intent.createChooser(intent, "Select a file"),
            REQUEST_PICK_AUDIO
        )
    }

    private fun pickPdfFromFileManager(){
        MaterialFilePicker()
            .withActivity(this)
            .withRequestCode(REQUEST_PICK_PDF)
            .withHiddenFiles(true)
            .withFilter(Pattern.compile(".*\\.pdf$"))
            .withTitle("Select PDF file")
            .start()
        //val intent = Intent()
        //intent.action = Intent.ACTION_OPEN_DOCUMENT
        //intent.addCategory(Intent.CATEGORY_OPENABLE)
        //intent.type = "*/*"
        //val extraMimeTypes = arrayOf("application/pdf", "application/doc")
        //intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes)
        //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        //startActivityForResult(intent, REQUEST_PICK_PDF)
    }

    private fun pickTextFromFileManager(){
        MaterialFilePicker()
            .withActivity(this)
            .withRequestCode(REQUEST_PICK_TXT)
            .withHiddenFiles(true)
            .withFilter(Pattern.compile(".*\\.txt$"))
            .withTitle("Select TEXT file")
            .start()
    }

    override fun onChooseToDebate() {

    }

    override fun onVideo() {
        pickVideoFromGallery()
    }

    override fun onClickPdf() {
        pickPdfFromFileManager()
    }

    override fun onVoice() {
        pickAudioFromFileManager()
    }

    override fun onPhoto() {
        checkPermissionAndPickImage()
    }

    override fun onText() {
        pickTextFromFileManager()
    }

    override fun onContinue() {

    }

    override fun onFacebook() {
        shareOnSocial("https://www.facebook.com/")
    }

    override fun onGoogle() {
        shareOnSocial("https://www.google.com/")
    }

    override fun onTwitter() {
        shareOnSocial("https://twitter.com/login?lang=en")
    }

    override fun onWhatsUp() {
        shareOnSocial("https://web.whatsapp.com/")
    }

    override fun toastShow(message: String) {
        toast(message)
    }

    override fun finishActivity() {
        finish()
    }

    override fun onSuccess(message: String) {
        showConfirmationMessage(message)
    }


    private fun showConfirmationMessage(text: String) {
        val dialog = Dialog(this@WblowerActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.confirm_message)

        (dialog.findViewById(R.id.message) as TextView).also {
            it.text = text
        }
        val ok = dialog.findViewById(R.id.ok) as Button
        ok.setOnClickListener {
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }

    private fun shareOnSocial(message: String){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type="text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(shareIntent, "Share Via"))
    }

}
