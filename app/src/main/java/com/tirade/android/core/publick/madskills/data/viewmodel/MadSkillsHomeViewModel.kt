package com.tirade.android.core.publick.madskills.data.viewmodel

import android.text.TextUtils
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.publick.madskills.data.model.topskills.Artist
import com.tirade.android.core.publick.madskills.data.repo.MadSkillsHomeRepo
import com.tirade.android.core.publick.tirade.data.listener.TiradeListener
import com.tirade.android.network.Connection
import com.tirade.android.core.trashtalk.home.data.model.temp.PostAction
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class MadSkillsHomeViewModel(private val repository: MadSkillsHomeRepo): ViewModel() {

    var listner: TiradeListener? = null

    private val mutableTopSkillsList: MutableLiveData<ArrayList<com.tirade.android.core.publick.madskills.data.model.topskills.Artist>> = MutableLiveData()
    private val mutableSkillsList: MutableLiveData<ArrayList<com.tirade.android.core.publick.madskills.data.model.skills.Data>> = MutableLiveData()
    private val mutableHomeList: MutableLiveData<ArrayList<com.tirade.android.core.publick.madskills.data.model.home.Data>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)
    var post_video: ObservableField<String> = ObservableField("")
    var post_image: ObservableField<String> = ObservableField("")
    var post_content: ObservableField<String> = ObservableField("")
    var mediaType: ObservableField<String> = ObservableField("")
    var file: ObservableField<File> = ObservableField()
    var postTypeKey: ObservableField<String> = ObservableField("")

    fun madSkillsTopSkillsCall(): LiveData<ArrayList<com.tirade.android.core.publick.madskills.data.model.topskills.Artist>>? {
        progress.set(View.VISIBLE)
        Coroutines.main {
            try {
                val response = repository.madSkillsTopSkillsCall()
                response.rate_top_skills.let {
                    val dataList = it[0].artist as ArrayList<Artist>
                    mutableTopSkillsList.postValue(dataList)//notify observable
                    progress.set(View.GONE)
                    return@main
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            } catch (e: Exception) {
                listner?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutableTopSkillsList
    }

    fun madSkillsListCall(): LiveData<ArrayList<com.tirade.android.core.publick.madskills.data.model.skills.Data>>? {
        progress.set(View.VISIBLE)
        Coroutines.main {
            try {
                val response = repository.madSkillsListCall()
                response.data.let {
                    val dataList = it as ArrayList<com.tirade.android.core.publick.madskills.data.model.skills.Data>
                    mutableSkillsList.postValue(dataList)//notify observable
                    progress.set(View.GONE)
                    return@main
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            } catch (e: Exception) {
                listner?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutableSkillsList
    }

    fun madSkillsHomeListCall(): LiveData<ArrayList<com.tirade.android.core.publick.madskills.data.model.home.Data>>? {
        progress.set(View.VISIBLE)
        Coroutines.main {
            try {
                val response = repository.madSkillsHomeListCall()
                response.data.let {
                    val dataList = it as ArrayList<com.tirade.android.core.publick.madskills.data.model.home.Data>
                    mutableHomeList.postValue(dataList)//notify observable
                    progress.set(View.GONE)
                    return@main
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            } catch (e: Exception) {
                listner?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutableHomeList
    }

    fun updatePostOnServer(){
        val postVideo = post_video.get()!!
        val postImage = post_image.get()!!
        val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_id

        if (TextUtils.isEmpty(postVideo)
            && TextUtils.isEmpty(postImage)) {
            listner?.toastShow("Please select any media source!")
            return
        }

        if (!Connection.isConnected()) {
            listner?.toastShow("Check Internet Connection!")
            return
        }

        progress.set(View.VISIBLE)

        Coroutines.main {
            try {
                when {
                    mediaType.get().equals("image") -> {
                        file.set(File(postImage))
                        postTypeKey.set("post_image")
                    }
                    mediaType.get().equals("video") -> {
                        file.set(File(postVideo))
                        postTypeKey.set("post_video")
                    }
                }

                val file = file.get()!!
                val requestFile: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
                val fileBody: MultipartBody.Part = MultipartBody.Part.createFormData(postTypeKey.get()!!, file.name, requestFile)

                // create a map of data to pass along
                val userId: RequestBody = createRequestBody(userId.toString())
                val postContent: RequestBody = createRequestBody(post_content.get()!!)

                var params:HashMap<String, RequestBody> = HashMap()
                params["user_id"] = userId
                params["post_content"] = postContent

                val response = repository.postDataToServer(params, fileBody)
                if(response.status == 1){
                    listner?.onSuccess(response.message)
                }

                progress.set(View.GONE)

            } catch (e: ApiException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            }
        }
    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }

    fun onPostLikeAction(postId: String,value: String) {
        Coroutines.main {
            try {
                val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_id

                val params = PostAction()
                params.post_id = postId.toInt()
                params.value = value
                params.user_id =userId!!.toInt()

                val response = repository.postActionToLikeServer(params)
                if (response.status == 1) {
                    listner?.onPostActionResponse(response.message!!)
                }

            } catch (e: ApiException) {
                listner?.onPostActionResponse(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onPostActionResponse(e.message!!)
            }
        }
    }

}