package com.tirade.android.core.publick.madskills.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.core.publick.tirade.data.listener.TiradeListener
import com.tirade.android.core.publick.madskills.data.model.skills.Data
import com.tirade.android.databinding.SkillsListItemBinding
import com.tirade.android.databinding.TopSkillsListItemBinding
import java.util.*

class SkillsAdapter(private var listener: TiradeListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mCategoryList = ArrayList<Data>()

    fun setAppList(categoryModel: ArrayList<Data>) {
        mCategoryList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mCategoryList.size)
        return mCategoryList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mCategoryList[position]
        (holder as RecyclerViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = SkillsListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: SkillsListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Data) {
            applicationBinding.feedViewModel = feed
            itemView.setOnClickListener{
                listener.onCategoryClick(adapterPosition)
            }
        }
    }
}