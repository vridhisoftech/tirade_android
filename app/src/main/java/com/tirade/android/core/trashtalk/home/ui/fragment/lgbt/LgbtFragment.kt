package com.tirade.android.core.trashtalk.home.ui.fragment.lgbt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tirade.android.R

class LgbtFragment : Fragment() {

    private lateinit var lgbtViewModel: LgbtViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lgbtViewModel =
            ViewModelProviders.of(this).get(LgbtViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_lgbt, container, false)
        val textView: TextView = root.findViewById(R.id.text_tools)
        lgbtViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}