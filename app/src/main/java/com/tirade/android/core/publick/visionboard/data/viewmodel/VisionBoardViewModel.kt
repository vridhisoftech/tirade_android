package com.tirade.android.core.publick.visionboard.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.R
import com.tirade.android.core.publick.visionboard.data.model.StoriesList
import com.tirade.android.core.publick.visionboard.data.repo.VisionboardRepo

class VisionBoardViewModel(private val repository: VisionboardRepo):ViewModel() {
    private val mutablePostList: MutableLiveData<ArrayList<StoriesList>> = MutableLiveData()

    fun getProjectList(): LiveData<ArrayList<StoriesList>>? {
        val storiesList = ArrayList<StoriesList>()
        var temp=StoriesList(R.drawable.logo_black)
        storiesList!!.add(temp)
        temp= StoriesList(R.drawable.vision_layer1)
        storiesList!!.add(temp)
        temp=StoriesList(R.drawable.vision_layer2)
        storiesList!!.add(temp)
        temp=StoriesList(R.drawable.vision_layer3)
        storiesList!!.add(temp)
        temp=StoriesList(R.drawable.vision_layer4)
        storiesList!!.add(temp)

        mutablePostList.postValue(storiesList)


        return mutablePostList
        }


    }

