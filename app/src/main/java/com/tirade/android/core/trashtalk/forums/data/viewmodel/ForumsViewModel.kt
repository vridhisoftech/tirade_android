package com.tirade.android.core.trashtalk.forums.data.viewmodel

import android.graphics.Color
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.trashtalk.forums.data.repo.ForumsRepo
import com.tirade.android.core.trashtalk.trash.data.listener.TrashListener
import com.tirade.android.network.Connection
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class ForumsViewModel(private val repository: ForumsRepo): ViewModel() {

    var listner: TrashListener? = null
    var category: ObservableField<String> = ObservableField("")
    var audience: ObservableField<String> = ObservableField("")
    var goal: ObservableField<String> = ObservableField("")
    var followLike: ObservableField<String> = ObservableField("")
    var name: ObservableField<String> = ObservableField("")
    var myWebsiteLinks: ObservableField<String> = ObservableField("")
    var addUrl: ObservableField<String> = ObservableField("")
    var socialLinks: ObservableField<String> = ObservableField("")
    var addProfileName: ObservableField<String> = ObservableField("")
    var progress: ObservableField<Int> = ObservableField(View.GONE)
    var forumImage: ObservableField<String> = ObservableField("")
    var forumNotes: ObservableField<String> = ObservableField("")
    var forumVideo: ObservableField<String> = ObservableField("")
    var postTypeKey: ObservableField<String> = ObservableField("")
    var mediaType: ObservableField<String> = ObservableField("")
    var file: ObservableField<File> = ObservableField()


    fun onSelectItem(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
        category.set(parent!!.selectedItem.toString())
        val textView = parent.getChildAt(0) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 20f
        textView.setPadding(20,0,0,0)
    }


    fun onFileSelection(){
        mediaType.set("pdf")
        listner?.onClickPdf()
    }

    fun onImageSelection(){
        listner?.onPhoto()
    }


    fun onContinue(){
       // listner?.finishActivity()

        val category = category.get()!!
        val audience = audience.get()!!
        val goal = goal.get()!!
        val followLike = followLike.get()!!
        val name = name.get()!!
        val myWebsiteLinks = myWebsiteLinks.get()!!
        val addUrl = addUrl.get()!!
        val socialLinks = socialLinks.get()!!
        val addProfileName = addProfileName.get()!!

        val fourmImage = forumImage.get()!!
        val forumNotes = forumNotes.get()!!
        val forumVideo = forumVideo.get()!!
        val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user?.user_id

        if (!Connection.isConnected()) {
            listner?.toastShow("Check Internet Connection!")
            return
        }

        if (TextUtils.isEmpty(fourmImage)
            && TextUtils.isEmpty(forumNotes)
            && TextUtils.isEmpty(forumVideo)
        ) {
            listner?.toastShow("Please select any media source!")
            return
        }

        if (!Connection.isConnected()) {
            listner?.toastShow("Check Internet Connection!")
            return
        }

        progress.set(View.VISIBLE)

        Coroutines.main {
            try {
                when {
                    mediaType.get().equals("image") -> {
                        file.set(File(fourmImage))
                        postTypeKey.set("fourmImage")
                    }

                    mediaType.get().equals("pdf") -> {
                        file.set(File(forumNotes))
                        postTypeKey.set("forumNotes")
                    }

                    mediaType.get().equals("video") -> {
                        file.set(File(forumVideo))
                        postTypeKey.set("forumVideo")
                    }
                }

                val file = file.get()!!
                // Parsing any Media type file
                val requestFile: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
                val fileBody: MultipartBody.Part = MultipartBody.Part.createFormData(postTypeKey.get()!!, file.name, requestFile)

                // create a map of data to pass along
                val userId: RequestBody = createRequestBody(userId.toString())
                val category: RequestBody = createRequestBody(category)
                val goal: RequestBody = createRequestBody(goal)
                val audience: RequestBody = createRequestBody(audience)
                val followLike: RequestBody = createRequestBody(followLike)
                val name: RequestBody = createRequestBody(name)
                val myWebsiteLinks: RequestBody = createRequestBody(myWebsiteLinks)
                val addUrl: RequestBody = createRequestBody(addUrl)
                val socialLinks: RequestBody = createRequestBody(socialLinks)
                val addProfileName: RequestBody = createRequestBody(addProfileName)

                var params:HashMap<String, RequestBody> = HashMap()
                params["user_id"] = userId
                params["category"] = category
                params["fourmGoal"] = goal
                params["fourmAudience"] = audience
                params["followLike"] = followLike
                params["fourmName"] = name
                params["myWebsite"] = myWebsiteLinks
                params["addUrl"] = addUrl
                params["socialLink"] = socialLinks
                params["profileName"] = addProfileName

                val response = repository.postDataToServer(params, fileBody)
                if(response.status == 1){
                    listner?.onSuccess(response.message)
                }
                progress.set(View.GONE)
            } catch (e: ApiException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            }
        }

    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }

    fun onFacebook(){
        listner?.onFacebook()
    }

    fun onGoogle(){
        listner?.onGoogle()
    }

    fun onTwitter(){
        listner?.onTwitter()
    }

    fun onWhatsUp(){
        listner?.onWhatsUp()
    }
}