package com.tirade.android.core.trashtalk.home.ui.fragment.business

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BusinessViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Business Fragment"
    }
    val text: LiveData<String> = _text
}