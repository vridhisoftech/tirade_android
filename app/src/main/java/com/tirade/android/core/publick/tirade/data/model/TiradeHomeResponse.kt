package com.tirade.android.core.publick.tirade.data.model

data class TiradeHomeResponse(
    val data: List<Data>
)