package com.tirade.android.core.publick.madskills.data.model.topskills

data class Artist(
    val business_image: String,
    val category: String,
    val dislikes: String,
    val likes: String,
    val skill_id: String,
    val user_id: String
)