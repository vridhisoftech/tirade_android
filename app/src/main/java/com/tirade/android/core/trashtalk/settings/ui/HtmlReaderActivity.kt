package com.tirade.android.core.trashtalk.settings.ui

import android.content.res.AssetManager
import android.content.res.Configuration
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.tirade.android.R
import java.io.InputStream

class HtmlReaderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_html_reader)

        var bundle :Bundle ?=intent.extras
        var url = bundle!!.getString("url")
        var labelText = bundle.getString("label")

        val linerLayout = findViewById<LinearLayout>(R.id.linear_layout)
        val label = findViewById<TextView>(R.id.label)
        label.text = labelText

        val back = findViewById<TextView>(R.id.label)
        back.setOnClickListener{
            finish()
        }
        val webView = WebView(this.createConfigurationContext(Configuration()))
        val webSetting = webView.settings
        webSetting.javaScriptEnabled = true
        webSetting.displayZoomControls = true
        webView.webViewClient = WebViewClient()

        webView.loadUrl(url)
        linerLayout.addView(webView)
    }

}
