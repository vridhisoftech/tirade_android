package com.tirade.android.core.trashtalk.forums.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumCommentListener
import com.tirade.android.core.trashtalk.forums.data.model.ForumComments
import com.tirade.android.databinding.CommentsListBinding
import com.tirade.android.databinding.ForumCommentsListBinding
import com.tirade.android.databinding.HomeListItemBinding
import com.tirade.android.utils.Helper
import kotlin.collections.ArrayList

class ForumCommentsAdapter(private var listener: ForumCommentListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mCommentList = ArrayList<ForumComments>()
    var feeder: ForumComments? = null
    fun setAppList(categoryModel: ArrayList<ForumComments>) {
        mCommentList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mCommentList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        feeder = mCommentList[position]
        (holder as RecyclerViewHolder).bind(feeder!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = ForumCommentsListBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: ForumCommentsListBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: ForumComments) {
            applicationBinding.viewModel = feed

            applicationBinding.rightCommentText.text =feed.comment
            applicationBinding.commentDate.text= Helper.getAmericanDateFormat(feed.created_at.toString())

        }


    }
}