package com.tirade.android.core.trashtalk.home.ui.fragment.race

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tirade.android.R

class RaceFragment : Fragment() {

    private lateinit var raceViewModel: RaceViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        raceViewModel =
            ViewModelProviders.of(this).get(RaceViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_race, container, false)
        val textView: TextView = root.findViewById(R.id.text_gallery)
        raceViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}