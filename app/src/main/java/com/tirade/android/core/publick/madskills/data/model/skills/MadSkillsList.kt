package com.tirade.android.core.publick.madskills.data.model.skills

data class MadSkillsList(
    val data: ArrayList<Data>
)