package com.tirade.android.core.publick.madskills.data.model.home

import com.tirade.android.core.publick.madskills.data.model.home.Data

data class MadHomeResponse(
    val data: ArrayList<Data>
)