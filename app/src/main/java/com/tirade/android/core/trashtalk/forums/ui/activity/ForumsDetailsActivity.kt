package com.tirade.android.core.trashtalk.forums.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.tirade.android.R
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumsListActivityListener
import com.tirade.android.core.trashtalk.forums.data.model.*
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumsDetailsViewModel
import com.tirade.android.core.trashtalk.forums.ui.adapter.ForumsDetailsAdapter
import com.tirade.android.databinding.ActivityDetailsForumsBinding
import com.tirade.android.utils.toast
import kotlinx.android.synthetic.main.activity_details_forums.view.*


class ForumsDetailsActivity : AppCompatActivity(), ForumsListActivityListener {

    private lateinit var binding: ActivityDetailsForumsBinding
    private lateinit var listViewModel2: ForumsDetailsViewModel
    private var dataList: ArrayList<CommonModel> = ArrayList()
    private var imagesList: ArrayList<ImagesModel> = ArrayList()
    private var videosList: ArrayList<VideosModel> = ArrayList()
    private var filesList: ArrayList<NotesModel> = ArrayList()
    private var spinnerList: ArrayList<String> = ArrayList()
    private lateinit var categoryListAdapter: ForumsDetailsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details_forums)
        listViewModel2 =
            ViewModelProvider(this, TrashTalkModelFactory()).get(ForumsDetailsViewModel::class.java)
        binding.viewModel = listViewModel2
        listViewModel2.listener = this

        val userId = intent.extras!!.get("userId").toString()
        listViewModel2.userId.set(userId)
        spinnerList.add("Select")
        spinnerList.add("Images")
        spinnerList.add("Videos")
        spinnerList.add("Files")

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.root.forumSpinner.adapter = adapter

        val forumData = intent.extras!!.getParcelableArrayList<Data>("forumData")
        val userDataList = ArrayList<Data>()
        for (i in 0 until forumData!!.size) {
            if (userId == forumData[i].user_id) {
                val temp = Data(
                    forumData[i].user_id, forumData[i].addUrl,
                    forumData[i].category,
                    forumData[i].customLink,
                    forumData[i].followLike,
                    forumData[i].forumNotes,
                    forumData[i].forumVideo,
                    forumData[i].fourmAudience,
                    forumData[i].fourmGoal,
                    forumData[i].fourmImage,
                    forumData[i].fourmName,
                    forumData[i].fourmsDescription,
                    forumData[i].linkSite,
                    forumData[i].myWebsite,
                    forumData[i].profileName,
                    forumData[i].socialLink,
                    forumData[i].titleLink
                )
                userDataList.add(temp)
            }
        }

        binding.root.back.setOnClickListener {
            onBackPressed()
            finish()
        }

        binding.back.setOnClickListener {
            finish()
        }


        binding.root.aboutForum.setOnClickListener {
            val intent = Intent(this, ForumsDetailExplore::class.java)
            intent.putExtra("forumData", userDataList)
            startActivity(intent)
        }

        binding.root.forumSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                @SuppressLint("SetTextI18n")
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    val textView: TextView = parent.getChildAt(0) as TextView
                    textView.setTextColor(Color.WHITE)
                    when (position) {
                        1 -> {
                            val commonList = ArrayList<CommonModel>()
                            for (i in 0 until imagesList.size) {
                                val temp = CommonModel(
                                    imagesList[i].total_agree,
                                    imagesList[i].total_comment,
                                    imagesList[i].forum_id,
                                    imagesList[i].fourmImage
                                )
                                commonList.add(temp)
                            }
                            if (imagesList.size == 0) {
                                binding.root.noItemFound.visibility = View.VISIBLE
                            } else {
                                binding.root.noItemFound.visibility = View.GONE
                            }
                            binding.root.totalViews.text = "Total Images " + imagesList.size
                            setRecyclerView(commonList, "images")

                        }
                        2 -> {
                            val commonList = ArrayList<CommonModel>()

                            for (i in 0 until videosList.size) {
                                val temp = CommonModel(
                                    videosList[i].total_agree,
                                    videosList[i].total_comment,
                                    videosList[i].forum_id,
                                    videosList[i].forumVideo
                                )
                                commonList.add(temp)
                            }
                            setRecyclerView(commonList, "videos")
                            binding.root.totalViews.text = "Total Videos " + videosList.size
                            if (videosList.size == 0) {
                                binding.root.noItemFound.visibility = View.VISIBLE
                            } else {
                                binding.root.noItemFound.visibility = View.GONE
                            }

                        }
                        3 -> {
                            val commonList = ArrayList<CommonModel>()

                            for (i in 0 until filesList.size) {
                                val temp = CommonModel(
                                    "",
                                    "",
                                    filesList[i].forum_id,
                                    filesList[i].forumNotes
                                )
                                commonList.add(temp)
                            }
                            binding.root.totalViews.text = "Total Files " + filesList.size
                            setRecyclerView(commonList, "files")
                            if (filesList.size == 0) {
                                binding.root.noItemFound.visibility = View.VISIBLE
                            } else {
                                binding.root.noItemFound.visibility = View.GONE
                            }
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // can leave this empty
                }
            }
        subscribeDataCallBack()

    }


    private fun subscribeDataCallBack() {
        listViewModel2.getForumDetails()?.observe(this, Observer<ArrayList<ForumData>> {
            if (it != null) {
                imagesList.addAll(it[0].images)
                videosList.addAll(it[0].videos)
                filesList.addAll(it[0].notes)
                Glide.with(this).load(it[0].user_image).into(binding.root.forumUserImage)
                binding.root.forumUsername.text = it[0].name
                Log.v("UserImageInside", "" + imagesList.size)
            }
        })
    }

    private fun setRecyclerView(dataList: ArrayList<CommonModel>, s: String) {
        categoryListAdapter = ForumsDetailsAdapter(this, this)
        val categoryLinearLayoutManager = GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false)
        binding.root.forum_recycler_view.layoutManager = categoryLinearLayoutManager
        categoryListAdapter.setForumList(dataList, s)
        binding.root.forum_recycler_view.adapter = categoryListAdapter
    }

    override fun onFailure(message: String) {

    }

    override fun onCategoryClick(position: Int) {
        this.toast("Postion $position")
    }

    override fun onBack() {
        finish()
    }

}
