package com.tirade.android.core.trashtalk.home.ui.fragment.drugs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DrugsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Drugs Fragment"
    }
    val text: LiveData<String> = _text
}