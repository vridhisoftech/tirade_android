package com.tirade.android.core.trashtalk.home.data.viewmodel

import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.trashtalk.home.data.interfaces.CommentListener
import com.tirade.android.core.trashtalk.home.data.interfaces.TrashTalkHomeListener
import com.tirade.android.core.trashtalk.home.data.model.home.Data
import com.tirade.android.core.trashtalk.home.data.model.temp.PostComment
import com.tirade.android.core.trashtalk.home.data.repo.CommentRepo
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.RequestBody

class CommentActivityViewModel(private val repository: CommentRepo):ViewModel() {
    var listner: CommentListener? = null
    var listner2: TrashTalkHomeListener? = null
    var post_id_fk: ObservableField<String> = ObservableField("")
    var auhtor_id_fk: ObservableField<String> = ObservableField("")
    var comment: ObservableField<String> = ObservableField("")


    fun onClickSent() {
        Log.v("POstIdInside","OOAa"+post_id_fk.get()!!)
        val postId = post_id_fk.get()!!
        val authorId = auhtor_id_fk.get()!!
        val comments = comment.get()!!

        when {
            TextUtils.isEmpty(comments) -> {
                listner?.toastShow("Comment field is empty!")
                return
            }
        }

        Coroutines.main {
            try {

                val params = PostComment()
                params.post_id_fk=postId
                params.author_id_fk=authorId
                params.comment=comments


                val response = repository.postCommentToServer(params)
                if (response.status == 1) {
                    sentComment(response.comment)
                    listner?.toastShow(response.message!!)
                }

            } catch (e: ApiException) {
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                listner?.toastShow(e.message!!)
            }
        }
    }
    fun onClickTiradeSent() {
        Log.v("POstIdInside","OOAa"+post_id_fk.get()!!)
        val postId = post_id_fk.get()!!
        val authorId = auhtor_id_fk.get()!!
        val comments = comment.get()!!

        when {
            TextUtils.isEmpty(comments) -> {
                listner?.toastShow("Comment field is empty!")
                return
            }
        }

        Coroutines.main {
            try {

                val params = PostComment()
                params.post_id_fk=postId
                params.author_id_fk=authorId
                params.comment=comments


                val response = repository.postTiradeCommentToServer(params)
                if (response.status == 1) {
                    sentComment(response.comment)
                    listner?.toastShow(response.message!!)
                }

            } catch (e: ApiException) {
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                listner?.toastShow(e.message!!)
            }
        }
    }

    private fun sentComment(comment: String?) {
        listner!!.getComment(comment!!)
    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }


    private var dataList: ArrayList<Data> = ArrayList()
    private val mutablePostList: MutableLiveData<ArrayList<Data>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)
    var noData: ObservableInt = ObservableInt(View.GONE)
    fun getProjectList(): LiveData<ArrayList<Data>>? {
        progress.set(View.VISIBLE)
        noData.set(View.GONE)
        Coroutines.main {
            try {
                val response = repository.fetchData()
                response.let {
                    dataList = response.data as ArrayList<Data>
                    // dataList = response as ArrayList<TrashHomeResponse>
                    mutablePostList.postValue(dataList)//notify observable
                    //repository.saveDataForOffline(dataList)//save data for offline
                    progress.set(View.GONE)
                    if(dataList.size == 0){
                        noData.set(View.VISIBLE)
                    }
                    return@main
                }
            } catch (e: ApiException) {
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                listner?.toastShow(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutablePostList
    }

}