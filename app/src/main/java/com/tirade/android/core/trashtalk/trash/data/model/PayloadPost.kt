package com.tirade.android.core.trashtalk.trash.data.model

data class PayloadPost(
    val created_by_fk : Int,
    val post_type : String,
    val category : String,
    val post_content : String,
    val who : String,
    val what : String
)