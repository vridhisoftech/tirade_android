package com.tirade.android.core.publick.madskills.data.model.form

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MadSkillFormResponse(){
    @SerializedName("status")
    @Expose
    val status: Int? = null
    @SerializedName("message")
    @Expose
    val message: String? = null
}