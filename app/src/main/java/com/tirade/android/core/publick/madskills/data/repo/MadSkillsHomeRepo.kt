package com.tirade.android.core.publick.madskills.data.repo

import com.tirade.android.core.publick.madskills.data.model.home.MadHomeResponse
import com.tirade.android.core.publick.madskills.data.model.skills.MadSkillsList
import com.tirade.android.core.publick.madskills.data.model.topskills.TopSkillsResponse
import com.tirade.android.core.trashtalk.home.data.model.home.PostActionResponse
import com.tirade.android.core.trashtalk.home.data.model.temp.PostAction
import com.tirade.android.core.trashtalk.trash.data.model.PostResponse
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class MadSkillsHomeRepo {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun madSkillsTopSkillsCall(): TopSkillsResponse {
        return apiServices.getMadSkillsTopSkillsList().await()
    }
    
    suspend fun madSkillsListCall(): MadSkillsList {
        return apiServices.getMadSkillsList().await()
    }

    suspend fun madSkillsHomeListCall(): MadHomeResponse {
        return apiServices.getMadSkillsHomeList().await()
    }

    suspend fun postDataToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): PostResponse {
        return apiServices.createPostForTirade("token", params, fileBody).await()
    }

    suspend fun postActionToLikeServer(params: PostAction): PostActionResponse {
        return apiServices.postLikeAction("token", params).await()
    }


    companion object{
        fun getInstance(): MadSkillsHomeRepo{
            val mInstance: MadSkillsHomeRepo by lazy { MadSkillsHomeRepo() }
            return mInstance
        }
    }
}