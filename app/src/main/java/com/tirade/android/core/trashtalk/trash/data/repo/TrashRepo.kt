package com.tirade.android.core.trashtalk.trash.data.repo

import com.tirade.android.core.trashtalk.trash.data.model.PostResponse
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class TrashRepo {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun postDataToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): PostResponse {
        return apiServices.postMultipartFromData("token", params, fileBody).await()
    }

    suspend fun postDataShoutToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): PostResponse {
        return apiServices.postMultipartShoutFromData("token", params, fileBody).await()
    }

    companion object {
        fun getInstance(): TrashRepo {
            val mInstance: TrashRepo by lazy { TrashRepo() }
            return mInstance
        }
    }
}