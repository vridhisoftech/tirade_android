package com.tirade.android.core.trashtalk.forums.data.model

data class ForumPostLike(
    val agree_data: List<AgreeDataX>,
    val message: String,
    val status: Int
)