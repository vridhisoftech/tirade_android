package com.tirade.android.core.trashtalk.debate.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tirade.android.R;
import com.tirade.android.core.trashtalk.debate.data.model.UserData;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    private ArrayList<UserData> data;
    private LayoutInflater inflater;

    public CustomAdapter(Context context, ArrayList<UserData> array) {
        data = array;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        final UserData current = data.get(i);

        if (view == null) {
            view = inflater.inflate(R.layout.list_item, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.item_name);
            viewHolder.userImg = (ImageView) view.findViewById(R.id.userImg);
            viewHolder.checkBox = (CheckBox) view.findViewById(R.id.checkbox);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Glide.with(view.getContext()).load(current.getUser_profile()).error(R.drawable.img_placeholder).into(viewHolder.userImg);
        viewHolder.name.setText(current.getUsername());
        try {
            viewHolder.checkBox.setChecked(current.getChecked());
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    private static class ViewHolder {
        ImageView userImg;
        TextView name;
        CheckBox checkBox;
    }
}
