package com.tirade.android.core.publick

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.auth.ui.IncognitoLogin
import com.tirade.android.core.publick.blackmarket.ui.BlvckMarketActivity
import com.tirade.android.core.publick.madskills.ui.MadSkillsFormActivity
import com.tirade.android.core.publick.tirade.ui.TiradeActivity
import com.tirade.android.core.trashtalk.home.ui.activitiy.TrashTalkHome
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.Helper
import com.tirade.android.utils.goActivity

class ChoosePlatform : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_platform)
        Helper.setFullScreen(this,window)
    }

    fun onTrashTalk(view: View) {
        if (PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.status == 1) {
            Intent(this, TrashTalkHome::class.java).also {
                startActivity(it)
            }
        }else {
            this.goActivity(IncognitoLogin::class.java)
        }
        finish()
    }
    fun onTirade(view: View) {
        this.goActivity(TiradeActivity::class.java)
        finish()
    }
    fun onMadSkills(view: View) {
        this.goActivity(MadSkillsFormActivity::class.java)
        finish()
    }
    fun onBlackMarket(view: View) {
        this.goActivity(BlvckMarketActivity::class.java)
        finish()
    }
}
