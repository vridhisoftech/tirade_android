package com.tirade.android.core.trashtalk.home.data.model.home

import android.os.Parcel
import android.os.Parcelable

 class Disagree() :Parcelable {
    var disagree: String? = null
    var name: String? = null
    var user_img: String? = null


    constructor(parcel: Parcel) : this() {
        name =parcel.readString()
        disagree =parcel.readString()
        user_img =parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(disagree)
        parcel.writeString(user_img)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Disagree> {
        override fun createFromParcel(parcel: Parcel): Disagree {
            return Disagree(
                parcel
            )
        }

        override fun newArray(size: Int): Array<Disagree?> {
            android.util.Log.v("ArraySize",""+size)
            return arrayOfNulls<Disagree>(size)

        }
    }
}
