package com.tirade.android.core.publick.tirade.data.listener

import com.tirade.android.core.publick.tirade.data.model.Video

interface TiradeListener {
    fun onFailure(statusMessage: String)
    fun onCategoryClick(adapterPosition: Int)
    fun onItemClick(feed: Video)
    fun toastShow(message: String)
    fun onSuccess(message: String)
    fun onPostActionClick(postId: Int?, s: String)
    fun onPostActionResponse(message: String?)
}