package com.tirade.android.core.trashtalk.forums.data.model

data class Comment(
    val comment: String,
    val created_at: String,
    val forum_id: String,
    val name: String,
    val user_id: String,
    val user_img: String
)