package com.tirade.android.core.trashtalk.forums.data.viewmodel

import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumsListActivityListener
import com.tirade.android.core.trashtalk.forums.data.model.Data
import com.tirade.android.core.trashtalk.forums.data.model.ForumData
import com.tirade.android.core.trashtalk.forums.data.repo.ForumsListRepo
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException


class ForumsListViewModel(private val repository: ForumsListRepo): ViewModel(){
    var listener: ForumsListActivityListener? = null
    var userId: ObservableField<String> = ObservableField("")

    private var dataList: ArrayList<Data> = ArrayList()
    private var forumList: ArrayList<ForumData> = ArrayList()
    private val mutablePostList: MutableLiveData<ArrayList<Data>> = MutableLiveData()
    private val mutableForumList: MutableLiveData<ArrayList<ForumData>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)

    fun getProjectList(): LiveData<ArrayList<Data>>? {
        progress.set(View.VISIBLE)

        Coroutines.main {
            try {
                val response = repository.fetchData()
                response.data.let {
                    dataList = it as ArrayList<Data>
                    mutablePostList.postValue(dataList)//notify observable
                    //repository.saveDataForOffline(it)//save data for offline
                    progress.set(View.GONE)
                    return@main
                }
            } catch (e: ApiException) {
                listener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listener?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutablePostList
    }
//    fun getForumDetails(): LiveData<ArrayList<ForumData>>? {
//        progress.set(View.VISIBLE)
//        val user_id = userId.get()!!
//
//        Coroutines.main {
//            try {
//                val response = repository.fetchForumData(user_id)
//                response.data.let {
//                    forumList = response.data as ArrayList<ForumData>
//                    // dataList = response as ArrayList<TrashHomeResponse>
//                    mutableForumList.postValue(forumList)//notify observable
//                    //repository.saveDataForOffline(dataList)//save data for offline
//                    progress.set(View.GONE)
//
//                    return@main
//                }
//            } catch (e: ApiException) {
//                listener?.onFailure(e.message!!)
//            } catch (e: NoInternetException) {
//                listener?.onFailure(e.message!!)
//            }
//            progress.set(View.GONE)
//        }
//
//        return mutableForumList
//    }

    fun onBack(){
        listener?.onBack()
    }

    fun onSearch(){

    }

}