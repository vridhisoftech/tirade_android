package com.tirade.android.core.trashtalk.home.ui.fragment.detail

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.R
import com.tirade.android.core.trashtalk.home.data.model.home.SAgree
import com.tirade.android.core.trashtalk.home.ui.adapter.TrashTalkViewersListAdapter

class ViewersFragment : DialogFragment() {
    private var viewersAdapter: TrashTalkViewersListAdapter? = null

    companion object {
        private var sAgreeList: ArrayList<SAgree>? = null
        fun showDialog(fragmentManager: FragmentManager, sAgree: ArrayList<SAgree>?) {
            val dialog = ViewersFragment()
            dialog.show(fragmentManager, "Viewers")
            sAgreeList = sAgree
        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.view_all_fragment, container)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme)
        val closeDialogFragment = view.findViewById<ImageView>(R.id.closeDialogFragment)
        closeDialogFragment.setOnClickListener {
            dialog!!.dismiss()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewersAdapter = TrashTalkViewersListAdapter(requireActivity())
        val recycler_view=view.findViewById<RecyclerView>(R.id.recycler_view)
        recycler_view!!.isNestedScrollingEnabled = false
        recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        viewersAdapter!!.setAppViewerList(sAgreeList!!)
        recycler_view.adapter = viewersAdapter
    }
}






