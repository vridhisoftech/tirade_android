package com.tirade.android.core.publick.editprofile.data.model

data class FavouriteImg(
    val s4_imgUrl: String
)