package com.tirade.android.core.auth.data.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tirade.android.core.auth.data.repo.AuthRepository
import com.tirade.android.core.auth.data.viewmodel.IncognitoViewModel
import com.tirade.android.core.auth.data.viewmodel.PublicViewModel

class AuthModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(IncognitoViewModel::class.java)) {
            IncognitoViewModel(AuthRepository.getInstance()) as T
        } else return if (modelClass.isAssignableFrom(PublicViewModel::class.java)) {
            PublicViewModel(AuthRepository.getInstance()) as T
        } else {
            //TODO for others view model repository
            throw IllegalArgumentException("Unknown ViewModel class") as Throwable
        }
    }

}