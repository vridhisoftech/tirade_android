package com.tirade.android.core.trashtalk.home.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.core.trashtalk.home.data.interfaces.CommentListener
import com.tirade.android.core.trashtalk.home.data.model.home.AllComments
import com.tirade.android.databinding.CommentsListBinding
import com.tirade.android.databinding.HomeListItemBinding
import com.tirade.android.utils.Helper
import kotlin.collections.ArrayList

class CommentsAdapter(private var listener: CommentListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mCommentList = ArrayList<AllComments>()
    var feeder: AllComments? = null
    fun setAppList(categoryModel: ArrayList<AllComments>) {
        mCommentList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mCommentList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        feeder = mCommentList[position]
        (holder as RecyclerViewHolder).bind(feeder!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = CommentsListBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: CommentsListBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: AllComments) {
            applicationBinding.viewModel = feed
            applicationBinding.commentDate.text=Helper.getAmericanDateFormat(feed.created_at.toString())
            applicationBinding.rightCommentText.text =feed.comment

        }


    }
}