package com.tirade.android.core.trashtalk.notification.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.core.trashtalk.notification.data.model.Data
import com.tirade.android.databinding.TrashNotificationListItemBinding
import com.tirade.player.core.exoplayer2.util.Log

class TrashNotificationListAdapter(private var requireActivity: FragmentActivity) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    Filterable {

    private val mCategoryList = ArrayList<Data>()
    private var mFilteredList = ArrayList<Data>()

    fun setAppList(categoryModel: ArrayList<Data>) {
        mCategoryList.clear()

        mCategoryList.addAll(categoryModel)

        mFilteredList=mCategoryList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mFilteredList.size)
        return mFilteredList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mFilteredList[position]
        (holder as RecyclerViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding =
            TrashNotificationListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: TrashNotificationListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root),
        View.OnClickListener {

        fun bind(feed: Data) {
            applicationBinding.viewModel = feed
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v) {
                itemView -> {
                    //listener.onCategoryClick(adapterPosition)
                }
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                mFilteredList = if (charString.isEmpty()) {
                    mCategoryList
                } else {
                    var filteredList: ArrayList<Data> = ArrayList()
                    for (row in mCategoryList) {
                        if (row.sender_username.toLowerCase().contains(charString.toLowerCase())||
                            row.message.contains(charSequence))
                            filteredList.add(row)
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = mFilteredList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                mFilteredList = filterResults.values as ArrayList<Data>
                notifyDataSetChanged()
            }
        }
    }
}