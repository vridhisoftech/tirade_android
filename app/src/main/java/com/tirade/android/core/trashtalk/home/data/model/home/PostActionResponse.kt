package com.tirade.android.core.trashtalk.home.data.model.home

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class PostActionResponse {
    @SerializedName("status")
    @Expose
     val status: Int? = null
    @Expose
     val message: String? = null
}