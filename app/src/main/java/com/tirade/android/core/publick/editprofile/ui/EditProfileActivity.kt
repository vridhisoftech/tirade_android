package com.tirade.android.core.publick.editprofile.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.PopupWindow
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.iceteck.silicompressorr.SiliCompressor
import com.tirade.android.BuildConfig
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.publick.editprofile.data.listener.EditProfileListener
import com.tirade.android.core.publick.editprofile.data.model.*
import com.tirade.android.core.publick.editprofile.data.viewmodel.EditProfileViewModel
import com.tirade.android.core.publick.factory.PublickFactory
import com.tirade.android.databinding.ActivityEditProfileBinding
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.toast
import com.tirade.android.widget.BottomSheetCameraGallery
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger
import kotlin.collections.ArrayList

class EditProfileActivity : AppCompatActivity(), EditProfileListener, BottomSheetCameraGallery.BottomSheetListener {

    private lateinit var fileUri: Uri
    private lateinit var mImageFileLocation: String
    private lateinit var binding: ActivityEditProfileBinding
    private lateinit var viewModel: EditProfileViewModel
    private var chooseImagePopUp: PopupWindow? = null

    private val TAG = "EditProfileActivity"

    companion object {
        private val CAMERA_PIC_REQUEST = 111
        private val IMAGE_PICK_CODE = 222
        private val REQUEST_PERMISSION = 100
        private val MEDIA_TYPE_IMAGE = 333
        private val IMAGE_DIRECTORY = "/TIRADE_IMAGES"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
        viewModel = ViewModelProvider(this@EditProfileActivity, PublickFactory()).get(EditProfileViewModel::class.java)
        binding.viewModel = viewModel

        //viewModel.listner = this

        getEditProfileData()

        val name = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.name
        userName.text = name

        val userProfile = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_profile_image
        Glide.with(userImage.context).load(userProfile).error(R.drawable.ic_user).into(userImage)


        imBack.setOnClickListener {
            finish()
        }

        setupPermissions()
    }

    private fun chooseFromCameraOrGallery() {
        val bottomSheet = BottomSheetCameraGallery()
        bottomSheet.show(supportFragmentManager, "BottomSheetCameraGallery")
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(this@EditProfileActivity, Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_PERMISSION
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    override fun onOptionClick(text: String) {
        when (text) {
            "CAMERA" -> {
                if (Build.VERSION.SDK_INT > 21) {
                    //use this if Lollipop_Mr1 (API 22) or above
                    val intent = Intent()
                    intent.action = MediaStore.ACTION_IMAGE_CAPTURE

                    // We give some instruction to the intent to save the image
                    var photoFile: File? = null
                    try {
                        // If the createImageFile will be successful, the photo file will have the address of the file
                        photoFile = createImageFile()
                        // Here we call the function that will try to catch the exception made by the throw function
                    } catch (e: IOException) {
                        Logger.getAnonymousLogger()
                            .info("Exception error in generating the file")
                        e.printStackTrace()
                    }
                    // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
                    val outputUri: Uri = FileProvider.getUriForFile(
                        this, BuildConfig.APPLICATION_ID.toString() + ".provider",
                        photoFile!!
                    )
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri)

                    // The following is a new line with a trying attempt
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    Logger.getAnonymousLogger().info("Calling the camera App by intent")

                    // The following strings calls the camera app and wait for his file in return.
                    startActivityForResult(intent, CAMERA_PIC_REQUEST)
                } else {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)

                    // start the image capture Intent
                    startActivityForResult(intent, CAMERA_PIC_REQUEST)
                }
            }
            "GALLERY" -> {
                pickImageFromGallery()
            }
        }
    }


    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), IMAGE_PICK_CODE)
    }

    /**
     * Creating file uri to store image/video
     */
    private fun getOutputMediaFileUri(type: Int): Uri {
        return Uri.fromFile(
            getOutputMediaFile(
                type
            )
        )
    }

    /**
     * returning image / video
     */
    private fun getOutputMediaFile(type: Int): File? {
        // External sdcard location
        val mediaStorageDir = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY)
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create $IMAGE_DIRECTORY directory")
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val mediaFile: File
        mediaFile = if (type == MEDIA_TYPE_IMAGE) {
            File(mediaStorageDir.path + File.separator + "IMG_" + ".jpg")
        } else {
            return null
        }
        return mediaFile
    }

    @Throws(IOException::class)
    fun createImageFile(): File? {
        Logger.getAnonymousLogger().info("Generating the image - method started")

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmSS").format(Date())
        val imageFileName = "IMAGE_$timeStamp"
        // Here we specify the environment location and the exact path where we want to save the so-created file
        val storageDirectory = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY)
        Logger.getAnonymousLogger().info("Storage directory set")

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir()

        // Here we create the file using a prefix, a suffix and a directory
        val image = File(storageDirectory, "$imageFileName.jpg")
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set")
        mImageFileLocation = image.absolutePath
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image
    }

    override fun onStop() {
        super.onStop()
        dismissPopup()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == IMAGE_PICK_CODE) {
                if (data != null) {
                    val image = data.data
                    val projection = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = contentResolver.query(image!!, projection, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()

                    val columnIndex = cursor.getColumnIndex(projection[0])
                    val mediaPath = cursor.getString(columnIndex)
                    cursor.close()

                    val fileDirSave = File(
                        this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/TIRADE_IMAGES"
                    )
                    val filePath = SiliCompressor.with(this).compress(mediaPath, fileDirSave)
                    setUpProfileImage(filePath)
                    viewModel.profileImg.set(filePath)
                }
            } else if (requestCode == CAMERA_PIC_REQUEST) {
                if (Build.VERSION.SDK_INT > 21) {
                    setUpProfileImage(mImageFileLocation)
                    val fileDirSave = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() +  "/TIRADE_IMAGES")
                    val filePath = SiliCompressor.with(this).compress(mImageFileLocation, fileDirSave)
                    viewModel.profileImg.set(filePath)
//                        viewModel.profileImg.set(mImageFileLocation)
                } else {
                    setUpProfileImage(fileUri)
//                        viewModel.profileImg.set(fileUri.path)
                    val fileDirSave = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() +  "/TIRADE_IMAGES")
                    val filePath = SiliCompressor.with(this).compress(fileUri.path, fileDirSave)
                    viewModel.profileImg.set(filePath)
                }
            }
        }
    }

    private fun setUpProfileImage(imoji: Any) {
        Glide.with(this)
            .asBitmap().load(imoji)
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Bitmap>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    bitmap: Bitmap?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    updateBitmapImage(bitmap)
                    return false
                }
            }
            ).submit()
    }

    fun updateBitmapImage(bitmap: Bitmap?) {
        this@EditProfileActivity.runOnUiThread(java.lang.Runnable {
            profileImage.setImageBitmap(bitmap)
        })
    }

    fun getEditProfileData() {
        viewModel.getProjectList()?.observe(this, Observer<ArrayList<EditProfileResponse>> {
            if (it != null) {
                progress_bar.visibility = View.GONE
                val getProfileData = it[0] as GetProfileData
                val editProfile = getProfileData.editProfile[0]
                viewModel.name.set(editProfile.s1_userName)
                viewModel.gender.set(editProfile.s1_userGender)
                viewModel.dob.set(editProfile.s1_userBirthdate)
                viewModel.profileImg.set(editProfile.s1_userImage)

                val addPeopleList: ArrayList<AddPeople> = getProfileData.addPeople
                val categoryAdapter = PeopleAdapter()
                val layoutManagerPeople = GridLayoutManager(this,1, GridLayoutManager.VERTICAL, false)
                recycler_view_people.layoutManager = layoutManagerPeople
                categoryAdapter.setAppList(addPeopleList)
                recycler_view_people.adapter = categoryAdapter

                val biography: Biography = getProfileData.Biography[0]
                viewModel.education.set(biography.s3_education)
                viewModel.where_born.set(biography.s3_born)
                viewModel.where_live.set(biography.s3_live)
                viewModel.where_work.set(biography.s3_work)
                viewModel.city_live.set(biography.s3_liveIn)

                val addFavouriteImgList: ArrayList<FavouriteImg> = getProfileData.favouriteImg
                val favoriteImagesAdapter = FavoriteImagesAdapter()
                val layoutManagerFavImages = GridLayoutManager(this,4, GridLayoutManager.VERTICAL, false)
                recycler_fav_images.layoutManager = layoutManagerFavImages
                favoriteImagesAdapter.setAppList(addFavouriteImgList)
                recycler_fav_images.adapter = favoriteImagesAdapter


                val hobby: Hobby = getProfileData.Hobbies[0]
                //viewModel.sport.set(hobby.s5_sport)
                viewModel.music.set(hobby.s5_music)
               // viewModel.singer.set(hobby.s5_singer)
                viewModel.hobby.set(hobby.s5_hobbies)
                viewModel.hero.set(hobby.s5_heros)
                viewModel.job.set(hobby.s5_dreamjob)
                viewModel.interest.set(hobby.s5_interested)

                viewModel.animalLover.set(hobby.s5_animalLover)
                viewModel.band.set(hobby.s5_band)
                viewModel.beliveInlife.set(hobby.s5_beliveInlife)
                viewModel.dreamCar.set(hobby.s5_dreamCar)
                viewModel.dreamVacation.set(hobby.s5_dreamVacation)
                viewModel.earning.set(hobby.s5_earning)
                viewModel.favouriteColor.set(hobby.s5_favouriteColor)
                viewModel.food.set(hobby.s5_food)
                viewModel.makesYouAngry.set(hobby.s5_makesYouAngry)
                viewModel.makesYouHappy.set(hobby.s5_makesYouHappy)
                viewModel.movies.set(hobby.s5_movies)
                viewModel.sadOrHappy.set(hobby.s5_sadOrHappy)
                viewModel.team.set(hobby.s5_team)

            }
        })
    }

    private fun dismissPopup() {
        chooseImagePopUp?.let {
            if (it.isShowing) {
                it.dismiss()
            }
            chooseImagePopUp = null
        }
    }

    override fun onFailure(statusMessage: String) {

    }

    override fun onCategoryClick(adapterPosition: Int) {

    }

    override fun toastShow(message: String) {

    }

    override fun onSuccess(message: String) {

    }

    override fun onEditProfile() {

    }

    override fun onCameraClick() {
        Log.i(TAG, "Camera Clicked")
        val permissionCam = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        )
        val permissionStorage = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        if (permissionCam == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to storage accepted")
            val bottomSheet = BottomSheetCameraGallery()
            bottomSheet.show(supportFragmentManager, "BottomSheetCameraGallery")
        } else if (permissionCam == PackageManager.PERMISSION_DENIED || permissionStorage == PackageManager.PERMISSION_DENIED) {
            Log.i(TAG, "Permission to storage always denied")
            val message: String = "We noticed you have disabled permission.\n " +
                    "Please enable  CAMERA and STORAGE permission from,\n" +
                    "Tirade Application settings\n"
            this.toast(message, true)
            setupPermissions()
        } else {
            setupPermissions()
        }
    }

    override fun onEditHobbies() {

    }

    override fun onEditFavImages() {

    }

    override fun onEditBiography() {

    }

    override fun onAddPeople() {

    }



}
