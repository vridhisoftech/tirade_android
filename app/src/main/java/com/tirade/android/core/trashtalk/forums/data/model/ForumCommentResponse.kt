package com.tirade.android.core.trashtalk.forums.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class ForumCommentResponse {
    @SerializedName("status")
    @Expose
    val status: Int? = null
    @SerializedName("message")
    @Expose
    val message: String? = null
    @SerializedName("comments")
    @Expose
    val comments: ArrayList<ForumComments>? = null

}