package com.tirade.android.core.auth.data.repo

import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.auth.data.model.publiclog.User
import com.tirade.android.core.trashtalk.trash.data.model.PostResponse
import com.tirade.android.network.TiradeClient
import com.tirade.android.network.TiradeApi
import com.tirade.android.store.PrefStoreManager
import okhttp3.MultipartBody
import okhttp3.RequestBody

class AuthRepository {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }


    /*suspend fun userIncognitoSignUp(user: User): AuthResponse {
        return apiServices.incognitoSignUp(user).await()
    }*/

    /*suspend fun userPublicSignUp(user: User): AuthResponse {
        return apiServices.publicSignUp(user).await()
    }*/

    suspend fun userIncognitoSignUp(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): AuthResponse {
        return apiServices.incognitoSignUp("token", params, fileBody).await()
    }

    suspend fun userPublicSignUp(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): AuthResponse {
        return apiServices.publicSignUp("token", params, fileBody).await()
    }


    fun saveDataForPublicUser(it: AuthResponse) {
        PrefStoreManager.clearPublic()
        PrefStoreManager.put(it, PrefStoreManager.PUBLIC_SIGN_UP)//add
    }

    fun getDataForPublicUser(): AuthResponse {
        return PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)!!
    }

    fun saveDataForIncognitoUser(it: AuthResponse) {
        PrefStoreManager.clearIncognito()
        PrefStoreManager.put(it, PrefStoreManager.INCOGNITO_SIGN_UP)//add
    }

    fun getDataForIncognitoUser(): AuthResponse {
        return PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)!!
    }


    companion object {
        fun getInstance(): AuthRepository {
            val mInstance: AuthRepository by lazy { AuthRepository() }
            return mInstance
        }
    }
}