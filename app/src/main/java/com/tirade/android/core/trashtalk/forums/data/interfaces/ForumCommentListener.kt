package com.tirade.android.core.trashtalk.forums.data.interfaces

import com.tirade.android.core.trashtalk.forums.data.model.ForumComments
interface ForumCommentListener {

    fun getComment(comment:ArrayList<ForumComments>)

    fun toastShow(message: String)

}