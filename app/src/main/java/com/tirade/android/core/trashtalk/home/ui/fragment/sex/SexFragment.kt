package com.tirade.android.core.trashtalk.home.ui.fragment.sex

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer

import com.tirade.android.R

class SexFragment : Fragment() {

    companion object {
        fun newInstance() = SexFragment()
    }

    private lateinit var viewModel: SexViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(SexViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_sex, container, false)
        val textView: TextView = root.findViewById(R.id.text_tools)
        viewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }

}
