package com.tirade.android.core.publick.tirade.data.model

data class TrendingData(
    val trending_videos: List<TrendingVideo>
)