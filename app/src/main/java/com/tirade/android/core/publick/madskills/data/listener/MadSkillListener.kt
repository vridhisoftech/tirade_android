package com.tirade.android.core.publick.madskills.data.listener

interface MadSkillListener {
    fun onVideo()
    fun onClickPdf()
    fun onVoice()
    fun onPhoto()
    fun onText()
    fun onContinue()
    fun onFacebook()
    fun onGoogle()
    fun onTwitter()
    fun onWhatsUp()
    fun toastShow(message: String)
    fun finishActivity()
    fun onSuccess(message: String)
}