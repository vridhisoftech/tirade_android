package com.tirade.android.core.trashtalk.home.data.repo

import com.tirade.android.core.trashtalk.home.data.model.home.PostActionResponse
import com.tirade.android.core.trashtalk.home.data.model.home.TrashHomeResponse
import com.tirade.android.core.trashtalk.home.data.model.newsfeed.NewsFeedResponse
import com.tirade.android.core.trashtalk.home.data.model.temp.PostAction
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient


class TrashTalkHomeRepository {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun fetchHomeData(): TrashHomeResponse {
        val response = apiServices.getTrashTalkHome().await()
        return response
    }

    suspend fun fetchNewsFeedData(): NewsFeedResponse {
        val response = apiServices.getTrashTalkNewsFeed().await()
        return response
    }

    suspend fun postActionToServer(params: PostAction): PostActionResponse {
        return apiServices.postAction("token", params).await()
    }

    fun saveDataForOffline(it: ArrayList<TrashHomeResponse>) {
        //Save response data for offline
    }

    companion object {
        fun getInstance(): TrashTalkHomeRepository {
            val mInstance: TrashTalkHomeRepository by lazy { TrashTalkHomeRepository() }
            return mInstance
        }
    }
}