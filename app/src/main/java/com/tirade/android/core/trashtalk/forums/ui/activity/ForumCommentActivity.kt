package com.tirade.android.core.trashtalk.forums.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumCommentListener
import com.tirade.android.core.trashtalk.forums.data.model.ForumComments
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumCommentActivityViewModel
import com.tirade.android.core.trashtalk.forums.ui.adapter.ForumCommentsAdapter
import com.tirade.android.core.trashtalk.home.data.interfaces.CommentListener
import com.tirade.android.core.trashtalk.home.data.interfaces.TrashTalkHomeListener
import com.tirade.android.databinding.ActivityForumCommentsBinding
import com.tirade.android.utils.toast
import kotlinx.android.synthetic.main.activity_forum_comments.view.*

class ForumCommentActivity : AppCompatActivity(), ForumCommentListener {

    private var scroll: ScrollView? = null
    private var commentsAdapter: ForumCommentsAdapter? = null
    private lateinit var binding: ActivityForumCommentsBinding
    private lateinit var viewModel: ForumCommentActivityViewModel
    var listener: TrashTalkHomeListener? = null
    var commentFlag = false
    var postComments: ArrayList<ForumComments>? = null


    var userId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forum_comments)
        viewModel = ViewModelProvider(this, TrashTalkModelFactory()).get(ForumCommentActivityViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.listner = this
        scroll = findViewById(R.id.scrollView)
        callMain()
    }
    private fun subscribeDataCallBack() {
        viewModel.getForumCommentList()?.observe(this, Observer {
            if (it != null) {
//                categoryAdapter.setAppList(it)
                postComments=it
                setRecyclerView(postComments!!)
            }
        })
    }
    private fun callMain() {
        binding.root.rlRightView.visibility = View.GONE
     val   user_id = intent.extras!!.getString("userId")
        Log.v("UserId",user_id)
        viewModel.forum_id.set(user_id)

//        setRecyclerView(postComments!!)
        subscribeDataCallBack()
    }

    private fun setRecyclerView(dataList: ArrayList<ForumComments>) {
        commentsAdapter = ForumCommentsAdapter(this)
        val categoryLinearLayoutManager = LinearLayoutManager(this)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.root.recycler_view.layoutManager = categoryLinearLayoutManager
        commentsAdapter!!.setAppList(dataList)
        binding.root.recycler_view.adapter = commentsAdapter

    }


    override fun getComment(comment: ArrayList<ForumComments>) {
        callMain()
        commentFlag = true
        binding.root.sendMessage.setText("")
        commentsAdapter!!.notifyDataSetChanged()

    }


    override fun toastShow(message: String) {
        toast(message)
    }

    override fun onBackPressed() {
        if (commentFlag) {
            val intent = Intent()
            setResult(2, intent)
        }
        finish()
        super.onBackPressed()
    }
}