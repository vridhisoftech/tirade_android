package com.tirade.android.core.auth.data.model.incognito

data class User(
    val name: String,
    val age: String,
    val zip: String,
    val gender: String
)