package com.tirade.android.core.publick.editprofile.data.model

data class Hobby(
    val s5_animalLover: String,
    val s5_band: String,
    val s5_beliveInlife: String,
    val s5_dreamCar: String,
    val s5_dreamVacation: String,
    val s5_dreamjob: String,
    val s5_earning: String,
    val s5_favouriteColor: String,
    val s5_food: String,
    val s5_heros: String,
    val s5_hobbies: String,
    val s5_interested: String,
    val s5_makesYouAngry: String,
    val s5_makesYouHappy: String,
    val s5_movies: String,
    val s5_music: String,
    val s5_sadOrHappy: String,
    val s5_team: String
)