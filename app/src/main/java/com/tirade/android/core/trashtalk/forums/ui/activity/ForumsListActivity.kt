package com.tirade.android.core.trashtalk.forums.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.tirade.android.R
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.forums.data.interfaces.ForumsListActivityListener
import com.tirade.android.core.trashtalk.forums.data.model.Data
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumsListViewModel
import com.tirade.android.core.trashtalk.forums.ui.adapter.ForumsListAdapter
import com.tirade.android.databinding.ActivityListForumsBinding
import com.tirade.android.utils.toast
import kotlinx.android.synthetic.main.activity_list_forums.view.*

class ForumsListActivity: AppCompatActivity(), ForumsListActivityListener {

    private lateinit var binding: ActivityListForumsBinding
    private lateinit var listViewModel: ForumsListViewModel
    private var dataList: ArrayList<Data> = ArrayList()
    private lateinit var categoryListAdapter: ForumsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_forums)
        listViewModel = ViewModelProvider(this, TrashTalkModelFactory()).get(ForumsListViewModel::class.java)
        binding.viewModel = listViewModel
        listViewModel.listener = this

        setRecyclerView(dataList)
        binding.etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                categoryListAdapter.filter.filter(binding.etSearch.text.toString())

            }
        })
        subscribeDataCallBack()
    }

    private fun subscribeDataCallBack() {
        listViewModel.getProjectList()?.observe(this, Observer<ArrayList<Data>> {
            if (it != null) {
                categoryListAdapter.setAppList(it)
            }
        })
    }

    private fun setRecyclerView(dataList: ArrayList<Data>) {
        categoryListAdapter = ForumsListAdapter(this)
        val categoryLinearLayoutManager = GridLayoutManager(this,2, GridLayoutManager.VERTICAL, false)
        binding.root.recycler_view.layoutManager = categoryLinearLayoutManager
        categoryListAdapter.setAppList(dataList)
        binding.root.recycler_view.adapter = categoryListAdapter
    }

    override fun onFailure(message: String) {

    }

    override fun onCategoryClick(position: Int) {
      this.toast("Postion $position")
    }

    override fun onBack() {
        finish()
    }
}
