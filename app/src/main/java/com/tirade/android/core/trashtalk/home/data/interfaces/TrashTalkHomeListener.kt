package com.tirade.android.core.trashtalk.home.data.interfaces


interface TrashTalkHomeListener {
    fun onFailure(message: String)
    fun onCategoryClick(position: Int)
    fun onShoutOutClick()
    fun onRepresentClick()
    fun onTrashClick()
    fun onDebateClick()
    fun onWblowerClick()
    fun onJokesClick()
    fun onForumsClick()
    fun onShameClick()
    fun onPostActionResponse(value:String)
    fun onPostActionClick(post_id:Int,user_id:Int,value:String)
    fun onSuccessComment(list:Boolean)


}