package com.tirade.android.core.trashtalk.notification.data.repo

import com.tirade.android.core.trashtalk.notification.data.model.Notification
import com.tirade.android.core.trashtalk.notification.data.model.NotificationResponse
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient

class TrashNotificationRepo {
    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun fetchData(payload: Notification): NotificationResponse {
        val response = apiServices.getTrashNotificationList(payload).await()
        return response

    }

    companion object {
        fun getInstance(): TrashNotificationRepo {
            val mInstance: TrashNotificationRepo by lazy { TrashNotificationRepo() }
            return mInstance
        }
    }
}