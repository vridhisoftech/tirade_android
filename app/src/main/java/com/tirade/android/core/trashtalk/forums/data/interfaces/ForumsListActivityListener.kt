package com.tirade.android.core.trashtalk.forums.data.interfaces

interface ForumsListActivityListener {
    fun onFailure(message: String)
    fun onCategoryClick(position: Int)
    fun onBack()
}