package com.tirade.android.core.trashtalk.home.data.model.home

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class CommentResponse {
    @SerializedName("status")
    @Expose
     val status: Int? = null
    @SerializedName("post_id")
    @Expose
     val postId: String? = null
    @SerializedName("author_id")
    @Expose
     val authorId: String? = null
    @SerializedName("comment")
    @Expose
     val comment: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
}