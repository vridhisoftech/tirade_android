package com.tirade.android.core.publick.blackmarket.data.listener


interface BlvckMarketListener {
    fun onFailure(message: String)
    fun onCategoryClick(position: Int)
    fun onTrashClick()
    fun onDebateClick()
    fun onWblowerClick()
    fun onJokesClick()
    fun onForumsClick()
    fun onShameClick()
    fun onPostActionResponse(value:String)
    fun onPostActionClick(post_id:Int,user_id:Int,value:String)
    fun onSuccessComment(list:Boolean)


}