package com.tirade.android.core.trashtalk.home.data.interfaces

interface MenuListItemListener {
    fun onItemSelected(position: Int)
}