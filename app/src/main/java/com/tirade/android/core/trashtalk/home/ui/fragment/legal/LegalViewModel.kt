package com.tirade.android.core.trashtalk.home.ui.fragment.legal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LegalViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Legal Fragment"
    }
    val text: LiveData<String> = _text
}