package com.tirade.android.core.publick.madskills.ui

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.tirade.android.R
import com.tirade.android.core.publick.madskills.data.model.home.Data
import com.tirade.android.core.publick.tirade.data.listener.TiradeListener
import com.tirade.android.databinding.MadSkillsHomeListItemBinding
import com.tirade.android.utils.Helper
import com.tirade.android.utils.toast
import java.util.*

class MadSkillsHomeListAdapter(
    private var listener: TiradeListener,
    private var requireActivity: FragmentActivity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {
    private val mCategoryList = ArrayList<Data>()
    private var mFilteredList = ArrayList<Data>()

    fun setAppList(categoryModel: ArrayList<Data>) {
        mCategoryList.addAll(categoryModel)
        mFilteredList = mCategoryList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + mCategoryList.size)
        return mFilteredList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = mFilteredList[position]
        (holder as RecyclerViewHolder).bind(feeder)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val applicationBinding = MadSkillsHomeListItemBinding.inflate(layoutInflater, parent, false)
        return RecyclerViewHolder(applicationBinding)
    }


    inner class RecyclerViewHolder(private var applicationBinding: MadSkillsHomeListItemBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: Data) {
            applicationBinding.feedViewModel = feed
            applicationBinding.date.text =
                Helper.getAmericanDateFormat(feed.created_date.toString())

            itemView.setOnClickListener {
                listener.onCategoryClick(adapterPosition)
            }

            applicationBinding.imPlus.setOnClickListener {
                if (applicationBinding.expMenu.visibility == View.VISIBLE) {
                    applicationBinding.expMenu.visibility = View.GONE
                } else {
                    applicationBinding.expMenu.visibility = View.VISIBLE
                }
            }

            applicationBinding.imMore.setOnClickListener {
                val popupMenu = PopupMenu(itemView.context, applicationBinding.imMore)
                popupMenu.menuInflater.inflate(R.menu.popup_trashtalk_menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.action_facebook ->
                            itemView.context.toast("Click More ${item.title} $position")
                        R.id.action_instagram ->
                            itemView.context.toast("Click More ${item.title} $position")
                        R.id.action_snapchat ->
                            itemView.context.toast("Click More ${item.title} $position")
                        R.id.action_tictok ->
                            itemView.context.toast("Click More ${item.title} $position")
                        R.id.action_twitter ->
                            itemView.context.toast("Click More ${item.title} $position")
                    }
                    true
                })
                popupMenu.show()
            }

            applicationBinding.like.setOnClickListener {
                listener.onPostActionClick(feed.post_id!!.toString().toInt(),"s_agree")
            }

            applicationBinding.disLike.setOnClickListener {
                listener.onPostActionClick(feed.post_id!!.toString().toInt(),"s_disagree")
            }

            applicationBinding.comment.setOnClickListener {
                /*val intent = Intent(requireActivity, TiradeCommentActivity::class.java)
                intent.putExtra("postId", feed.post_id.toString())
                intent.putExtra("type", "All Posts")

                intent.putExtra("comments", feed.comments)
                (requireActivity).startActivityForResult(intent,2)*/
            }

            applicationBinding.share.setOnClickListener {
                var path = ""
                 if(!feed.image_path.isNullOrEmpty()){
                    path = feed.image_path
                } else if(!feed.video_path.isNullOrEmpty()){
                    path = feed.video_path
                }
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type="text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, path);
                requireActivity.startActivity(Intent.createChooser(shareIntent, "Share Via"))
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                mFilteredList = if (charString.isEmpty()) {
                    mCategoryList
                } else {
                    var filteredList: ArrayList<Data> = ArrayList()
                    for (row in mCategoryList) {
                        if (row.user_name.toLowerCase().contains(charString.toLowerCase()) ||
                            row.business_name.toLowerCase().contains(charString.toLowerCase()) ||
                            row.message.toLowerCase().contains(charString.toLowerCase())
                        )
                            filteredList.add(row)
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = mFilteredList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                mFilteredList = filterResults.values as ArrayList<Data>
                notifyDataSetChanged()
            }
        }
    }
}
