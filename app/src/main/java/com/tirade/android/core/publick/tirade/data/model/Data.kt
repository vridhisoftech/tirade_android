package com.tirade.android.core.publick.tirade.data.model

import android.os.Parcelable
import com.tirade.android.core.trashtalk.home.data.model.home.*
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    val agree:  ArrayList<Agree>? = null,
    val audio_path: String? = null,
    val boo: ArrayList<Boo>? = null,
    val category: String? = null,
    val cheers: ArrayList<Cheer>? = null,
    val comments: ArrayList<AllComments>? = null,
    val created_date: String? = null,
    val disagree: ArrayList<Disagree>? = null,
    val how: String?  = null,
    val image_path: String?  = null,
    val message: String?  = null,
    val name: String?  = null,
    val notification: ArrayList<AllNotification>? = null,
    val pdf_path: String?  = null,
    val post_content: String?  = null,
    val post_id: String?  = null,
    val post_type: String?  = null,
    val s_agree: ArrayList<SAgree>? = null,
    val s_disagree: ArrayList<SDisagree>? = null,
    val txtfile_path: String?  = null,
    val user_id: String?  = null,
    val user_profile: String?  = null,
    val video_path: String?  = null,
    val what: String?  = null,
    val who: String?  = null
): Parcelable