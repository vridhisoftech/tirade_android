package com.tirade.android.core.auth.data.interfaces

interface AuthListener {
    fun onSuccess(message: String)
    fun onFailure(message: String)
    fun onImageSelection()
    fun openDatePicker()
}