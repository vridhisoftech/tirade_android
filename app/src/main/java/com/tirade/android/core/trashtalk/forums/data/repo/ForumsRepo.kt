package com.tirade.android.core.trashtalk.forums.data.repo

import com.tirade.android.core.trashtalk.forums.data.model.ForumCommentResponse
import com.tirade.android.core.trashtalk.forums.data.model.ForumComments
import com.tirade.android.core.trashtalk.trash.data.model.PostResponse
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ForumsRepo {
    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun postDataToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): PostResponse {
        return apiServices.postMultipartFromForum("token", params, fileBody).await()
    }

    suspend fun postCommentToServer(params: ForumComments): ForumCommentResponse {
        return apiServices.postForumComment("token", params).await()
    }
    suspend fun fetchCommentData(userId: RequestBody): ForumCommentResponse {
        return apiServices.getForumComment("token", userId).await()
    }

    companion object {
        fun getInstance(): ForumsRepo {
            val mInstance: ForumsRepo by lazy { ForumsRepo() }
            return mInstance
        }
    }
}