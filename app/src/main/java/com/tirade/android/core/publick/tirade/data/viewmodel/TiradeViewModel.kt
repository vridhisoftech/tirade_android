package com.tirade.android.core.publick.tirade.data.viewmodel

import android.text.TextUtils
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.publick.tirade.data.listener.TiradeListener
import com.tirade.android.core.publick.tirade.data.model.Data
import com.tirade.android.core.publick.tirade.data.model.TrendingData
import com.tirade.android.core.publick.tirade.data.model.TrendingVideo
import com.tirade.android.core.publick.tirade.data.model.Video
import com.tirade.android.core.publick.tirade.data.repo.TiradeRepository
import com.tirade.android.network.Connection
import com.tirade.android.core.trashtalk.home.data.model.temp.PostAction
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class TiradeViewModel(private val repository: TiradeRepository): ViewModel() {

    var listner: TiradeListener? = null

    private var dataList: ArrayList<Data> = ArrayList()
    private var dataListTrending: ArrayList<Video> = ArrayList()
    private val mutableHomeList: MutableLiveData<ArrayList<Data>> = MutableLiveData()
    private val mutableTrendingList: MutableLiveData<ArrayList<Video>> = MutableLiveData()
    var progress: ObservableInt = ObservableInt(View.GONE)
    var post_video: ObservableField<String> = ObservableField("")
    var post_image: ObservableField<String> = ObservableField("")
    var post_content: ObservableField<String> = ObservableField("")
    var mediaType: ObservableField<String> = ObservableField("")
    var file: ObservableField<File> = ObservableField()
    var postTypeKey: ObservableField<String> = ObservableField("")

    fun getHomeDataList(): LiveData<ArrayList<Data>>? {
        progress.set(View.VISIBLE)
        Coroutines.main {
            try {
                val response = repository.fetchHomeData()
                response.data.let {
                    dataList = it as ArrayList<Data>
                    mutableHomeList.postValue(dataList)//notify observable
                    progress.set(View.GONE)
                    return@main
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutableHomeList
    }

    fun getTrendingDataList(): LiveData<ArrayList<Video>>? {
        progress.set(View.VISIBLE)
        Coroutines.main {
            try {
                val response = repository.fetchTrendingData()
                response.trending_videos.let {
                    dataListTrending = it.get(0).videos
                    mutableTrendingList.postValue(dataListTrending)//notify observable
                    progress.set(View.GONE)
                    return@main
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
            progress.set(View.GONE)
        }

        return mutableTrendingList
    }

    fun updatePostOnServer(){
        val postVideo = post_video.get()!!
        val postImage = post_image.get()!!
        val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_id

        if (TextUtils.isEmpty(postVideo)
            && TextUtils.isEmpty(postImage)) {
            listner?.toastShow("Please select any media source!")
            return
        }

        if (!Connection.isConnected()) {
            listner?.toastShow("Check Internet Connection!")
            return
        }

        progress.set(View.VISIBLE)

        Coroutines.main {
            try {
                when {
                    mediaType.get().equals("image") -> {
                        file.set(File(postImage))
                        postTypeKey.set("post_image")
                    }
                    mediaType.get().equals("video") -> {
                        file.set(File(postVideo))
                        postTypeKey.set("post_video")
                    }
                }

                val file = file.get()!!
                val requestFile: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
                val fileBody: MultipartBody.Part = MultipartBody.Part.createFormData(postTypeKey.get()!!, file.name, requestFile)

                // create a map of data to pass along
                val userId: RequestBody = createRequestBody(userId.toString())
                val postContent: RequestBody = createRequestBody(post_content.get()!!)

                var params:HashMap<String, RequestBody> = HashMap()
                params["user_id"] = userId
                params["post_content"] = postContent

                val response = repository.postDataToServer(params, fileBody)
                if(response.status == 1){
                    listner?.onSuccess(response.message)
                }

                progress.set(View.GONE)

            } catch (e: ApiException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            } catch (e: NoInternetException) {
                progress.set(View.GONE)
                listner?.toastShow(e.message!!)
            }
        }
    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }

    fun onPostLikeAction(postId: String,value: String) {
        Coroutines.main {
            try {
                val userId = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_id

                val params = PostAction()
                params.post_id = postId.toInt()
                params.value = value
                params.user_id =userId!!.toInt()

                val response = repository.postActionToLikeServer(params)
                if (response.status == 1) {
                    listner?.onPostActionResponse(response.message!!)
                }

            } catch (e: ApiException) {
                listner?.onPostActionResponse(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onPostActionResponse(e.message!!)
            }
        }
    }

}