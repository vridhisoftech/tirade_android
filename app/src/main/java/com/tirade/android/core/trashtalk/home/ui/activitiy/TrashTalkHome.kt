package com.tirade.android.core.trashtalk.home.ui.activitiy

import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.ActivityNavigator
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.intro.LoginIntro
import com.tirade.android.core.trashtalk.home.data.interfaces.MenuListItemListener
import com.tirade.android.core.trashtalk.home.ui.adapter.MenuDrawerAdapter
import com.tirade.android.core.trashtalk.home.ui.fragment.home.TrashTalkHomeFragment
import com.tirade.android.core.trashtalk.notification.ui.TrashNotificationActivity
import com.tirade.android.core.trashtalk.settings.ui.TrashSettingActivity
import com.tirade.android.core.trashtalk.shame.ui.ShameActivity
import com.tirade.android.databinding.ActivityTrashTalkHomeBinding
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.goActivity
import com.tirade.menuarc.StateChangeListener
import kotlinx.android.synthetic.main.activity_trash_talk_home.*
import kotlinx.android.synthetic.main.app_bar_trash_talk_home.*
import kotlinx.android.synthetic.main.content_trash_talk_home.*


class TrashTalkHome : AppCompatActivity(), MenuListItemListener {

    private lateinit var searchListener: SearchListener
    private lateinit var binding: ActivityTrashTalkHomeBinding
    private var menuDataList: ArrayList<String> = ArrayList()
    private val catArray = arrayOf<String>(
        "All Posts",
        "Politics",
        "Race",
        "Government",
        "LBGT",
        "Business",
        "Legal",
        "Religion",
        "Sex",
        "Drugs",
        "Music",
        "Sports",
        "Education",
        "Law enforcement",
        "Medical",
        "Love",
        "Culture",
        "Language",
        "Behaviour",
        "Psyche",
        "Beauty",
        "Community",
        "Exercise",
        "Cooking",
        "Cosmology",
        "Dancing",
        "Gender",
        "Dream Interpretation",
        "Eschatology",
        "Courtship",
        "Ethics",
        "World Relations",
        "Etiquette",
        "Faith",
        "Food",
        "Taboos",
        "Funeral Rites",
        "Games",
        "Gift Giving",
        "Healing",
        "Hairstyle",
        "Housing",
        "Hygiene",
        "Incest",
        "Inheritance",
        "Rules",
        "Joking",
        "Kinship",
        "Relatives",
        "Systems",
        "Categorizing",
        "Sexual restrictions",
        "Soul Concept",
        "Luck",
        "Superstition",
        "Magic",
        "Marriage",
        "Mel times",
        "Medicine",
        "Obstetrics",
        "Pregnancy",
        "Childbirth",
        "Rituals",
        "Penal Sanctions",
        "Punishment of Crimes",
        "Personal Names",
        "Population",
        "Policy",
        "Post Natal Care",
        "Property Rights",
        "Propitiation of Supernatural Beings",
        "Puberty",
        "Customs",
        "Other")

    interface SearchListener {
        fun onSearchClick()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trash_talk_home)

        menuIcon.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        addFragment(TrashTalkHomeFragment.newInstance("All Posts"))
        val name = PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user?.name
        userName.text = name

        val userProfile = PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.user?.user_profile
        Glide.with(userImage.context).load(userProfile).error(R.drawable.img_placeholder).into(userImage)

        logout.setOnClickListener{
            this.goActivity(LoginIntro::class.java)
            finish()
            PrefStoreManager.clearIncognito()
        }

        tv_search.setOnClickListener {
            searchListener.onSearchClick()
        }

        //navController = Navigation.findNavController(this, R.id.nav_host)
        //NavigationUI.setupWithNavController(navView, navController)//drawer navigation
        //NavigationUI.setupWithNavController(bottomNavView, navController)//bottom navigation

        setUpBottomNavigationBar()

        val tiradeLogo = findViewById<View>(R.id.tirade_logo)

        arcMenu.setNothingClickListener(null)
        //arcMenu.setMenuIcon(ContextCompat.getDrawable(this,R.drawable.vision_tirade_logo))
        arcMenu.setStateChangeListener(object : StateChangeListener {
            override fun onMenuOpened() {
                //this@TrashTalkHome.toast("Menu Opened")
                if (arcMenu.mDrawable is Animatable) (arcMenu.mDrawable as Animatable).start()
                tiradeLogo.visibility = View.VISIBLE
            }

            override fun onMenuClosed() {
                //this@TrashTalkHome.toast("Menu Closed")
                tiradeLogo.visibility = View.GONE
            }
        })

        findViewById<View>(R.id.golive).setOnClickListener(subMenuClickListener)
        tiradeLogo.setOnClickListener{
            this.goActivity(LoginIntro::class.java)
            finish()
            //PrefStoreManager.clear()
        }

        for (item in catArray){
            menuDataList.add(item)
        }

        setRecyclerView(menuDataList)
    }

    fun registerSearchListner(searchListener: SearchListener) {
        this.searchListener = searchListener
    }

    private fun setRecyclerView(dataList: ArrayList<String>) {
        val categoryAdapter = MenuDrawerAdapter(this)
        val categoryLinearLayoutManager = GridLayoutManager(this,1, GridLayoutManager.VERTICAL, false)
        recycler_view_menu.layoutManager = categoryLinearLayoutManager
        categoryAdapter.setAppList(dataList)
        recycler_view_menu.adapter = categoryAdapter
    }

    private fun addFragment(fragment:Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.containerFrag, fragment)
        ft.commit()
    }

    private val subMenuClickListener = View.OnClickListener {
        when (it.id) {
            R.id.golive -> {
                this@TrashTalkHome.toast("GO LIVE is in Under Construction")
            }
        }
        arcMenu.toggleMenu()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == 2) {
            val type = data!!.getStringExtra("type")
            Log.v("Okay",type)
            addFragment(TrashTalkHomeFragment.newInstance(type))

        }
    }
    private fun setUpBottomNavigationBar() {
        bottomNavView.itemIconTintList = null//To show SVG
        //bottomNavView.getOrCreateBadge(R.id.bottom_nav_notification) // Show badge
        //bottomNavView.removeBadge(R.id.bottom_nav_notification) // Remove badge
        //val badge = bottomNavView.getBadge(R.id.bottom_nav_notification) // Get badge

        bottomNavView.setOnNavigationItemSelectedListener { item: MenuItem ->
            return@setOnNavigationItemSelectedListener when (item.itemId) {
                R.id.bottom_nav_home -> {
                    toast("Bottom HOME item click")
                    //navController.navigate(R.id.bottom_nav_home)
                    true
                }
                R.id.bottom_nav_settings -> {
                    //toast("Settings item clicked")
                    //navController.navigate(R.id.bottom_nav_settings)
                    this.goActivity(TrashSettingActivity::class.java)
                    true
                }
                R.id.bottom_nav_plus -> {
                    //toast("ADD item clicked")
                    //navController.navigate(R.id.bottom_nav_plus)
                    arcMenu.toggleMenu()
                    true
                }
                R.id.bottom_nav_notification -> {
                    //toast("Notification item clicked")
                   // navController.navigate(R.id.bottom_nav_notification)
                    this.goActivity(TrashNotificationActivity::class.java)
                    true
                }
                R.id.bottom_nav_info -> {
                    //toast("INFO item clicked")
                    //navController.navigate(R.id.bottom_nav_info)
                    this.goActivity(ShameActivity::class.java)
                    true
                }
                else -> false
            }
        }

    }

    private fun toast(s: String) {
        Toast.makeText(applicationContext, s, Toast.LENGTH_SHORT).show()
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            Navigation.findNavController(this, R.id.navController),
            drawerLayout
        )
    }

    override fun onItemSelected(position: Int) {
        addFragment(TrashTalkHomeFragment.newInstance(catArray[position]))
        drawerLayout.closeDrawer(GravityCompat.START)
    }
}
