package com.tirade.android.core.publick.madskills.data.model.topskills

data class TopSkillsResponse(
    val rate_top_skills: List<RateTopSkill>
)