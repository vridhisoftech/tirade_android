package com.tirade.android.core.trashtalk.forums.data.model

data class AgreeData(
    val agree: String,
    val created_at: String,
    val forum_id: String,
    val user_id: String,
    val user_img: String,
    val user_name: String
)