package com.tirade.android.core.publick.madskills.data.model.home

data class Data(
    val business_category: String,
    val business_image: String,
    val business_name: String,
    val created_date: String,
    val experience: String,
    val file_path: String,
    val height: String,
    val image_path: String,
    val message: String,
    val music_path: String,
    val otherSkills: String,
    val post_id: String,
    val user_id: String,
    val user_name: String,
    val user_profile: String,
    val video_path: String,
    val weight: String,
    val youRepresent: String
)