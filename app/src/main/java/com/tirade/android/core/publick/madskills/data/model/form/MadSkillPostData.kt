package com.tirade.android.core.publick.madskills.data.model.form

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MadSkillPostData(
    var user_id: Int? = null,
    var business_category:String?=null,
    var business_name:String?=null,
    var experience:String?=null,
    var youRepresent:String?=null,
    var height:String?=null,
    var weight:String?=null,
    var otherSkills:String?=null,
    var image_path: String?=null,
    var  video_path: String?=null,
    var music_path: String?=null,
    var file_path: String?=null) {

}