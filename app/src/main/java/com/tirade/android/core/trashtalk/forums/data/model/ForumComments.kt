package com.tirade.android.core.trashtalk.forums.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
 class ForumComments(  var user_id: String? = null,
         var forum_id: String? = null,
         var comment: String? = null,
        var created_at: String? = null,
         var name: String? = null,
        var user_img: String? = null) :Parcelable {


}