package com.tirade.android.core.trashtalk.home.data.model.home

import android.os.Parcel
import android.os.Parcelable

 class AllNotification() :Parcelable {
    var sender_id: String? = null
    var debate_reason: String? = null
    var created_at: String? = null
    var sender_username: String? = null
    var user_img: String? = null

    constructor(parcel: Parcel) : this() {
        sender_id =parcel.readString()
        debate_reason =parcel.readString()
        sender_username =parcel.readString()
        created_at =parcel.readString()
        user_img =parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sender_id)
        parcel.writeString(debate_reason)
        parcel.writeString(sender_username)
        parcel.writeString(created_at)
        parcel.writeString(user_img)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AllNotification> {
        override fun createFromParcel(parcel: Parcel): AllNotification {
            return AllNotification(
                parcel
            )
        }

        override fun newArray(size: Int): Array<AllNotification?> {
            android.util.Log.v("ArraySize",""+size)
            return arrayOfNulls<AllNotification>(size)

        }
    }
}
