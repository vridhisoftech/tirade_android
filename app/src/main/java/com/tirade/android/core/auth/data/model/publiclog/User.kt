package com.tirade.android.core.auth.data.model.publiclog

data class User(
    val user_id: String,
    val user_profile: String,
    val user_profile_image: String,
    val name : String,
    val birthday : String,
    val age : String,
    val gender : String,
    val zip : String,
    val email : String,
    val phone : String,
    val password : String
)