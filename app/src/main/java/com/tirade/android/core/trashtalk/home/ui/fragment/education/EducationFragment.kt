package com.tirade.android.core.trashtalk.home.ui.fragment.education

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer

import com.tirade.android.R

class EducationFragment : Fragment() {

    companion object {
        fun newInstance() = EducationFragment()
    }

    private lateinit var viewModel: EducationViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(EducationViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_education, container, false)
        val textView: TextView = root.findViewById(R.id.text_tools)
        viewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }

}
