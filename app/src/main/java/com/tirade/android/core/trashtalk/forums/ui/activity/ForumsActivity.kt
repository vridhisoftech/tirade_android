package com.tirade.android.core.trashtalk.forums.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.iceteck.silicompressorr.SiliCompressor
import com.nbsp.materialfilepicker.MaterialFilePicker
import com.nbsp.materialfilepicker.ui.FilePickerActivity
import com.tirade.android.BuildConfig
import com.tirade.android.R
import com.tirade.android.core.trashtalk.factory.TrashTalkModelFactory
import com.tirade.android.core.trashtalk.forums.data.viewmodel.ForumsViewModel
import com.tirade.android.core.trashtalk.trash.data.listener.TrashListener
import com.tirade.android.databinding.ActivityForumsBinding
import com.tirade.android.utils.RealPathUtil
import com.tirade.android.utils.goActivity
import com.tirade.android.utils.toast
import com.tirade.android.widget.BottomSheetCamVideoGallery
import kotlinx.android.synthetic.main.activity_forums.*
import kotlinx.android.synthetic.main.activity_forums.view.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger
import java.util.regex.Pattern

class ForumsActivity : AppCompatActivity(), TrashListener,
    BottomSheetCamVideoGallery.BottomSheetListener {

    private val TAG: String = "ForumsActivity"
    private lateinit var binding: ActivityForumsBinding
    private lateinit var viewModel: ForumsViewModel
    private val REQUEST__PERMISSION = 999
    private val CAMERA_PIC_REQUEST = 111
    private val IMAGE_PICK_CODE = 222
    private val MEDIA_TYPE_IMAGE = 333
    private val REQUEST_PICK_VIDEO = 444
    private val REQUEST_PICK_PDF = 555

    private lateinit var fileUri: Uri
    private lateinit var mImageFileLocation: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forums)
        viewModel = ViewModelProvider(this, TrashTalkModelFactory()).get(ForumsViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.listner = this
        binding.root.seeAllForums.setOnClickListener {
            finish()
            this.goActivity(ForumsListActivity::class.java)
        }
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            REQUEST__PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    this.toast("Permission denied")
                }
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {
                IMAGE_PICK_CODE -> {
                    if (data != null) {
                        val image = data.data
                        val projection = arrayOf(MediaStore.Images.Media.DATA)
                        val cursor = contentResolver.query(image!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()

                        val fileDirSave = File(
                            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                                .toString() + "/TIRADE_IMAGES"
                        )
                        val filePath = SiliCompressor.with(this).compress(mediaPath, fileDirSave)
                        viewModel.mediaType.set("image")
                        viewModel.forumImage.set(filePath)
                    }
                }
                CAMERA_PIC_REQUEST -> {
                    if (Build.VERSION.SDK_INT > 21) {
                        val fileDirSave = File(
                            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                                .toString() + "/TIRADE_IMAGES"
                        )
                        val filePath =
                            SiliCompressor.with(this).compress(mImageFileLocation, fileDirSave)
                        viewModel.forumImage.set(filePath)
                    } else {
                        val fileDirSave = File(
                            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                                .toString() + "/TIRADE_IMAGES"
                        )
                        val filePath = SiliCompressor.with(this).compress(fileUri.path, fileDirSave)
                        viewModel.forumImage.set(filePath)
                    }
                    viewModel.mediaType.set("image")
                }
                REQUEST_PICK_VIDEO -> {
                    var videoRealPath: String? = null
                    val video = data?.data
                    videoRealPath = try {
                        RealPathUtil.getRealPath(this, video)
                    } catch (e: java.lang.Exception) {
                        data?.data!!.path
                    }
                    val fileDirSave =
                        File(getExternalFilesDir(Environment.DIRECTORY_MOVIES).toString() + "/TIRADE_VIDEOS")
                    if (fileDirSave.mkdirs() || fileDirSave.isDirectory) {
                        VideoCompressAsyncTask(this).execute(videoRealPath, fileDirSave.path)
                    }
                    viewModel.mediaType.set("video")
                }
                REQUEST_PICK_PDF -> {
                    val textFileUri = data?.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                    viewModel.forumNotes.set(textFileUri)
                    viewModel.mediaType.set("pdf")
                }
            }
        }
    }

    private fun checkPermissionAndPickImage() {
        Log.i(TAG, "Camera Clicked")
        val permissionCam = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        )
        val permissionStorage = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        if (permissionCam == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to storage accepted")
            val bottomSheet = BottomSheetCamVideoGallery()
            bottomSheet.show(supportFragmentManager, "BottomSheetCameraGallery")
        } else if (permissionCam == PackageManager.PERMISSION_DENIED || permissionStorage == PackageManager.PERMISSION_DENIED) {
            Log.i(TAG, "Permission to storage always denied")
            val message: String = "We noticed you have disabled permission.\n " +
                    "Please enable  CAMERA and STORAGE permission from,\n" +
                    "Tirade Application settings\n"
            this.toast(message, true)
            setupPermissions()
        } else {
            setupPermissions()
        }
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(
                this@ForumsActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST__PERMISSION
            )
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select a file"),
            IMAGE_PICK_CODE
        )
    }

    private fun pickVideoFromGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "video/*"
        startActivityForResult(Intent.createChooser(intent, "Select a file"), REQUEST_PICK_VIDEO)
    }

    override fun onOptionClick(text: String) {
        when (text) {
            "CAMERA" -> {
                if (Build.VERSION.SDK_INT > 21) {
                    //use this if Lollipop_Mr1 (API 22) or above
                    val intent = Intent()
                    intent.action = MediaStore.ACTION_IMAGE_CAPTURE

                    // We give some instruction to the intent to save the image
                    var photoFile: File? = null
                    try {
                        // If the createImageFile will be successful, the photo file will have the address of the file
                        photoFile = createImageFile()
                        // Here we call the function that will try to catch the exception made by the throw function
                    } catch (e: IOException) {
                        Logger.getAnonymousLogger()
                            .info("Exception error in generating the file")
                        e.printStackTrace()
                    }
                    // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
                    val outputUri: Uri = FileProvider.getUriForFile(
                        this, BuildConfig.APPLICATION_ID.toString() + ".provider",
                        photoFile!!
                    )
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri)

                    // The following is a new line with a trying attempt
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    Logger.getAnonymousLogger().info("Calling the camera App by intent")

                    // The following strings calls the camera app and wait for his file in return.
                    startActivityForResult(intent, CAMERA_PIC_REQUEST)
                } else {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)

                    // start the image capture Intent
                    startActivityForResult(intent, CAMERA_PIC_REQUEST)
                }
            }
            "GALLERY" -> {
                pickImageFromGallery()
            }

            "VIDEO" -> {
                pickVideoFromGallery()
            }
        }
    }

    /**
     * Creating file uri to store image/video
     */
    private fun getOutputMediaFileUri(type: Int): Uri {
        return Uri.fromFile(
            getOutputMediaFile(
                type
            )
        )
    }

    /**
     * returning image / video
     */
    private fun getOutputMediaFile(type: Int): File? {
        // External sdcard location
        val mediaStorageDir =
            File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/TIRADE_IMAGES")
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create TIRADE_IMAGES directory")
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val mediaFile: File
        mediaFile = if (type == MEDIA_TYPE_IMAGE) {
            File(mediaStorageDir.path + File.separator + "IMG_" + ".jpg")
        } else {
            return null
        }
        return mediaFile
    }

    @Throws(IOException::class)
    fun createImageFile(): File? {
        Logger.getAnonymousLogger().info("Generating the image - method started")

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmSS").format(Date())
        val imageFileName = "IMAGE_$timeStamp"
        // Here we specify the environment location and the exact path where we want to save the so-created file
        val storageDirectory =
            File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/TIRADE_IMAGES")
        Logger.getAnonymousLogger().info("Storage directory set")

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir()

        // Here we create the file using a prefix, a suffix and a directory
        val image = File(storageDirectory, "$imageFileName.jpg")
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set")
        mImageFileLocation = image.absolutePath
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image
    }

    private fun pickPdfFromFileManager(){
        MaterialFilePicker()
            .withActivity(this)
            .withRequestCode(REQUEST_PICK_PDF)
            .withHiddenFiles(true)
            .withFilter(Pattern.compile(".*\\.pdf$"))
            .withTitle("Select PDF file")
            .start()
        //val intent = Intent()
        //intent.action = Intent.ACTION_OPEN_DOCUMENT
        //intent.addCategory(Intent.CATEGORY_OPENABLE)
        //intent.type = "*/*"
        //val extraMimeTypes = arrayOf("application/pdf", "application/doc")
        //intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes)
        //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        //startActivityForResult(intent, REQUEST_PICK_PDF)
    }

    override fun onChooseToDebate() {

    }


    override fun onVideo() {
     //TODO UNUSED
    }

    override fun onClickPdf() {
        pickPdfFromFileManager()
    }

    override fun onVoice() {
     //TODO UNUSED
    }

    override fun onPhoto() {
        checkPermissionAndPickImage()
    }

    override fun onText() {
        //TODO UNUSED
    }

    override fun onContinue() {

    }

    override fun onFacebook() {
        shareOnSocial("https://www.facebook.com/")
    }

    override fun onGoogle() {
        shareOnSocial("https://www.google.com/")
    }

    override fun onTwitter() {
        shareOnSocial("https://twitter.com/login?lang=en")
    }

    override fun onWhatsUp() {
        shareOnSocial("https://web.whatsapp.com/")
    }

    override fun toastShow(message: String) {
        toast(message)
    }

    override fun finishActivity() {
        finish()
        this.goActivity(ForumsListActivity::class.java)
    }

    override fun onSuccess(message: String) {
        showConfirmationMessage(message)
    }


    private fun showConfirmationMessage(text: String) {
        val dialog = Dialog(this@ForumsActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.confirm_message)

        (dialog.findViewById(R.id.message) as TextView).also {
            it.text = text
        }
        val ok = dialog.findViewById(R.id.ok) as Button
        ok.setOnClickListener {
            dialog.dismiss()
            finish()
            this.goActivity(ForumsListActivity::class.java)
        }

        dialog.show()
    }
    private fun shareOnSocial(message: String){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type="text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(shareIntent, "Share Via"))
    }


    @SuppressLint("StaticFieldLeak")
    inner class VideoCompressAsyncTask(private val mContext: Context) :
        AsyncTask<String?, String?, String>() {

        override fun doInBackground(vararg paths: String?): String {
            try {
                return SiliCompressor.with(mContext).compressVideo(paths[0], paths[1])
            } catch (e: Exception) {
                dismissDialogUpload()
                e.printStackTrace()
            }
            return ""
        }

        override fun onPostExecute(compressedFilePath: String) {
            super.onPostExecute(compressedFilePath)
            Log.i("CompressedPATH", "Path: $compressedFilePath")
            dismissDialogUpload()
            if (!TextUtils.isEmpty(compressedFilePath)) {
                viewModel.forumVideo.set(compressedFilePath)
            }
        }

        init {
            dialogUpload()
        }

        private fun dialogUpload() {
            progress.visibility = View.VISIBLE
        }

        private fun dismissDialogUpload() {
           progress.visibility = View.GONE
        }
    }
}
