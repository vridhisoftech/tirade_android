package com.tirade.android.core.publick.tirade.data.repo

import com.tirade.android.core.publick.tirade.data.model.Data
import com.tirade.android.core.publick.tirade.data.model.TiradeHomeResponse
import com.tirade.android.core.publick.tirade.data.model.TrendingData
import com.tirade.android.core.trashtalk.home.data.model.home.PostActionResponse
import com.tirade.android.core.trashtalk.trash.data.model.PostResponse
import com.tirade.android.core.trashtalk.home.data.model.temp.PostAction
import com.tirade.android.network.TiradeApi
import com.tirade.android.network.TiradeClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class TiradeRepository {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { TiradeClient.client().create(TiradeApi::class.java) }

    suspend fun fetchHomeData(): TiradeHomeResponse {
        return apiServices.getTiradeHomeList().await()
    }

    suspend fun fetchTrendingData(): TrendingData {
        return apiServices.getTiradeTrendingList().await()
    }

    suspend fun postDataToServer(params: HashMap<String, RequestBody>, fileBody: MultipartBody.Part): PostResponse {
        return apiServices.createPostForTirade("token", params, fileBody).await()
    }

    suspend fun postActionToLikeServer(params: PostAction): PostActionResponse {
        return apiServices.postLikeAction("token", params).await()
    }

    fun saveDataForOffline(it: List<Data>) {
        //Save response data for offline
    }

    companion object{
        fun getInstance(): TiradeRepository{
            val mInstance: TiradeRepository by lazy { TiradeRepository() }
            return mInstance
        }
    }
}