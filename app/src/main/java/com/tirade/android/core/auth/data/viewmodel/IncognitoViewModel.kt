package com.tirade.android.core.auth.data.viewmodel

import android.graphics.Bitmap
import android.graphics.Color
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.tirade.android.R
import com.tirade.android.core.auth.data.interfaces.AuthListener
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.auth.data.model.publiclog.User
import com.tirade.android.core.auth.data.repo.AuthRepository
import com.tirade.android.network.Connection
import com.tirade.android.utils.ApiException
import com.tirade.android.utils.Coroutines
import com.tirade.android.utils.NoInternetException
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class IncognitoViewModel(private val repository: AuthRepository) : ViewModel() {
    var listener: AuthListener? = null

    var customname: ObservableField<String> = ObservableField("")
    var age: ObservableField<String> = ObservableField("")
    var zip: ObservableField<String> = ObservableField("")
    var gender: ObservableField<String> = ObservableField("Male")
    var profileImg: ObservableField<String> = ObservableField("")
    var progress: ObservableInt = ObservableInt(View.GONE)
    //val incognitoUser: AuthResponse = repository.getDataForIncognitoUser()

    /*fun onSelectItemZip(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
        zip.set(parent!!.selectedItem.toString())
        val textView = parent.getChildAt(0) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 20f
    }*/

    fun onSelectItemAge(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
        age.set(parent!!.selectedItem.toString())
        val textView = parent.getChildAt(0) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 20f
    }

    fun onCheckedChanged(radioGroup: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.male -> {
                gender.set("Male")
            }
            R.id.female -> {
                gender.set("Female")
            }
            R.id.other -> {
                gender.set("Other")
            }
        }
    }

    fun onImageSelection() {
        listener?.onImageSelection()
    }

    fun enterSignUp() {
        val name = customname.get()!!
        val age = age.get()!!
        val zip = zip.get()!!
        val gender = gender.get()!!
        val profileImg = profileImg.get()!!

        if(TextUtils.isEmpty(name)){
            listener?.onFailure("Name filed is invalid!")
            return
        }else if(TextUtils.isEmpty(age)|| age == "Birth date"){
            listener?.onFailure("Age filed is invalid!")
            return
        }else if(TextUtils.isEmpty(zip)){
            listener?.onFailure("Zip code filed is invalid!")
            return
        }else if(TextUtils.isEmpty(gender)){
            listener?.onFailure("Select your gender!")
            return
        } else if(TextUtils.isEmpty(profileImg)){
            listener?.onFailure("Choose profile image!")
            return
        }

        if (!Connection.isConnected()) {
            listener?.onFailure("Check Internet Connection!")
            return
        }

        progress.set(View.VISIBLE)
        Coroutines.main {
            try {
                   val file = File(profileImg)
                   val requestFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), file)
                   val fileBody: MultipartBody.Part = MultipartBody.Part.createFormData("user_profile", file.name, requestFile)

                   val name: RequestBody = createRequestBody(name)
                   val age: RequestBody = createRequestBody(age)
                   val zip: RequestBody = createRequestBody(zip)
                   val gender: RequestBody = createRequestBody(gender)


                  var params: HashMap<String, RequestBody> = HashMap()
                  params["name"] = name
                  params["age"] = age
                  params["zip"] = zip
                  params["gender"] = gender

                lateinit var authResponse: AuthResponse
                try {
                     authResponse = repository.userIncognitoSignUp(params, fileBody)

                    //val user = User(user_profile = profileImg, name = name,age = age,zip = zip,gender = gender, phone = "", password = "", birthday = "", email = "", user_id ="")
                    //val authResponse = repository.userIncognitoSignUp(user)

                    repository.saveDataForIncognitoUser(authResponse)// save data in shared pref
                }catch (e: Exception){
                    e.printStackTrace()
                }
                if (authResponse.status == 0) {
                    //Failed
                    progress.set(View.GONE)
                    listener?.onFailure(authResponse.message)
                    return@main
                } else {
                    //Success
                    authResponse.message.let {
                        progress.set(View.GONE)
                        listener?.onSuccess(it)
                        return@main
                    }
                }
            } catch (e: ApiException) {
                progress.set(View.GONE)
                listener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                progress.set(View.GONE)
                listener?.onFailure(e.message!!)
            }
        }
    }

    private fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }

    fun openLogin() {

    }
}
