package com.tirade.android.core.publick.madskills.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Animatable
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.iceteck.silicompressorr.SiliCompressor
import com.tirade.android.R
import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.auth.ui.IncognitoLogin
import com.tirade.android.core.auth.ui.PublicLogin
import com.tirade.android.core.intro.LoginIntro
import com.tirade.android.core.publick.blackmarket.ui.BlvckMarketIntro
import com.tirade.android.core.publick.editprofile.ui.EditProfileActivity
import com.tirade.android.core.publick.factory.PublickFactory
import com.tirade.android.core.publick.madskills.data.viewmodel.MadSkillsHomeViewModel
import com.tirade.android.core.publick.tirade.data.listener.TiradeListener
import com.tirade.android.core.publick.tirade.data.model.Video
import com.tirade.android.core.publick.visionboard.ui.VisionBoardActivity
import com.tirade.android.core.trashtalk.home.data.interfaces.MenuListItemListener
import com.tirade.android.core.trashtalk.home.ui.activitiy.TrashTalkHome
import com.tirade.android.core.trashtalk.home.ui.adapter.MenuDrawerAdapter
import com.tirade.android.core.trashtalk.notification.ui.TrashNotificationActivity
import com.tirade.android.core.trashtalk.settings.ui.TrashSettingActivity
import com.tirade.android.core.trashtalk.shame.ui.ShameActivity
import com.tirade.android.databinding.ActivityMadSkillsHomeBinding
import com.tirade.android.store.PrefStoreManager
import com.tirade.android.utils.Helper
import com.tirade.android.utils.RealPathUtil
import com.tirade.android.utils.goActivity
import com.tirade.android.utils.toast
import com.tirade.android.widget.BottomSheetTiradeCreatePost
import com.tirade.menuarc.StateChangeListener
import kotlinx.android.synthetic.main.activity_mad_skills_home.*
import kotlinx.android.synthetic.main.mad_skill_content.*
import kotlinx.android.synthetic.main.mad_skill_content.view.*
import kotlinx.android.synthetic.main.mad_skill_content.view.cross
import kotlinx.android.synthetic.main.mad_skill_content.view.etSearch
import kotlinx.android.synthetic.main.mad_skill_content.view.imInfo
import kotlinx.android.synthetic.main.mad_skill_content.view.imPlus
import kotlinx.android.synthetic.main.mad_skill_content.view.logout
import kotlinx.android.synthetic.main.mad_skill_content.view.menuIcon
import kotlinx.android.synthetic.main.mad_skill_content.view.progress_bar
import kotlinx.android.synthetic.main.mad_skill_content.view.recycler_view
import kotlinx.android.synthetic.main.mad_skill_content.view.recycler_view_trending
import kotlinx.android.synthetic.main.mad_skill_content.view.searchView
import kotlinx.android.synthetic.main.mad_skill_content.view.swipeRefresh
import kotlinx.android.synthetic.main.mad_skill_content.view.tv_search
import kotlinx.android.synthetic.main.mad_skill_content.view.userImage
import kotlinx.android.synthetic.main.mad_skill_content.view.userName
import java.io.File

class MadSkillsHomeActivity : AppCompatActivity(), TiradeListener, MenuListItemListener,
    BottomSheetTiradeCreatePost.BottomSheetListenerTirade {

    companion object {
        private val PERMISSION_CODE = 1000
        private val IMAGE_PICK_CODE = 1001
        private val REQUEST_PICK_VIDEO = 1002
    }

    private var mDialog: ProgressDialog? = null
    private lateinit var binding: ActivityMadSkillsHomeBinding
    private lateinit var viewModel: MadSkillsHomeViewModel
    private var dataList: ArrayList<String> = ArrayList()
    private lateinit var madSkillsHomeListAdapter: MadSkillsHomeListAdapter
    private lateinit var topSkillsAdapter: TopSkillsAdapter
    private lateinit var skillsAdapter: SkillsAdapter

    private val catArray = arrayOf<String>(
        "Acting",
        "Music",
        "Art",
        "Rap",
        "Poetry",
        "Sports",
        "Sports",
        "Singing",
        "Games")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Helper.setFullScreen(this, window)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mad_skills_home)
        viewModel = ViewModelProvider(
            this@MadSkillsHomeActivity,
            PublickFactory()
        ).get(MadSkillsHomeViewModel::class.java)

        binding.viewModel = viewModel

        viewModel.listner = this
        if (mDialog == null) {
            mDialog = ProgressDialog(this)
        }

        swipeRefresh.setOnRefreshListener {
            refresh()
        }

        setUpHomeListRecyclerView()
        setUpSkillsListRecyclerView()
        setUpTopSkillsListRecyclerView()

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                madSkillsHomeListAdapter.filter.filter(etSearch.text.toString())

            }
        })

        madSkillsTopSkillsCall()
        madSkillsListCall()
        madSkillsHomeListCall()

        val tiradeLogo = findViewById<View>(R.id.tirade_logo)

        arcMenu.setNothingClickListener(null)
        //arcMenu.setMenuIcon(ContextCompat.getDrawable(this,R.drawable.vision_tirade_logo))
        arcMenu.setStateChangeListener(object : StateChangeListener {
            override fun onMenuOpened() {
                //this@TrashTalkHome.toast("Menu Opened")
                if (arcMenu.mDrawable is Animatable) (arcMenu.mDrawable as Animatable).start()
                tiradeLogo.visibility = View.VISIBLE
            }

            override fun onMenuClosed() {
                //this@TrashTalkHome.toast("Menu Closed")
                tiradeLogo.visibility = View.GONE
            }
        })

        findViewById<View>(R.id.golive).setOnClickListener {
            this.toastShow("Under construction")
            arcMenu.toggleMenu()
        }

        findViewById<View>(R.id.notification).setOnClickListener {
            this.toastShow("Under construction")
            arcMenu.toggleMenu()
        }

        findViewById<View>(R.id.home).setOnClickListener {

        }

        tiradeLogo.setOnClickListener {
            this.goActivity(LoginIntro::class.java)
            finish()
            //PrefStoreManager.clear()
        }

        for (item in catArray) {
            dataList.add(item)
        }

        setMenuRecyclerView(dataList)

        menuIcon.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        val name = PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.name
        userName.text = name

        val userProfile =
            PrefStoreManager.get<AuthResponse>(PrefStoreManager.PUBLIC_SIGN_UP)?.user?.user_profile_image
        Glide.with(userImage.context).load(userProfile).error(R.drawable.ic_user).into(userImage)

        logout.setOnClickListener {
            this.goActivity(PublicLogin::class.java)
            finish()
            PrefStoreManager.clearPublic()
        }

        tv_search.setOnClickListener {
            if (searchView.visibility == View.VISIBLE) {
                searchView.visibility = View.GONE
            } else {
                searchView.visibility = View.VISIBLE
            }
        }

        cross.setOnClickListener {
            etSearch.setText("")
            searchView.visibility = View.GONE
        }

        imHome.setOnClickListener {
            toast("Bottom HOME item click")
        }

        imSettings.setOnClickListener {
            this.goActivity(TrashSettingActivity::class.java)
        }

        imPlus.setOnClickListener {
            //this.goActivity(VisionBoardActivity::class.java)
            arcMenu.toggleMenu()
        }

        imNotification.setOnClickListener {
            this.goActivity(TrashNotificationActivity::class.java)
        }

        imInfo.setOnClickListener {
            this.goActivity(ShameActivity::class.java)
        }

    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                } else{
                    //permission from popup denied
                    this.toast("Permission denied")
                }
            }
        }
    }

    private fun checkPermissionAndPickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery()
            }
        } else{
            //system OS is < Marshmallow
            pickImageFromGallery()
        }
    }


    private fun pickImageFromGallery() {
        viewModel.mediaType.set("image")
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(Intent.createChooser(intent, "Select a file"), IMAGE_PICK_CODE)
    }

    private fun pickVideoFromGallery() {
        viewModel.mediaType.set("video")
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "video/*"
        startActivityForResult(Intent.createChooser(intent, "Select a file"), REQUEST_PICK_VIDEO)
    }

    private fun setMenuRecyclerView(dataList: ArrayList<String>) {
        val categoryAdapter = MenuDrawerAdapter(this)
        val categoryLinearLayoutManager =
            GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false)
        recycler_view_menu.layoutManager = categoryLinearLayoutManager
        categoryAdapter.setAppList(dataList)
        recycler_view_menu.adapter = categoryAdapter
    }

    private fun madSkillsTopSkillsCall() {
        viewModel.madSkillsTopSkillsCall()?.observe(
            this,
            Observer<ArrayList<com.tirade.android.core.publick.madskills.data.model.topskills.Artist>> {
                if (it != null) {
                    progress_bar.visibility = View.GONE
                    topSkillsAdapter.setAppList(it)
                }
            })
    }

    private fun madSkillsListCall() {
        viewModel.madSkillsListCall()?.observe(
            this,
            Observer<ArrayList<com.tirade.android.core.publick.madskills.data.model.skills.Data>> {
                if (it != null) {
                    progress_bar.visibility = View.GONE
                    skillsAdapter.setAppList(it)
                }
            })
    }

    private fun madSkillsHomeListCall() {
        viewModel.madSkillsHomeListCall()?.observe(
            this,
            Observer<ArrayList<com.tirade.android.core.publick.madskills.data.model.home.Data>> {
                if (it != null) {
                    progress_bar.visibility = View.GONE
                    madSkillsHomeListAdapter.setAppList(it)
                }
            })
    }

    private fun setUpTopSkillsListRecyclerView() {
        topSkillsAdapter = TopSkillsAdapter(this)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recycler_view_pager.layoutManager = layoutManager
        recycler_view_pager.adapter = topSkillsAdapter
    }

    private fun setUpSkillsListRecyclerView() {
        skillsAdapter = SkillsAdapter(this)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recycler_view_trending.layoutManager = layoutManager
        recycler_view_trending.adapter = skillsAdapter
    }

    private fun setUpHomeListRecyclerView() {
        madSkillsHomeListAdapter = MadSkillsHomeListAdapter(this, this)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler_view.layoutManager = layoutManager
        recycler_view.adapter = madSkillsHomeListAdapter
        binding.root.swipeRefresh.isRefreshing = false
    }

    private fun refresh() {
        binding.root.swipeRefresh.isRefreshing = true
        madSkillsHomeListAdapter.notifyDataSetChanged()
        setUpHomeListRecyclerView()
        madSkillsHomeListCall()
    }

    override fun onFailure(statusMessage: String) {}

    override fun onCategoryClick(position: Int) {}

    override fun onItemClick(feed: Video) {

    }

    override fun toastShow(message: String) {
        this.toast(message)
    }

    override fun onSuccess(message: String) {
        showConfirmationMessage(message)
    }

    fun onPostCreateClick(view: View) {
        val bottomSheet = BottomSheetTiradeCreatePost()
        bottomSheet.show(supportFragmentManager, "BottomSheetTiradeCreatePost")
    }

    override fun onPostActionClick(postId: Int?, s: String) {
        viewModel.onPostLikeAction(
            postId.toString(),s
        )
    }

    override fun onPostActionResponse(message: String?) {
        toast(message!!)
    }

    fun onTrashTalk(view: View) {
        if (PrefStoreManager.get<AuthResponse>(PrefStoreManager.INCOGNITO_SIGN_UP)?.status == 1) {
            Intent(this, TrashTalkHome::class.java).also {
                startActivity(it)
            }
        }else {
            this.goActivity(IncognitoLogin::class.java)
        }
        finish()
    }

    fun onMadSkills(view: View) {
        this.goActivity(MadSkillIntro::class.java)
        //finish()
    }
    fun onBlackMarket(view: View) {
        this.goActivity(BlvckMarketIntro::class.java)
        //finish()
    }

    private fun showConfirmationMessage(text: String) {
        val dialog = Dialog(this@MadSkillsHomeActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.confirm_message)

        (dialog.findViewById(R.id.message) as TextView).also {
            it.text = text
        }
        val ok = dialog.findViewById(R.id.ok) as Button
        ok.setOnClickListener {
            dialog.dismiss()
            onResume()
        }

        dialog.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                when (requestCode) {
                    IMAGE_PICK_CODE -> {
                        val image = data.data
                        val projection = arrayOf(MediaStore.Images.Media.DATA)
                        val cursor = contentResolver.query(image!!, projection, null, null, null)
                        assert(cursor != null)
                        cursor!!.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(projection[0])
                        val mediaPath = cursor.getString(columnIndex)
                        cursor.close()

                        val fileDirSave = File(
                            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                                .toString() + "/TIRADE_IMAGES"
                        )
                        val imagePath = SiliCompressor.with(this).compress(mediaPath, fileDirSave)
                        viewModel.post_image.set(imagePath)
                    }

                    REQUEST_PICK_VIDEO -> {
                        var videoRealPath: String? = null
                        val video = data.data
                        videoRealPath = try {
                            RealPathUtil.getRealPath(this, video)
                        } catch (e: java.lang.Exception) {
                            data.data!!.path
                        }
                        val fileDirSave = File(
                            this.getExternalFilesDir(Environment.DIRECTORY_MOVIES)
                                .toString() + "/TIRADE_VIDEOS"
                        )
                        if (fileDirSave.mkdirs() || fileDirSave.isDirectory) {
                            VideoCompressAsyncTask(this).execute(videoRealPath, fileDirSave.path)
                        }
                    }

                    2 -> {
                        //val type = data!!.getStringExtra("type")
                        //  refresh()
                    }

                }
            }
            // check if the request code is same as what is passed  here it is 2
            if (requestCode == 2) {
                refresh()
            }
        }
    }


    override fun onItemSelected(position: Int) {
        //TODO(Click filter needs to apply)
    }

    override fun onPickImage() {
        checkPermissionAndPickImage()
    }

    override fun onPickVideo() {
        pickVideoFromGallery()
    }

    override fun onPostClick(content: String) {
        viewModel.post_content.set(content)
        viewModel.updatePostOnServer()
    }

    @SuppressLint("StaticFieldLeak")
    inner class VideoCompressAsyncTask(private val mContext: Context) : AsyncTask<String?, String?, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg paths: String?): String {
            try {
                return SiliCompressor.with(mContext).compressVideo(paths[0], paths[1])
            } catch (e: Exception) {
                dismissDialogUpload()
                e.printStackTrace()
            }
            return ""
        }

        override fun onPostExecute(compressedFilePath: String) {
            super.onPostExecute(compressedFilePath)
            Log.i("CompressedPATH", "Path: $compressedFilePath")
            dismissDialogUpload()
            if (!TextUtils.isEmpty(compressedFilePath)) {
                viewModel.post_video.set(compressedFilePath)
            }
        }

        init {
            dialogUpload("Compressing Video", "Please Wait")
        }
    }

    private fun dialogUpload(title: String, message: String) {
        mDialog!!.setCancelable(false)
        mDialog!!.setTitle(title)
        mDialog!!.setMessage(message)
        mDialog!!.show()
    }

    private fun dismissDialogUpload() {
        if (mDialog != null) {
            mDialog!!.dismiss()
        }
    }


}

