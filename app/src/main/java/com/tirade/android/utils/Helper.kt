package com.tirade.android.utils

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.Window
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class Helper {
    companion object {
        fun setFullScreen(context: Context ,window: Window){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val decorView = window.decorView
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                                               View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                                               View.SYSTEM_UI_FLAG_LOW_PROFILE or
                                               View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                                               View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                                               View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                decorView.setOnApplyWindowInsetsListener { v, insets ->
                    val defaultInsets = v.onApplyWindowInsets(insets)
                    defaultInsets.replaceSystemWindowInsets(
                        defaultInsets.systemWindowInsetLeft,
                        0,
                        defaultInsets.systemWindowInsetRight,
                        defaultInsets.systemWindowInsetBottom
                    )
                }
                ViewCompat.requestApplyInsets(decorView)
                //Make the status bar transparent, if you don't want to be transparent, you can set a different color
                window.statusBarColor = ContextCompat.getColor(context, android.R.color.transparent)
                window.navigationBarColor = (ContextCompat.getColor(context,android.R.color.transparent))
            }
        }

        /**Check email validation*/
        fun isEmailValid(email: CharSequence): Boolean {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }


        fun playerView(webView: WebView, progressBar: ProgressBar, url: String) {
            val webSettings: WebSettings = webView.settings
            webSettings.javaScriptEnabled = true
            //webSettings.setMediaPlaybackRequiresUserGesture(WebSettings.RenderPriority.HIGH);
            //webSettings.setMediaPlaybackRequiresUserGesture(WebSettings.RenderPriority.HIGH);
            webSettings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
            webSettings.setAppCacheEnabled(true)
            webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            webSettings.domStorageEnabled = true
            webSettings.saveFormData = true
            webSettings.setEnableSmoothTransition(true)
            webSettings.allowContentAccess = true
            webSettings.allowFileAccess = true
            webSettings.pluginState = WebSettings.PluginState.ON_DEMAND
            webSettings.loadWithOverviewMode = true
            webSettings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
            webSettings.pluginState = WebSettings.PluginState.ON
            webSettings.databaseEnabled = true
            webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH)
            webSettings.useWideViewPort = true
            webView.setBackgroundColor(Color.BLACK);
            webView.isHorizontalScrollBarEnabled = false
            webView.isScrollbarFadingEnabled = true
            webView.setNetworkAvailable(true)
            webView.loadUrl(url)
            progressBar.visibility = View.VISIBLE
            webView.webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(
                    view: WebView,
                    newProgress: Int
                ) {
                    super.onProgressChanged(view, newProgress)
                    if (newProgress >= 100) {
                        progressBar.visibility = View.GONE
                    } else {
                        if (progressBar.visibility == View.GONE) {
                            progressBar.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }

        fun getAmericanDateFormat(date: String): String{
                var strDate: String? = ""
                val utilDate: Date
                val sqlDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                try {
                    val calTempDate = Calendar.getInstance()
                    calTempDate.firstDayOfWeek = Calendar.MONDAY
                    utilDate = sqlDateFormat.parse(date)
                    calTempDate.time = utilDate
                    strDate = SimpleDateFormat("MMMM dd, yyyy HH:mm a")
                        .format(calTempDate.time) //output format
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return strDate!!
        }
    }
}