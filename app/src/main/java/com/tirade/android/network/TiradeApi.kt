package com.tirade.android.network

import com.tirade.android.core.auth.data.model.publiclog.AuthResponse
import com.tirade.android.core.publick.editprofile.data.model.EditProfileResponse
import com.tirade.android.core.publick.madskills.data.model.home.MadHomeResponse
import com.tirade.android.core.publick.madskills.data.model.form.MadSkillFormResponse
import com.tirade.android.core.publick.madskills.data.model.skills.MadSkillsList
import com.tirade.android.core.publick.madskills.data.model.topskills.TopSkillsResponse
import com.tirade.android.core.publick.tirade.data.model.TiradeHomeResponse
import com.tirade.android.core.publick.tirade.data.model.TrendingData
import com.tirade.android.core.trashtalk.debate.data.model.Invite
import com.tirade.android.core.trashtalk.debate.data.model.InviteResponse
import com.tirade.android.core.trashtalk.forums.data.model.*
import com.tirade.android.core.trashtalk.home.data.model.home.CommentResponse
import com.tirade.android.core.trashtalk.home.data.model.home.PostActionResponse
import com.tirade.android.core.trashtalk.home.data.model.home.TrashHomeResponse
import com.tirade.android.core.trashtalk.home.data.model.newsfeed.NewsFeedResponse
import com.tirade.android.core.trashtalk.home.data.model.temp.DataModel
import com.tirade.android.core.trashtalk.home.data.model.temp.PostAction
import com.tirade.android.core.trashtalk.home.data.model.temp.PostComment
import com.tirade.android.core.trashtalk.notification.data.model.Notification
import com.tirade.android.core.trashtalk.notification.data.model.NotificationResponse
import com.tirade.android.core.trashtalk.trash.data.model.PostResponse
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface TiradeApi {

    /*@POST("login")
    @Headers("Content-Type: application/json")
    fun loginAuth(@Body login: String): Response<AuthResponse>*/

    @FormUrlEncoded
    @POST("api/user/login")
    fun incognitoLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Deferred<AuthResponse>


    @FormUrlEncoded
    @POST("api/user/login")
    fun publicLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Deferred<AuthResponse>


    /*@Headers("Content-Type: application/json")
    @POST("incognito/user/incognitoSignup")
    fun incognitoSignUp(@Body body: User) : Deferred<AuthResponse>*/

    /*@Headers("Content-Type: application/json")
    @POST("api/user/publicSignup")
    fun publicSignUp(@Body body: User) : Deferred<AuthResponse>*/

    @Multipart
    @POST("incognito/user/incognitoSignup1")
    fun incognitoSignUp(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<AuthResponse>

    @Multipart
    @POST("public/user/publicSignup")
    fun publicSignUp(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<AuthResponse>

    @GET("appapi/lt/category/home")
    fun getProjectList(): Deferred<DataModel>

    @GET("incognito/forum/get_forum")
    fun getForumsHomeListData(): Deferred<ForumsListResponse>

    @POST("incognito/forum/forum_detail")
    fun getForumsDetailData( @Body params: RequestBody): Deferred<ForumDetailsResponse>

    @GET("incognito/postdata/getdata")
    fun getTrashTalkHome(): Deferred<TrashHomeResponse>

        @GET("incognito/postdata/get_news_feed")
        fun getTrashTalkNewsFeed(): Deferred<NewsFeedResponse>

    @GET("public/postdata/getdata")
    fun getTiradeHomeList(): Deferred<TiradeHomeResponse>

   @GET("public/postdata/trending_videos")
    fun getTiradeTrendingList(): Deferred<TrendingData>

    @Multipart
    @POST("incognito/forum/post_forum")
    fun postMultipartFromForum(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<PostResponse>

    @Multipart
    @POST("incognito/postdata/postdata")
    fun postMultipartFromData(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<PostResponse>

   @Multipart
    @POST("incognito/postdata/news_feed")
    fun postMultipartShoutFromData(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<PostResponse>

 @Multipart
    @POST("madeskills/postdata/postdata")
    fun postMultipartMadSkillData(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<MadSkillFormResponse>


    @POST("incognito/comment/postcomment")
    fun postComment(
        @Header("Authorization") authorization: String,
        @Body commentBody: PostComment
    ): Deferred<CommentResponse>
    @POST("public/comment/postcomment")
    fun postTiradeComment(
        @Header("Authorization") authorization: String,
        @Body commentBody: PostComment
    ): Deferred<CommentResponse>
 @POST("incognito/forum/forum_comments")
    fun postForumComment(
        @Header("Authorization") authorization: String,
        @Body commentBody: ForumComments
    ): Deferred<ForumCommentResponse>
@POST("incognito/forum/get_comments")
    fun getForumComment(
        @Header("Authorization") authorization: String,
        @Body commentBody: RequestBody
    ): Deferred<ForumCommentResponse>

    @POST("incognito/action/action")
    fun postAction(
        @Header("Authorization") authorization: String,
        @Body commentBody: PostAction
    ): Deferred<PostActionResponse>
 @POST("public/action/action")
    fun postLikeAction(
        @Header("Authorization") authorization: String,
        @Body commentBody: PostAction
    ): Deferred<PostActionResponse>

    @POST("incognito/user/notification")
    fun inviteUsers(
        @Body invite: Invite
    ): Deferred<InviteResponse>

    @POST("incognito/user/get_notification")
    fun getTrashNotificationList(
        @Body notification: Notification
    ): Deferred<NotificationResponse>

    @POST("incognito/forum/get_likes")
    fun getLikeForum(
        @Body payload: ForumId
    ): Deferred<ForumGetLike>

    @POST("incognito/forum/get_comments")
    fun getCommentForum(
        @Body payload: ForumId
    ): Deferred<ForumGetComment>

    @POST("incognito/forum/forum_like_action")
    fun postForumLike(
        @Body payload: ForumActionPayload
    ): Deferred<ForumPostLike>

    @Multipart
    @POST("public/postdata/postdata")
    fun createPostForTirade(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<PostResponse>

    @Multipart
    @POST("public/postdata/getprofile")
    fun getEditProfileData(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>
    ): Deferred<EditProfileResponse>

    @Multipart
    @POST("public/postdata/public_profile")
    fun postProfileDataToServer(
        @Header("Authorization") authorization: String,
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part fileBody: MultipartBody.Part
    ): Deferred<EditProfileResponse>

    @GET("madeskills/postdata/rate_top_skills")
    fun getMadSkillsTopSkillsList(): Deferred<TopSkillsResponse>

    @GET("madeskills/postdata/get_made_skills")
    fun getMadSkillsList(): Deferred<MadSkillsList>

    @GET("madeskills/postdata/getdata")
    fun getMadSkillsHomeList(): Deferred<MadHomeResponse>


}

