package com.tirade.android.network

import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class APIClient {

    companion object {
        private const val baseURL: String = "https://ltappfeeds.intoday.in/"
        private var retrofit: Retrofit? = null

        fun client(): Retrofit {
            if (retrofit == null) {

                // val networkConnectionInterceptor = NetworkConnectionInterceptor(mContext)
                val okkHttpclient = OkHttpClient.Builder()
                        // .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        //  .addInterceptor(networkConnectionInterceptor)
                        .connectionSpecs(listOf(ConnectionSpec.MODERN_TLS, ConnectionSpec.COMPATIBLE_TLS))
                        .followRedirects(true)
                        .followSslRedirects(true)
                        .retryOnConnectionFailure(true)
                        .connectTimeout(20, TimeUnit.SECONDS)
                        .readTimeout(20, TimeUnit.SECONDS)
                        .writeTimeout(20, TimeUnit.SECONDS)
                        .cache(null)
                        .build()

                retrofit = Retrofit.Builder()
                        .addCallAdapterFactory(CoroutineCallAdapterFactory())
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(baseURL)
                        .client(okkHttpclient)
                        .build()
                //.create(APIClient::class.java)
            }
            return retrofit!!
        }
    }
}