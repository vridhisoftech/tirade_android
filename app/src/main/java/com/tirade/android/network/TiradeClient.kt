package com.tirade.android.network

import com.google.gson.GsonBuilder
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class TiradeClient {
    companion object {
        //private const val baseURL: String = "https://vridhisoftech.co.in/sh/tiradeapp/endpoints/"
        public const val baseURL: String = "https://vridhisoftech.in/tirade/"
        private var retrofit: Retrofit? = null

        fun client(): Retrofit {
            if (retrofit == null) {

                val networkConnectionInterceptor = NetworkConnectionInterceptor()
                val okkHttpclient = OkHttpClient.Builder()
                     .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                     .addInterceptor(networkConnectionInterceptor)
                    .connectionSpecs(listOf(ConnectionSpec.MODERN_TLS, ConnectionSpec.COMPATIBLE_TLS,ConnectionSpec.CLEARTEXT))
                    .followRedirects(true)
                    .followSslRedirects(true)
                    .retryOnConnectionFailure(true)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .cache(null)
                    .build()

                val gson = GsonBuilder()
                    .setLenient()
                    .create()

                retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(baseURL)
                    .client(okkHttpclient)
                    .build()
            }
            return retrofit!!
        }
    }
}