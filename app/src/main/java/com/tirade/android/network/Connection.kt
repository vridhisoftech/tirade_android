package com.tirade.android.network

import android.content.Context
import android.net.ConnectivityManager
import com.tirade.android.Tirade

class Connection {
    companion object{
         fun isConnected() : Boolean{
            val connectivityManager =
                Tirade.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            connectivityManager.activeNetworkInfo.also {
                return it != null && it.isConnected
            }
        }
    }
}