package com.tirade.android.constants

object Constants {

    //post_Type

    const val POST_IMAGE="Image"
    const val POST_VIDEO="Video"
    const val POST_PDF="Pdf"
    const val POST_TXT="TXT"
    const val POST_AUDIO="Audio"

}