package com.tirade.android

import android.app.Application
import com.tirade.android.store.PrefStoreManager

class Tirade :  Application() {
    init {
        mInstance = this
    }
    companion object{
        private lateinit var mInstance: Tirade
        @Synchronized
        fun getInstance(): Tirade {
            return mInstance as Tirade
        }
    }

    override fun onCreate() {
        super.onCreate()
        PrefStoreManager.with(this)
    }
}