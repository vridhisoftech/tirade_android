package com.tirade.android.dialog.video

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.tirade.android.R
import com.tirade.android.Tirade
import com.tirade.android.network.TiradeClient
import com.tirade.android.utils.Helper
import com.tirade.android.utils.goActivity
import com.tirade.android.utils.toast
import com.tirade.player.core.exoplayer2.C
import com.tirade.player.core.exoplayer2.ExoPlayerFactory
import com.tirade.player.core.exoplayer2.Player
import com.tirade.player.core.exoplayer2.SimpleExoPlayer
import com.tirade.player.core.exoplayer2.database.ExoDatabaseProvider
import com.tirade.player.core.exoplayer2.extractor.DefaultExtractorsFactory
import com.tirade.player.core.exoplayer2.source.ExtractorMediaSource
import com.tirade.player.core.exoplayer2.source.ProgressiveMediaSource
import com.tirade.player.core.exoplayer2.trackselection.AdaptiveTrackSelection
import com.tirade.player.core.exoplayer2.trackselection.DefaultTrackSelector
import com.tirade.player.core.exoplayer2.upstream.DataSource
import com.tirade.player.core.exoplayer2.upstream.DefaultBandwidthMeter
import com.tirade.player.core.exoplayer2.upstream.DefaultDataSourceFactory
import com.tirade.player.core.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.tirade.player.core.exoplayer2.upstream.cache.Cache
import com.tirade.player.core.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.tirade.player.core.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.tirade.player.core.exoplayer2.upstream.cache.SimpleCache
import com.tirade.player.core.exoplayer2.util.Util
import com.tirade.player.core.ui.AspectRatioFrameLayout
import com.tirade.player.core.ui.PlayerView
import kotlinx.android.synthetic.main.activity_player.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class PlayerActivity : AppCompatActivity(), OnLoadCompleteListener {

    private val TAG = "PlayerActivity"
    private var url:String?=null
    private var type:String?=null
    private lateinit var progressBar: ProgressBar
    private lateinit var progressBarPdf: ProgressBar
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private lateinit var mediaDataSourceFactory: DataSource.Factory
    private lateinit var playerView: PlayerView
    private lateinit var imageView: ImageView
    private lateinit var textView: WebView
    private lateinit var pdfView: PDFView
    var textHolder=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        Helper.setFullScreen(this,window)

        playerView = findViewById(R.id.playerView)
        imageView = findViewById(R.id.imageView)
        textView = findViewById(R.id.textView)
        pdfView = findViewById(R.id.pdfView)
        progressBar = findViewById(R.id.progressBar)

        url =intent.extras!!.getString("url")
        type =intent.extras!!.getString("type")
        imageViewExit.setOnClickListener {
            finish()
        }
        CheckForValidUrlAsyncTask(this).execute(url)

    }
    @SuppressLint("StaticFieldLeak")
    inner class CheckForValidUrlAsyncTask(private val mContext: Context) : AsyncTask<String?, String, Boolean>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg paths: String?): Boolean {

            try
            {
                val url = URL(paths[0])
                val urlConnection = url
                    .openConnection() as HttpURLConnection
                var  responseCode = urlConnection.getResponseCode()
                urlConnection.disconnect()
                return responseCode == 200
            }
            catch (e:MalformedURLException) {
                e.printStackTrace()
                return false
            }
            catch (e:IOException) {
                e.printStackTrace()
                return false
            }
        }

        override fun onPostExecute(compressedFilePath: Boolean) {
            super.onPostExecute(compressedFilePath)
            if (compressedFilePath)
            {
                displayContent()
            }
            else{
                toast("Invalid File")
                finish()
            }
        }


    }

    fun displayContent(){
        when (type) {
            "image" -> {
                playerView.visibility=View.GONE
                imageView.visibility=View.VISIBLE
                pdfView.visibility=View.GONE
                textView.visibility=View.GONE

                Glide.with(this).load(url).into(imageView)
            }
            "pdf" -> {

                loadPdf(pdfView)
                pdfView.visibility=View.VISIBLE
                playerView.visibility=View.GONE
                textView.visibility=View.GONE
                imageView.visibility=View.GONE
            }
            "txt" -> {
                textView.visibility=View.VISIBLE
                callWebview(textView,url!!)
                pdfView.visibility=View.GONE
                playerView.visibility=View.GONE
                imageView.visibility=View.GONE

            }
            else -> {
                pdfView.visibility=View.GONE
                textView.visibility=View.GONE
                playerView.visibility=View.VISIBLE
                imageView.visibility=View.GONE
            }
        }

    }

    private fun loadPdf(pdfView: PDFView) {
        //PDF View
        GlobalScope.launch {
            val input: InputStream = URL(url).openStream()
            pdfView.fromStream(input)
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
                .scrollHandle(null)
                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
                .spacing(0)
//                .pageFitPolicy(FitPolicy.WIDTH)
                .load()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun callWebview(webView2: WebView, src: String) {
        webView2.settings.setSupportZoom(true)
        webView2.settings.builtInZoomControls = true
        webView2.settings.javaScriptEnabled = true
        webView2.loadUrl(src)
        webView2.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                webView2.loadUrl(("javascript:(function() { " + "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()"))
            }
        }
    }


    private fun initializePlayer() {

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this)

        mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "Tirade"))

        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(
            Uri.parse(url))

        simpleExoPlayer.prepare(mediaSource, false, false)
        simpleExoPlayer.playWhenReady = true

        playerView.setShutterBackgroundColor(Color.TRANSPARENT)
        playerView.player = simpleExoPlayer
        playerView.requestFocus()

        /** Default repeat mode is REPEAT_MODE_ONE */
        simpleExoPlayer.repeatMode = Player.REPEAT_MODE_ONE


        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
        simpleExoPlayer.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
        simpleExoPlayer.addListener( object : Player.EventListener{

            /** 4 playbackState exists */
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when(playbackState){
                    Player.STATE_BUFFERING -> {
                        progressBar.visibility = View.VISIBLE
                        Log.d(TAG, "onPlayerStateChanged - STATE_BUFFERING" )
                    }
                    Player.STATE_READY -> {
                        progressBar.visibility = View.INVISIBLE
                        Log.d(TAG, "onPlayerStateChanged - STATE_READY" )
                    }
                    Player.STATE_IDLE -> {
                        Log.d(TAG, "onPlayerStateChanged - STATE_IDLE" )
                    }
                    Player.STATE_ENDED -> {
                        Log.d(TAG, "onPlayerStateChanged - STATE_ENDED" )
                    }
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                Log.d(TAG, "onLoadingChanged: ")
            }

            override fun onPositionDiscontinuity(reason: Int) {
                Log.d(TAG, "onPositionDiscontinuity: ")
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
                Log.d(TAG, "onRepeatModeChanged: ")
                Toast.makeText(baseContext, "repeat mode changed", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun releasePlayer() {
        simpleExoPlayer.release()
    }

    public override fun onStart() {
        super.onStart()

        if (Util.SDK_INT > 23) initializePlayer()
    }

    public override fun onResume() {
        super.onResume()

        if (Util.SDK_INT <= 23) initializePlayer()
    }

    public override fun onPause() {
        super.onPause()

        if (Util.SDK_INT <= 23) releasePlayer()
    }

    public override fun onStop() {
        super.onStop()

        if (Util.SDK_INT > 23) releasePlayer()
    }

    override fun loadComplete(nbPages: Int) {
//        progressBarPdf.visibility=View.GONE
    }
}
