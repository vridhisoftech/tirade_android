package com.tirade.expendablemenu

interface ExpandedMenuClickListener {
    fun onItemClick(position : Int)
}